<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chore extends Model
{
    //
    protected $guarded = [];

    public function routine()
    {
        return $this->belongsTo('App\Routine', 'routine_id');
    }

    public function getAssignToAttribute()
    {
        return $this->completed_at;
    }
}
