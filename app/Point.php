<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
    //

    protected $guarded = [];

    public function checklist()
    {
        return $this->belongsTo('App\Checklist', 'checklist_id');
    }

    /*
     * Point statu
     * */
    public function getPointStatusAttribute()
    {
        return $this->status;
    }

    public function getIsCompleteAttribute()
    {
        if ($this->status == 'complete') {
            return true;
        };
        return false;
    }
}
