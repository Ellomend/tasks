<?php

namespace App;

use App\Models\Calendar\CalendarEvent;
use App\Models\State\TaskState;
use App\Models\Status\TaskStatus;
use App\Models\Traits\HasOwner;
use App\Traits\HasCarbonDatesConstraints;
use Carbon\Carbon;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;

/**
 * @property array|mixed done_statuses
 */
class Task extends Model
{

    use HasOwner;
    use HasCarbonDatesConstraints;
    use Filterable;
    protected $guarded = [];

    public function contexts()
    {
        return $this->belongsToMany('App\Context');
    }

    public function owner()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function blocks()
    {
        return $this->belongsToMany('App\Block');
    }

    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }

    public function mood()
    {
        return $this->belongsTo('App\Mood', 'mood_id');
    }

    public function project()
    {
        return $this->belongsTo('App\Project', 'project_id');
    }

    /**
     * String representation of state
     * State is reflection of task date
     * @return string
     */
    public function state()
    {
        $state = new TaskState($this);
        return $state->getDateDiff();
    }

    /**
     * Reflection of task status
     * @return TaskStatus
     */
    public function getStatusObjectAttribute()
    {
        return new Models\Status\TaskStatus($this);
    }

    /**
     * String reflection of status
     * @return string
     */
    public function getStatusStringAttribute()
    {
        $status =  new Models\Status\TaskStatus($this);
        return $status->getStatus();
    }




}
