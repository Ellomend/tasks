<?php

namespace App;

use App\Models\Traits\HasOwner;
use Carbon\Carbon;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;

class Routine extends Model
{
        use HasOwner;
    use Filterable;
    //
    protected $guarded = [];
    public $timestamps = false;

    public function owner()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function chores()
    {
        return $this->hasMany('App\Chore');
    }

    public function blocks()
    {
        return $this->belongsToMany('\App\Block');
    }

    public function complete()
    {
        $completedAt = Carbon::now();
        $this->completeDated($completedAt);
    }
    public function completeDated(Carbon $date)
    {
        $completedAt = $date;
        $routine = $this;
        $chore = Chore::create([
            'name'         => 'Chore - '.$routine->name,
            'description'  => $routine->description,
            'routine_id'   => $routine->id,
            'completed_at' => $completedAt->toDateTimeString()
        ]);
        $chore->save();
        if ($routine->interval_type == 1) {
            $nextTime = $completedAt->addHours($routine->interval_value);
        } elseif ($routine->interval_type == 2) {
            $nextTime = Carbon::today()->addDays($routine->interval_value);
        } else {
            $nextTime = $completedAt->addHours(1);
        }
        $routine->last_complete = Carbon::now()->toDateTimeString();
        $routine->next = $nextTime->toDateTimeString();
        $routine->save();
    }

    public function getLastAttribute()
    {
        return Carbon::parse($this->last_complete);
    }

    public function getNextOneAttribute()
    {
        return Carbon::parse($this->next);
    }

    public function getStateAttribute()
    {
        $now = Carbon::now();
        if ($now->gt($this->next_one)) {
            return 'active';
        }
        return 'done';
    }

    public function getIntervalKindAttribute()
    {
        if ($this->interval_type == '1') {
            return 'hour';
        }
        if ($this->interval_type == '2') {
            return 'day';
        }
        return 'unknown';
    }
}
