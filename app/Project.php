<?php

namespace App;

use App\Models\Traits\HasOwner;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
        use HasOwner;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'projects';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description'];

	public function owner() {
		return $this->belongsTo('App\User', 'user_id');
	}


}
