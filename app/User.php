<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function posts()
    {
        return $this->hasMany('App\Post');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function tasks()
    {
        return $this->hasMany('App\Task');
    }
    public function contexts()
    {
        return $this->hasMany('App\Context');
    }

    public function blocks()
    {
        return $this->hasMany('App\Block');
    }

	public function goals() {
		return $this->hasMany('App\Goal');
	}

	public function routines() {
		return $this->hasMany('App\Routine');
	}
	public function stacks() {
		return $this->hasMany('App\Stack');
	}

	public function checklists() {
		return $this->hasMany('App\Checklist');
	}

	public function categories() {
		return $this->hasMany('App\Category');
	}

	public function moods() {
		return $this->hasMany('App\Mood');
	}

	public function projects() {
		return $this->hasMany('App\Project');
	}
}
