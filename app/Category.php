<?php

namespace App;

use App\Models\Traits\HasOwner;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasOwner;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description'];

    public function tasks()
    {
        return $this->hasMany('\Modules\Tasks\Entities\Task');
    }

    public function goals()
    {
        return $this->hasMany('\App\Goal');
    }

	public function owner() {
		return $this->belongsTo('App\User', 'user_id');
	}

}
