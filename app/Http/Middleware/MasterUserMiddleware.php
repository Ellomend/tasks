<?php

namespace App\Http\Middleware;

use App\Models\Master;
use Closure;
use Illuminate\Support\Facades\Auth;

class MasterUserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $master = app()->make('Master');
        /**
         *
         */
        /** @var Master $master */
        $master->setCurrentUser(Auth::user());
        return $next($request);
    }
}
