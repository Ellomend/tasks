<?php

namespace App\Http\Middleware;

use App\Step;
use Carbon\Carbon;
use Closure;

class SkipSteps
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $steps = Step::all();
        foreach ($steps as $step) {
            $now = Carbon::today();
            if(Carbon::parse($step->end_date) < $now && !in_array($step->status, $step->done_statuses))
            {
                $step->status = 'skipped';
                $step->finished = $now->toDateString();
                $step->save();
                $test = 'test';
            }
        }

        return $next($request);
    }
}
