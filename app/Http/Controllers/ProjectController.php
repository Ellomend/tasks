<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Project;
use App\Repositories\Project\DbProjectRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class ProjectController extends Controller
{
	/**
	 * @var DbProjectRepository
	 */
	private $repository;

	/**
	 * ProjectController constructor.
	 *
	 * @param DbProjectRepository $repository
	 */
	public function __construct(DbProjectRepository $repository) {
		$this->repository = $repository;
	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $projects = $this->repository->getOwned();

        return view('project.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('project.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $this->repository->createFromFormRequest($request);

        Session::flash('flash_message', 'Project added!');

        return redirect('project');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $project = $this->repository->getById($id);

        return view('project.show', compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $project = $this->repository->getById($id);

        return view('project.edit', compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $this->repository->updateFromFormRequest($id, $request);

        Session::flash('flash_message', 'Project updated!');

        return redirect('project');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $this->repository->delete($id);

        Session::flash('flash_message', 'Project deleted!');

        return redirect('project');
    }

	public function getDelete($id) {
		$this->repository->delete($id);

		Session::flash('flash_message', 'Project deleted!');

		return redirect('project');
	}
}
