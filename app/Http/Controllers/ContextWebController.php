<?php

namespace App\Http\Controllers;

use App\Context;
use App\Repositories\Context\DbContextRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class ContextWebController extends Controller {
	/**
	 * @var DbContextRepository
	 */
	private $repository;

	/**
	 * ContextWebController constructor.
	 *
	 * @param DbContextRepository $repository
	 */
	public function __construct( DbContextRepository $repository ) {
		$this->repository = $repository;
	}

	/**
	 * Display a listing of the resource.
	 * @return Response
	 */
	public function index() {
//        $contexts = Auth::user()->contexts()->get();
		$contexts = $this->repository->getOwned();

		return view( 'tasks.context.index', compact( 'contexts' ) );
	}

	/**
	 * Show the form for creating a new resource.
	 * @return Response
	 */
	public function create() {
		return view( 'tasks.context.create' );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  Request $request
	 *
	 * @return Response
	 */
	public function store( Request $request ) {

		$context = $this->repository->createFromFormRequest( $request );

		return redirect()->route( 'context.index' );
	}

	/**
	 * Show the form for editing the specified resource.
	 * @return Response
	 */
	public function edit( $id ) {
		$context = $this->repository->getById( $id );
		$this->authorize( 'edit', $context );

		return view( 'tasks.context.edit', compact( 'context' ) );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  Request $request
	 *
	 * @return Response
	 */
	public function update( Request $request, $id ) {
		$context = $this->repository->updateFromFormRequest( $id, $request );
		return redirect()->route( 'context.show', compact( 'context' ) );
	}

	public function show( $id ) {
		$context = $this->repository->getById( $id );

		return view( 'tasks.context.show', compact( 'context' ) );
	}

	/**
	 * Remove the specified resource from storage.
	 * @return Response
	 */
	public function destroy() {
	}

	public function getDelete( $id ) {
		$this->repository->delete( $id );

		return redirect()->route( 'context.index' );
	}

	public function my() {

		$contexts = $this->repository->getOwned();
		return view( 'tasks.context.my_contexts', compact( 'contexts' ) );
	}
}
