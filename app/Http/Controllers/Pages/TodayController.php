<?php

namespace App\Http\Controllers\Pages;

use App\Repositories\GlobalRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TodayController extends Controller
{

    //
    /**
     * @var GlobalRepository
     */
    private $repository;

    /**
     * TodayController constructor.
     * @param GlobalRepository $repository
     */
    public function __construct(GlobalRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $blocks = $this->repository->blocks->getOwned();
        $tasks = $this->repository->tasks->getToday();
        return view('pages.day.day_playground', compact('blocks', 'tasks'));
    }
}
