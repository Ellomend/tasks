<?php

namespace App\Http\Controllers\Pages;

use App\Models\Helpers\DateRange\Month;
use App\Models\Helpers\DateRange\Week;
use App\Models\Master;
use App\Repositories\GlobalRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    /**
     * @var GlobalRepository
     */
    private $repository;

    /**
     * PagesController constructor.
     * @param GlobalRepository $repository
     */
    public function __construct(GlobalRepository $repository)
    {
        $this->repository = $repository;
    }

    public function day()
    {

        $dueTasks = $this->repository->tasks->due();
        $lateTasks = $this->repository->tasks->late();

        /** @var Master $master */
        $master = app()->make('Master');
        $tasks = $master->tasks;
        $blocks = $this->repository->blocks->getOwned();
        return view('pages.day.day', compact('dueTasks', 'lateTasks', 'blocks'));
    }
    public function mixed()
    {
        $tasks = $this->repository->tasks->getOwned();
        $filters = $this->repository->getFilterObjects();
//        dd($filters);
        return view('tasks.pages.mixed.mixed', compact('tasks', 'filters'));
    }

    public function week()
    {
        $tasks = $this->repository->tasks->getOwned();

        $week = new Week();

        $events = $week->assignEvents($tasks);
        return view('tasks.pages.week', compact('week'));
    }

    public function month()
    {
        $tasks = $this->repository->tasks->getOwned();
        $month = new Month();
        $events = $month->assignEvents($tasks);
        return view('tasks.pages.month', compact('month'));
    }

}
