<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use App\Models\Status\StatusEnum;
use App\Repositories\GlobalRepository;
use App\Step;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CalendarController extends Controller
{
    /**
     * @var GlobalRepository
     */
    private $repository;

    /**
     * CalendarController constructor.
     * @param GlobalRepository $repository
     */
    public function __construct(GlobalRepository $repository)
    {
        $this->repository = $repository;
    }

    public function tasks()
    {
        $contexts = $this->repository->contexts->getOwned();
        $blocks = $this->repository->blocks->getOwned();
        $categories = $this->repository->categories->getOwned();
        $moods = $this->repository->moods->getOwned();
        $projects = $this->repository->projects->getOwned();
        return view('pages.calendar.tasks_calendar', compact('contexts', 'blocks', 'categories', 'moods', 'projects'));
    }

    public function getTasks()
    {
        $tasks = $this->repository->tasks->getOwned();
        $tasks = $tasks->each(function ($task) {
            //            $task->title = $task->name;
            $task->title = 'T('.$task->id.'): '.$task->name.' ('.$task->state().')';
            $task->type = 'task';

        });
        return $tasks;
    }

    public function storeTask(Request $request)
    {
        $task = $this->repository->tasks->createFromFormRequest($request);
        $task->title = $task->name;
        return redirect()->back();
    }


    public function updateTask(Request $request)
    {
        $data = json_decode($request->get('data'));
        return $this->move($data);
    }

    public function combined()
    {
        $filters = $this->repository->getFilterObjects();
        return view('pages.calendar.combined_calendar', compact('filters'));
    }

    public function getSteps()
    {
        $goals = $this->repository->goals->getOwned();
        $steps = collect([]);
        foreach ($goals as $goal) {
            foreach ($goal->steps as $step) {
                $steps->push($step);
            }
        }
        $steps = $steps->each(function ($step) {
            $step->title = 'S: '.$step->name.' ('.$step->state.')';
            $step->type = 'step';
            $step->start = $step->start_date;
            $step->end = 'test';
        });
        return $steps;
    }

    public function getCompletedTasks()
    {
        $allTasks = $this->getTasks();
        $incomplete = collect();
        $allTasks->each(function ($task) use ($incomplete) {
            if ($task->status == StatusEnum::STATUS_COMPLETE) {
                $incomplete->push($task);
            }
        });
        return $incomplete;
    }

    public function getIncompleteTasks()
    {
        $allTasks = $this->getTasks();
        $incomplete = collect();
        $allTasks->each(function ($task) use ($incomplete) {
            if ($task->status != StatusEnum::STATUS_COMPLETE) {
                $incomplete->push($task);
            }
        });
        return $incomplete;
    }

    public function getCompletedSteps()
    {
        $allSteps = $this->getSteps();
        $completed = collect();
        $allSteps->each(function ($step) use ($completed) {
            if ($step->state == 'done') {
                $step->end1 = 123;
                $completed->push($step);
            }
        });
        return $completed;
    }

    public function getIncompleteSteps()
    {
        $allSteps = $this->getSteps();
        $completed = collect();
        $allSteps->each(function ($step) use ($completed) {
            if ($step->state != 'done') {
                $step->end = 222;
                $completed->push($step);
            }
        });
        return $completed;
    }


    protected function move($data)
    {
        if ($data->type == 'task') {
            $task = $this->repository->tasks->getById($data->id);
            $task->start = $data->start;
            $task->end = $data->end;
            $task->save();
            return $task;
        } else {
            $step = Step::find($data->id);
            $step->start_date = $data->start;
            $step->end_date = $data->end;
            $step->save();
            return $step;
        }
    }
}
