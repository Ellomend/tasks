<?php

namespace App\Http\Controllers\Tasks;

use App\Http\Controllers\Controller;
use App\Repositories\Tasks\Today\DbTaskTodayRepository;
use Illuminate\Http\Request;

class TaskTodayController extends Controller
{
    //
    /**
     * @var DbTaskTodayRepository
     */
    private $repository;

    /**
     * TaskTodayController constructor.
     *
     * @param DbTaskTodayRepository $repository
     */
    public function __construct(DbTaskTodayRepository $repository)
    {
        $this->repository = $repository;
    }

    public function today()
    {
        $lateTasks = $this->repository->late();
        $dueTasks = $this->repository->due();
        $timelessTasks = $this->repository->timeless();
        return view('tasks.pages.today', compact('lateTasks', 'dueTasks', 'timelessTasks'));
    }
}
