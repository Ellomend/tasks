<?php

namespace App\Http\Controllers\Tasks;

use App\Category;
use App\Http\Controllers\Controller;
use App\Models\Helpers\DateRange\Month;
use App\Models\Helpers\DateRange\Week;
use App\Models\Status\StatusEnum;
use App\Mood;
use App\Project;
use App\Repositories\GlobalRepository;
use App\Repositories\Goal\DbGoalRepository;
use App\Repositories\Tasks\DbTaskRepository;
use App\User;
use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Support\Facades\Auth;
use App\Block;
use App\Context;
use App\Task;

class TaskWebController extends Controller
{
    /**
     * @var DbTaskRepository
     */
    private $repository;
    /**
     * @var GlobalRepository
     */
    private $globalRepository;

    /**
     * TaskWebController constructor.
     *
     * @param DbTaskRepository $repository
     * @param GlobalRepository $globalRepository
     */
    public function __construct(DbTaskRepository $repository, GlobalRepository $globalRepository)
    {
        $this->repository = $repository;
        $this->globalRepository = $globalRepository;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

        $tasks = $this->repository->getOwned();
        return view('tasks.pages.index', compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('tasks.pages.create', compact('contexts', 'blocks', 'categories', 'projects', 'moods'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $task = $this->repository->createFromFormRequest($request);
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $users = User::all();
        $contexts = Auth::user()->contexts()->get();
        $blocks = Auth::user()->blocks()->get();
        $categories = Auth::user()->categories()->get();
        $task = $this->repository->findById($id);
        $moods = Auth::user()->moods()->get();
        $projects = Auth::user()->projects()->get();

        return view('tasks.pages.edit', compact('task', 'contexts', 'users', 'blocks', 'categories', 'moods', 'projects'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $task = $this->repository->updateFromFormRequest($id, $request);
        return redirect()->route('task.index');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function show($id)
    {
        $task = $this->repository->getById($id);
        return view('tasks.pages.show', compact('task'));
    }

    public function getDelete($id)
    {
        $this->repository->delete($id);
        return redirect()->route('task.index');
    }


    public function complete($id, $status)
    {
        $task = Task::find($id);
        $task->status = $status;
        $task->finished = Carbon::now()->toDateTimeString();
        $task->save();
        return redirect()->back();
    }

    public function gantt()
    {
        $select = 'name as label, DATE_FORMAT(start, \'%Y-%m-%d\') as start, DATE_FORMAT(end, \'%Y-%m-%d\') as end';
        $projects = \App\Task::select(\Illuminate\Support\Facades\DB::raw($select))
            ->orderBy('start', 'asc')
            ->orderBy('end', 'asc')
            ->get();

        /**
         *  You'll pass data as an array in this format:
         *  [
         *    [
         *      'label' => 'The item title',
         *      'start' => '2016-10-08',
         *      'end'   => '2016-10-14'
         *    ]
         *  ]
         */
        if (empty($projects)) {
            return 'no tasks';
        }
        $gantt = new \Swatkins\LaravelGantt\Gantt($projects->toArray(), array(
            'title' => 'Demo',
            'cellwidth' => 25,
            'cellheight' => 35
        ));

        return view('tasks.gantt')->with(['gantt' => $gantt]);
    }


    public function widgets()
    {
        $tasks = $this->repository->getOwned();
        return view('tasks.pages.widgets.widgets', compact('tasks'));
    }

    public function archive($id)
    {
        $task = $this->repository->getById($id);
        $task->status = StatusEnum::STATUS_ARCHIVED;
        $task->save();
        return redirect()->back();
    }

    public function archived()
    {
        $tasks = $this->repository->getOwnedWithArchived();
        $tasks = $tasks->filter(function ($value, $key) {
            if ($value->status == StatusEnum::STATUS_ARCHIVED) {
                return true;
            }
            return false;
        });
        return view('tasks.pages.index', compact('tasks'));
    }
}
