<?php

namespace App\Http\Controllers\Tasks;

use App\Http\Controllers\Controller;
use App\Repositories\Tasks\Filter\DbTaskFilterRepository;
use App\Task;
use Illuminate\Http\Request;

class TaskFilterController extends Controller
{
    //
    /**
     * @var DbTaskFilterRepository
     */
    private $repository;

    /**
     * TaskFilterController constructor.
     *
     * @param DbTaskFilterRepository $repository
     */
    public function __construct(DbTaskFilterRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View action
     * action
     */
    public function filter(Request $request)
    {
        $result = $this->repository->init();
        $tasks = Task::filter($request->all())->get();
        return view('tasks.pages.filter.task_filter', compact('request', 'tasks', 'result'));
    }
}
