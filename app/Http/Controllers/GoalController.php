<?php

namespace App\Http\Controllers;

use Alpha\B;
use App\Category;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Goal;
use App\Models\Helpers\DateRange\Month;
use App\Models\Helpers\DateRange\Week;
use App\Repositories\Goal\DbGoalRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Block;
use Session;

class GoalController extends Controller
{
    /**
     * @var DbGoalRepository
     */
    private $repository;

    /**
     * GoalController constructor.
     * @param DbGoalRepository $repository
     */
    public function __construct(DbGoalRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $goals = $this->repository->getOwned();

        return view('goal.index', compact('goals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $categories = Category::all();
        $blocks = Auth::user()->blocks;

        return view('goal.create', compact('categories', 'blocks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        //TODO assigned blocks test
        $this->repository->createFromFormRequest($request);

        Session::flash('flash_message', 'Goal added!');

        return redirect('goal');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {

        //TODO repalce all with repository call
        $goal = $this->repository->getById($id);
        $steps = $goal->steps;
        $categories = Category::all();

        return view('goal.show', compact('goal', 'steps', 'categories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $goal = $this->repository->getById($id);
        $categories = Category::all();
        $blocks = Auth::user()->blocks;

        return view('goal.edit', compact('goal', 'categories', 'blocks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->repository->updateFromFormRequest($id, $request);

        Session::flash('flash_message', 'Goal updated!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $this->repository->delete($id);

        Session::flash('flash_message', 'Goal deleted!');

        return redirect('goal');
    }

    public function getDelete($id)
    {
        $this->repository->delete($id);
        Session::flash('flash_message', 'Goal deleted!');

        return redirect('goal');
    }

    public function completeCurrentStep($id)
    {
        $goal = $this->repository->completeCurrentStep($id);
        return redirect()->back();
    }


    public function weekHistory($id)
    {

        $goal = $this->repository->getById($id);
        $steps = $goal->steps;
        $week = new Week();

        $events = $week->assignEvents($steps);
        return view('tasks.pages.week', compact('week'));

    }

    public function monthHistory($id)
    {
        $goal = $this->repository->getById($id);
        $steps = $goal->steps;
        $month = new Month();
        $events = $month->assignEvents($steps);
        return view('tasks.pages.month', compact('month'));
    }


}
