<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Mood;
use App\Repositories\Mood\DbMoodRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class MoodController extends Controller
{
	/**
	 * @var DbMoodRepository
	 */
	private $repository;

	/**
	 * MoodController constructor.
	 *
	 * @param DbMoodRepository $repository
	 */
	public function __construct(DbMoodRepository $repository) {
		$this->repository = $repository;
	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
		$mood = $this->repository->getOwned();
        return view('mood.index', compact('mood'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('mood.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $this->repository->createFromFormRequest($request);

        Session::flash('flash_message', 'Mood added!');

        return redirect('mood');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $mood = $this->repository->getById($id);

        return view('mood.show', compact('mood'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $mood = $this->repository->getById($id);

        return view('mood.edit', compact('mood'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $this->repository->updateFromFormRequest($id, $request);

        Session::flash('flash_message', 'Mood updated!');

        return redirect('mood');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $this->repository->delete($id);

        Session::flash('flash_message', 'Mood deleted!');

        return redirect('mood');
    }

	public function getDelete( $id ) {
		$this->repository->delete($id);

		Session::flash('flash_message', 'Mood deleted!');

		return redirect('mood');
	}
}
