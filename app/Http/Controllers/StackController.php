<?php

namespace App\Http\Controllers;

use App\Repositories\Stack\DbStackRepository;
use App\Stack;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StackController extends Controller
{
    /**
     * @var DbStackRepository
     */
    private $repository;

    /**
     * StackController constructor.
     * @param DbStackRepository $repository
     */
    public function __construct(DbStackRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //TODO get own instead
        $stacks = Stack::all();

        return view('stacks.stacks.index', compact('stacks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('stacks.stacks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data  = $request->except('_token', '_method');
        $stack = Stack::create($data);
        $user = Auth::user();
        if ($user) {
            $stack->owner()->associate($user);
            $stack->save();
        }
        return redirect()->route('stack.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $stack = Stack::find($id);
        return view('stacks.stacks.show', compact('stack'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $stack =  Stack::find($id);
        return view('stacks.stacks.edit', compact('stack'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = $request->except('_method', '_token');
        $stack = Stack::find($id);
        $stack->update($data);
        return redirect()->route('stack.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Stack::destroy($id);
        return redirect()->route('stack.index');
    }

    public function bump($id)
    {
        $bumped = $this->repository->getById($id);
        $stacks = $this->repository->getOwned();
        return view('stacks.items.bump_stack', compact('bumped', 'stacks'));
    }

    public function review()
    {
        $stacks = $this->repository->getOwned();

        return view('stacks.pages.stacks_review', compact('stacks'));
    }

    public function singleReview($id)
    {
        $stacks[] = $this->repository->getById($id);
        return view('stacks.pages.stacks_review', compact('stacks'));
    }
}
