<?php

namespace App\Http\Controllers;

use App\Item;
use App\Repositories\Stack\DbItemRepository;
use App\Stack;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ItemController extends Controller
{
    /**
     * @var DbItemRepository
     */
    private $repository;

    /**
     * ItemController constructor.
     * @param DbItemRepository $repository
     */
    public function __construct(DbItemRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $items = Item::all();
        return view('stacks.items.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $stacks = Stack::all();
        $user = Auth::user();
//	    if ($user)
//	    {
//	    	$stacks = $user->stacks;
//	    }
//	    dd($stacks);
        return view('stacks.items.create', compact('stacks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->except('_token', '_method');
        $item = Item::create($data);
//	    dd($data);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $item = Item::find($id);
        return view('stacks.items.show', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $item = Item::find($id);
        $stacks = Stack::all();
        $user = Auth::user();
        if ($user) {
            $stacks = $user->stacks;
        }
        return view('stacks.items.edit', compact('item', 'stacks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = $request->except('_method', '_token');
        $item = Item::find($id);
        $item->update($data);
        return redirect()->route('stack.review.single', $item->stack->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Item::destroy($id);
        return redirect()->back();
    }

    public function status(Request $request)
    {
//        dd($request->all());
        $item = $this->repository->getById($request->get('id'));
        $item->status = $request->get('status');
        $item->save();
        return redirect()->back();
    }
}
