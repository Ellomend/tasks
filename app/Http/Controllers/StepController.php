<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\Goal\DbGoalRepository;
use App\Repositories\Goal\DbStepRepository;
use App\Step;
use App\Models\Helpers\StepsHelper;
use App\Models\Helpers\Recurrence;
use App\Goal;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use Swatkins\LaravelGantt\Gantt;

class StepController extends Controller
{
    /**
     * @var DbStepRepository
     */
    private $repository;
    /**
     * @var DbGoalRepository
     */
    private $goals;

    /**
     * StepController constructor.
     * @param DbStepRepository $repository
     * @param DbGoalRepository $goals
     */
    public function __construct(DbStepRepository $repository, DbGoalRepository $goals)
    {
        $this->repository = $repository;
        $this->goals = $goals;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $step = $this->repository->getOwned();

        return view('step.index', compact('step'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {

        $goals = $this->goals->getOwned();

        return view('step.create', compact('goals'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->repository->createFromFormRequest($request);

        Session::flash('flash_message', 'Step added!');

        return redirect('step');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $step = $this->repository->getById($id);

        return view('step.show', compact('step'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $step = $this->repository->getById($id);
        $goals = $this->goals->getOwned();
        return view('step.edit', compact('step', 'goals'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->repository->updateFromFormRequest($id, $request);

        Session::flash('flash_message', 'Step updated!');

        return redirect('step');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $this->repository->delete($id);

        Session::flash('flash_message', 'Step deleted!');

        return redirect('step');
    }

    public function regenerate($id)
    {
        $goal = $this->goals->getById($id);



        if (!$goal) {
            flash('goal not found', 'warining');
            return redirect()->route('tasks.my.index');
        }

        $this->regenerateSteps($goal);


        flash('Steps regenerated', 'success');
        return redirect()->back();
    }

    public function regenerateall()
    {
        $goals = $this->goals->getOwned();
        foreach ($goals as $goal) {
            $this->regenerateSteps($goal);
        }
        return redirect()->route('goal.index');
    }

    /**
     * @param $goal
     * action
     */
    public function regenerateSteps($goal)
    {
        $recurrences = StepsHelper::getRecurrences($goal);
        $steps = StepsHelper::createSteps($recurrences, $goal);
    }


    public function complete($id, $status)
    {
        $step = $this->repository->getById($id);
        $step->status = $status;
        $step->finished = Carbon::now();
        $step->save();
        flash('step completed', 'success');
        return redirect()->back();
    }


    public function delete($id)
    {
        $step = $this->repository->delete($id);
        return redirect()->route('step.index');
    }

    public function gantt($id)
    {
        $goal = Goal::find($id);
        $steps = $goal->steps()
            ->orderBy('start_date', 'asc')
            ->orderBy('end_date', 'asc')
            ->get();;
        $select = 'name as label, DATE_FORMAT(start, \'%Y-%m-%d\') as start, DATE_FORMAT(end, \'%Y-%m-%d\') as end';
        $projects = \App\Task::select(\Illuminate\Support\Facades\DB::raw($select))
            ->orderBy('start', 'asc')
            ->orderBy('end', 'asc')
            ->get();


        $gantt = new Gantt($steps->toArray(), array(
            'title' => 'Demo',
            'cellwidth' => 25,
            'cellheight' => 35
        ));

        return view('step.gantt')->with(['gantt' => $gantt]);
    }

    public function skip($id)
    {
        $step = $this->repository->getById($id);
        $step->skip();
        return redirect()->back();
    }
}
