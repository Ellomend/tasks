<?php

namespace App\Http\Controllers;

use App\Repositories\Block\DbBlockRepository;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Block;

class BlockWebController extends Controller
{
	/**
	 * @var DbBlockRepository
	 */
	private $repository;

	/**
	 * BlockWebController constructor.
	 *
	 * @param DbBlockRepository $repository
	 */
	public function __construct(DbBlockRepository $repository) {
		$this->repository = $repository;
	}

	/**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
		$blocks = $this->repository->getOwned();
        return view('tasks.block.index', compact('blocks'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('tasks.block.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->repository->createFromFormRequest($request);
        return redirect()->route('block.index');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
		$block = $this->repository->getById($id);
        $users = User::all();
        return view('tasks.block.edit', compact('block', 'users'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->repository->updateFromFormRequest($id, $request);
        return redirect()->route('block.index');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function getDelete($id)
    {
    	$this->repository->delete($id);
        return redirect()->route('block.index');
    }

    public function show($id)
    {
    	$block = $this->repository->getById($id);
        return view('tasks.block.show', compact('block'));
    }

    public function regenerate()
    {
        $user = Auth::user();
        $blocks = $user->blocks;
        foreach ($blocks as $block) {
            if ($block->description == 'default block')
            {
                $block->delete();
            }
        }

        $block_morning = factory(Block::class)->make([
            'name' => 'morning',
            'description' => 'default block',
            'start' => '7:00:00',
            'end' => '12:00:00',
            'user_id' => $user->id
        ])->save();
        $block_day = factory(Block::class)->make([
            'name' => 'day',
            'description' => 'default block',
            'start' => '12:00:00',
            'end' => '18:00:00',
            'user_id' => $user->id
        ])->save();
        $block_evening = factory(Block::class)->make([
            'name' => 'evening',
            'description' => 'default block',
            'start' => '18:00:00',
            'end' => '23:59:59',
            'user_id' => $user->id
        ])->save();
        $block_night = factory(Block::class)->make([
            'name' => 'night',
            'description' => 'default block',
            'start' => '00:00:00',
            'end' => '6:59:59',
            'user_id' => $user->id
        ])->save();

        return redirect()->route('block.index');
    }
}
