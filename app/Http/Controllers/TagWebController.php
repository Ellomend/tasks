<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Tag;
use SuperClosure\Analyzer\TokenAnalyzer;

class TagWebController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $tags = Tag::all();
        return view('blog.tags.index', compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('blog.tags.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $tag = Tag::create($request->except('_token'));
//        return view('blog.tags.show', compact('tag'));
        return redirect()->route('tag.index');
    }


    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $tag = Tag::find($id);

        return view('blog.tags.edit', compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $tag = Tag::find($id);
        $tag->update($request->except('_token'));
        return redirect()->route('tag.index');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function getdelete($id)
    {
        $tag = Tag::find($id);
        $tag->delete();
        return redirect()->route('tag.index');
    }

    public function show($id)
    {
        $tag = Tag::find($id);
        return view('blog.tags.show', compact('tag'));
    }
}
