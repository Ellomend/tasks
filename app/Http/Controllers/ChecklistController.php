<?php

namespace App\Http\Controllers;

use App\Checklist;
use App\Point;
use App\Repositories\Checklist\DbChecklistRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChecklistController extends Controller
{
    /**
     * @var DbChecklistRepository
     */
    private $repository;

    /**
     * ChecklistController constructor.
     * @param DbChecklistRepository $repository
     */
    public function __construct(DbChecklistRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //TODO get own instead
        $checklists = $this->repository->getOwned();

        return view('checklists.checklists.index', compact('checklists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('checklists.checklists.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->repository->createFromFormRequest($request);
        return redirect()->route('checklist.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $checklist = $this->repository->getById($id);
        return view('checklists.checklists.show', compact('checklist'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $checklist = $this->repository->getById($id);
        return view('checklists.checklists.edit', compact('checklist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->repository->updateFromFormRequest($id, $request);
        return redirect()->route('checklist.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $this->repository->delete($id);
        return redirect()->back();
    }


    public function addPoint($id)
    {
        $checklist = $this->repository->getById($id);
        return view('checklists.points.create_for_checklist', compact('checklist'));
    }

    public function getBlueprints()
    {
        $allChecklists = $this->repository->getOwned();
        $checklists = $allChecklists->filter(function ($checklist) {
            if ($checklist->blueprint) {
                return true;
            }
            return false;
        });
        return view('checklists.checklists.index', compact('checklists'));
    }

    public function startBlueprint($id)
    {
        $blueprint = $this->repository->getById($id);
        $blueprintPoints = $blueprint->points;
        $checklist = factory(Checklist::class)->create([
            'name'        => $blueprint->name,
            'description' => $blueprint->description,
            'user_id'     => $blueprint->user_id,
            'type'        => 2
        ]);
        foreach ($blueprintPoints as $point) {
            $checlistPoints[] = factory(Point::class)->create([
                'name'         => $point->name,
                'description'  => $point->description,
                'checklist_id' => $checklist->id,
                'status'       => 'active'
            ]);
        }
        return redirect()->route('checklist.show', $checklist->id);
    }

    public function reorder($id)
    {
        $lastItem = $this->repository->getLastItemOrder($id);
        return redirect()->back();
    }

    public function running()
    {
        $checklists = $this->repository->getOwnRunning();
        return view('checklists.checklists.pages.running', compact('checklists'));
    }
}
