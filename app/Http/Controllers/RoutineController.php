<?php

namespace App\Http\Controllers;

use App\Models\Helpers\DateRange\Month;
use App\Models\Helpers\DateRange\Week;
use App\Models\Master;
use App\Repositories\Block\DbBlockRepository;
use App\Repositories\Routine\DbRoutineRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RoutineController extends Controller
{
    //
    /**
     * @var DbRoutineRepository
     */
    private $repository;
    /**
     * @var DbBlockRepository
     */
    private $blockRepository;

    /**
     * RoutineController constructor.
     *
     * @param DbRoutineRepository $repository
     * @param DbBlockRepository $blockRepository
     */
    public function __construct(DbRoutineRepository $repository, DbBlockRepository $blockRepository)
    {
        $this->repository = $repository;
        $this->blockRepository = $blockRepository;
    }

    public function index()
    {
        $routines = $this->repository->getOwned();

        return view('routines.routines.index', compact('routines'));
    }

    public function create()
    {
        return view('routines.routines.create');
    }

    public function store(Request $request)
    {
        $this->repository->createFromFormRequest($request);

        return redirect()->route('routine.index');
    }

    public function edit($id)
    {
        $routine = $this->repository->getById($id);
        $blocks = $this->blockRepository->getOwned();


        return view('routines.routines.edit', compact('routine', 'blocks'));
    }


    public function update(Request $request, $id)
    {
        $this->repository->updateFromFormRequest($id, $request);
        return redirect()->back();
    }

    public function show($id)
    {
        $routine = $this->repository->getById($id);
        return view('routines.routines.show', compact('routine'));
    }

    public function destroy($id)
    {
        $this->repository->delete($id);
        return redirect()->back();
    }

    public function actual()
    {
        $routines = $this->repository->ownedComplete();
        return view('routines.pages.actual', compact('routines'));
    }


    public function complete($id, Request $request)
    {
        $this->repository->complete($id);
        return redirect()->back();
    }

    public function blockFilter($id = 'now')
    {
        $blocks = $this->blockRepository->getCurrentBlocks();
        $routines = [];
        foreach ($blocks as $block) {
            foreach ($block->routines as $routine) {
                if (Carbon::parse($routine->next)->lessThanOrEqualTo(Carbon::now())) {
                    $routines[] = $routine;
                }
            }
        }

        return view('routines.pages.now', compact('routines'));
    }

    public function routineWeekHistory($id)
    {

        $routine = $this->repository->getById($id);
        $chores = $routine->chores;
        $week = new Week();

        $events = $week->assignEvents($chores);
        return view('tasks.pages.week', compact('week'));

    }

    public function routineMonthHistory($id)
    {
        $routine = $this->repository->getById($id);
        $chores = $routine->chores;
        $month = new Month();
        $events = $month->assignEvents($chores);
        return view('tasks.pages.month', compact('month'));
    }
}
