<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Comment;
use App\Post;

class CommentWebController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('blog.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create($id)
    {
        $post = Post::find($id);

        return view('blog.pages.comment.add', compact('post'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request, $id)
    {
        $post = Post::find($id);

        $user = Auth::user();

        if($user && $post )
        {
            $comment = Comment::create([
                'user_id' => $user->id,
                'post_id' => $post->id,
                'content' => $request->get('content')
            ]);
        }
        else
        {

            return redirect()->route('post.index');
        }
        return redirect()->route('post.show', [$post->id]);

    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $comment = Comment::find($id);
        $post = $comment->post()->get();
        return view('blog.pages.comment.edit', compact('comment', 'post'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $comment = Comment::find($id);
        $comment->update([
            'content' => $request->get('content')
        ]);
        $post = $comment->post;
        return redirect()->route('post.show', [$post->id]);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function getDelete($id)
    {
        $comment = Comment::find($id);
        $post = $comment->post;
        $comment->delete();
        return redirect()->route('post.show', [$post->id]);
    }
}
