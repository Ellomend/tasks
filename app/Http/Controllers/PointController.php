<?php

namespace App\Http\Controllers;

use App\Checklist;
use App\Point;
use App\Repositories\Checklist\DbChecklistRepository;
use App\Repositories\Checklist\DbPointRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PointController extends Controller
{
    /**
     * @var DbPointRepository
     */
    private $repository;
    /**
     * @var DbChecklistRepository
     */
    private $checklistRepository;

    /**
     * PointController constructor.
     * @param DbPointRepository $repository
     * @param DbChecklistRepository $checklistRepository
     */
    public function __construct(DbPointRepository $repository, DbChecklistRepository $checklistRepository)
    {
        $this->repository = $repository;
        $this->checklistRepository = $checklistRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $points = $this->repository->getOwned();
        return view('checklists.points.index', compact('points'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $checklists = $this->checklistRepository->getOwned();
        return view('checklists.points.create', compact('checklists'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $point = $this->repository->createFromFormRequest($request);
        $checklist = $point->checklist;
        return redirect()->route('checklist.show', [$checklist->id] );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $point = $this->repository->getById($id);
        return view('checklists.points.show', compact('point'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $point = $this->repository->getById($id);
        $checklists = $this->checklistRepository->getOwned();
        return view('checklists.points.edit', compact('point', 'checklists'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = $request->except('_method', '_token');
        $point = Point::find($id);
        $point->update($data);
        return redirect()->route('point.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Point::destroy($id);
        return redirect()->route('point.index');
    }

    public function moveUp($id)
    {
        $this->repository->moveUp($id);
        $checklist = $this->repository->findById($id)->checklist;
        return redirect()->back();
    }
    public function moveDown($id)
    {
        $this->repository->moveDown($id);
        $checklist = $this->repository->findById($id)->checklist;
        return redirect()->back();
    }

    public function setStatus($id, $status)
    {
        $point = $this->repository->getById($id);
        $point->status = $status;
        $checklist = $point->checklist;
        $point->save();
        return redirect()->back();
    }
}
