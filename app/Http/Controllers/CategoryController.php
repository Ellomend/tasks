<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Category;
use App\Repositories\Category\DbCategoryRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class CategoryController extends Controller
{
	/**
	 * @var DbCategoryRepository
	 */
	private $repository;

	/**
	 * CategoryController constructor.
	 *
	 * @param DbCategoryRepository $repository
	 */
	public function __construct(DbCategoryRepository $repository) {
		$this->repository = $repository;
	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
		$category = $this->repository->getOwned();

        return view('category.index', compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $this->repository->createFromFormRequest($request);

        Session::flash('flash_message', 'Category added!');

        return redirect('category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {

    	$category = $this->repository->getById($id);
        return view('category.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {

    	$category = $this->repository->getById($id);
        return view('category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $this->repository->updateFromFormRequest($id, $request);

        Session::flash('flash_message', 'Category updated!');

        return redirect('category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
	    $this->repository->delete($id);

        Session::flash('flash_message', 'Category deleted!');

        return redirect('category');
    }

	public function getDelete( $id ) {
		$this->repository->delete($id);
		Session::flash('flash_message', 'Category deleted!');
		return redirect()->route('category.index');
	}
}
