<?php

namespace App\Http\Controllers;

use App\Routine;
use App\Chore;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChoreController extends Controller
{

	public function index() {
		$chores = Chore::all();

		return view( 'routines.chores.index', compact( 'chores' ) );
	}

	public function create() {
		$user = Auth::user();
		$routines = Routine::all();
		if($user)
		{
			$routines = $user->routines;
		}

		return view( 'routines.chores.create', compact('routines'));
	}

	public function store( Request $request ) {
		$data = $request->except( '_token', '_method' );
		$chore = Chore::create( $data );
		$user = Auth::user();
		$routineId = $request->get('routine_id');
		$routine = Routine::find($routineId);
		if ( $routine )
		{
			$chore->routine()->associate( $routine );
			$chore->save();
		}

		return redirect()->route('chore.index');
	}

	public function edit( $id ) {
		$chore = Chore::find( $id );
		$routines = Routine::all();

		return view( 'routines.chores.edit', compact( 'chore', 'routines' ) );


	}


	public function update( Request $request, $id ) {
		$chore = Chore::find($id);
		$chore->update($request->except('_token', '_method'));
		return redirect()->route('chore.index');
	}

	public function show( $id ) {
		$chore = Chore::find($id);
		return view('routines.chores.show', compact('chore'));
	}

	public function destroy( $id ) {
		Chore::destroy($id);
		return redirect()->route('chore.index');
	}
    //
}
