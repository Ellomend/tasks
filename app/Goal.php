<?php

namespace App;

use App\Models\Traits\HasOwner;
use App\Traits\HasCarbonDatesConstraints;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @property Carbon carbon_start
 * @property string type
 * @property int count
 * @property array weekdays
 * @property int duration
 * @property Collection steps
 * @property string name
 * @property string description
 * @property int id
 */
class Goal extends Model
{
    use HasOwner;
    use HasCarbonDatesConstraints;

    const DAILY = 'daily';
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'goals';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    protected $guarded = [];
    protected $casts = [
        'weekdays' => 'array'
    ];

    //relationships

    public function blocks()
    {
        return $this->belongsToMany('\App\Block');
    }

    public function category()
    {
        return $this->belongsTo('\App\Category', 'category_id');
    }

    public function owner()
    {
        return $this->belongsTo('\App\User', 'user_id');
    }

    public function steps()
    {
        return $this->hasMany('App\Step');
    }

    public function getIsActiveAttribute()
    {
        $allSteps = $this->steps;
        $activeSteps = $allSteps->filter(function ($step) {
            if ($step->state == 'active') {
                return true;
            }
        });
        if (count($activeSteps) > 0) {
            return true;
        }
        return false;
    }

    public function getCarbonEndAttribute()
    {
        return null;
    }

}
