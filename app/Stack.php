<?php

namespace App;

use App\Models\Traits\HasOwner;
use Illuminate\Database\Eloquent\Model;

class Stack extends Model
{
        use HasOwner;
    //
	protected $guarded = [];

	public function owner(  ) {
		return $this->belongsTo('App\User', 'user_id');
	}

    public function items()
    {
        return $this->hasMany('App\Item');
    }
}
