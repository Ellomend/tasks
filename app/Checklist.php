<?php

namespace App;

use App\Models\Traits\HasOwner;
use Illuminate\Database\Eloquent\Model;

class Checklist extends Model
{
    use HasOwner;
    /*
     * Checklist type
     * 0 - blueprint
     * 1 - regular checklist
     * 2 - running
     * 3 - completed
     * */
    protected $guarded = [];

    public function points()
    {
        return $this->hasMany('App\Point');
    }

    public function getBlueprintAttribute()
    {
        if ($this->type == 0) {
            return true;
        }
        return false;
    }


    public function getStatusAttribute()
    {
        switch ($this->type) {
            case 0:
                return 'blueprint';
            case 1:
                return 'regular';
            case 2:
                return 'running';
            case 3:
                return 'complete';
            default:
                return 'unknown beast';
        }
    }

    public function getOrderedPointsAttribute()
    {
        return $this->points->sortBy('order');
    }
}
