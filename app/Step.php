<?php

namespace App;

use App\Models\Calendar\CalendarEvent;
use Carbon\Carbon;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;

class Step extends Model implements CalendarEvent
{
    use Filterable;
    public $done_statuses = ['done', 'nailed', 'failed'];
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'steps';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $guarded = [];


    protected $appends = [
        'start',
        'calendar_start',
        'calendar_end',
        'end',
        'label'
    ];

    public function goal()
    {
        return $this->belongsTo('App\Goal', 'goal_id');
    }

    public function getStartAttribute()
    {
        return $this->start_date;
    }

    public function getEndAttribute()
    {
        return Carbon::parse($this->end_date)->toDateString();
    }

    public function getLabelAttribute()
    {
        return $this->name;
    }


    public function getStateAttribute($value)
    {
        $today = Carbon::today();
        $stepStart = Carbon::parse($this->start_date);
        $stepEnd = Carbon::parse($this->end_date);
        if (in_array($this->status, $this->done_statuses)) {
            return $this->status;
        }
        if ($today->lt($stepStart)) {
            return 'future';
        }
//        dd($today, $stepEnd, $today->gt($stepEnd));
        if ($today->gt($stepEnd) && !in_array($this->status, $this->done_statuses)) {
            return 'late';
        }
        if ($today->gte($stepStart) && $today->lte($stepEnd) && $this->status != 'skipped' && !in_array($this->status, $this->done_statuses)) {
            return 'active';
        }
//        dd($today, $stepEnd, ($today->gt($stepEnd)));
        return 'error';

    }

    public function getCalendarTitleAttribute()
    {
        return $this->name;
    }

    public function getCalendarStartAttribute()
    {
        return $this->start_date;
    }

    public function getCalendarEndAttribute()
    {
        return Carbon::parse($this->end_date)->addDay(1)->toDateString();
    }


    public function getCarbonStartAttribute()
    {
        return Carbon::parse($this->start_date);
    }

    public function getCarbonEndAttribute()
    {
        return Carbon::parse($this->end_date);
    }

    public function skip()
    {
        $now = Carbon::now();
        $this->status = 'skipped';
        $this->finished = $now->toDateString();
        $this->save();
    }
}
