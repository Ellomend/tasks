<?php

namespace App\Policies;

use App\User;
use App\Context;
use Illuminate\Auth\Access\HandlesAuthorization;

class ContextPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the context.
     *
     * @param  \App\User  $user
     * @param  \App\Context  $context
     * @return mixed
     */
    public function view(User $user, Context $context)
    {
        return $user->id == $context->user_id;
    }

    /**
     * Determine whether the user can create contexts.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
	    return true;
    }

    /**
     * Determine whether the user can update the context.
     *
     * @param  \App\User  $user
     * @param  \App\Context  $context
     * @return mixed
     */
    public function update(User $user, Context $context)
    {
	    return $user->id == $context->user_id;

    }

    /**
     * Determine whether the user can delete the context.
     *
     * @param  \App\User  $user
     * @param  \App\Context  $context
     * @return mixed
     */
    public function delete(User $user, Context $context)
    {
        //
	    return $user->id == $context->user_id;
    }

	public function edit( User $user, Context $context ) {
		return $user->id == $context->user_id;

	}
}
