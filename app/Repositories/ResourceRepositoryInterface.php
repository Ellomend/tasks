<?php


namespace App\Repositories;

use Illuminate\Http\Request;

interface ResourceRepositoryInterface
{
    public function getAll();
    public function getById($id);
    public function getOwned();
    public function create(array $attributes);
    public function update($id, array $attributes);
    public function delete($id);
    public function createFromFormRequest(Request $request);
    public function updateFromFormRequest($id, Request $request);
}
