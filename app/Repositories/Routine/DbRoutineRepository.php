<?php


namespace App\Repositories\Routine;

use App\Chore;
use App\Repositories\DbRepositoryAbstract;
use App\Repositories\ResourceRepositoryInterface;
use App\Routine;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DbRoutineRepository extends DbRepositoryAbstract implements ResourceRepositoryInterface
{
    /**
     * @var Routine
     */
    public $model;
    /**
     * @var Request
     */
    private $request;

    /**
     * DbRoutineRepository constructor.
     *
     * @param Routine $model
     * @param Request $request
     */
    public function __construct(Routine $model)
    {
        $this->model = $model;
    }



    public function complete($id)
    {
        $routine = Routine::find($id);
        $routine->complete();
    }

    public function ownedComplete()
    {
        return Routine::filter(['actual' => 'test'])->get();
    }

    public function updateByRequest($id, Request $request)
    {
        $data = $this->getRequestData($request);
        $task = $this->getById($id);
        $this->sync($request, $task);
        $task->update($data);
        return $task;
    }

    public function getRequestData(Request $request)
    {
        $data = $request->except('_token', 'contexts', 'blocks', '_method');
        return $data;
    }

    public function sync(Request $request, $task)
    {
        $blocks = $request->get('blocks');
        if ($blocks) {
            $task->blocks()->sync($blocks);
        }
        $contexts = $request->get('contexts');
        if ($contexts) {
            $task->contexts()->sync($contexts);
        }
        $task->save();
        return $task;
    }

}
