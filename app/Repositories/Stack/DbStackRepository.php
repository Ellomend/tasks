<?php


namespace App\Repositories\Stack;

use App\Repositories\DbRepositoryAbstract;
use App\Repositories\ResourceRepositoryInterface;
use App\Stack;

class DbStackRepository extends DbRepositoryAbstract implements ResourceRepositoryInterface
{
    /**
     * @var Stack
     */
    public $model;

    /**
     * DbStackRepository constructor.
     * @param Stack $model
     */
    public function __construct(Stack $model)
    {
        $this->model = $model;
    }
}
