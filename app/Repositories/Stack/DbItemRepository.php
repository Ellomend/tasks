<?php


namespace App\Repositories\Stack;


use App\Item;
use App\Repositories\DbRepositoryAbstract;
use App\Repositories\ResourceRepositoryInterface;

class DbItemRepository extends DbRepositoryAbstract implements ResourceRepositoryInterface
{
    public $model;

    /**
     * DbItemRepository constructor.
     * @param Item $model
     */
    public function __construct(Item $model)
    {
        $this->model = $model;
    }
}