<?php


namespace App\Repositories;


use App\Block;
use App\Category;
use App\Checklist;
use App\Context;
use App\Goal;
use App\Mood;
use App\Point;
use App\Project;
use App\Repositories\Block\DbBlockRepository;
use App\Repositories\Category\DbCategoryRepository;
use App\Repositories\Checklist\DbChecklistRepository;
use App\Repositories\Checklist\DbPointRepository;
use App\Repositories\Context\DbContextRepository;
use App\Repositories\Goal\DbGoalRepository;
use App\Repositories\Mood\DbMoodRepository;
use App\Repositories\Project\DbProjectRepository;
use App\Repositories\Routine\DbRoutineRepository;
use App\Repositories\Stack\DbStackRepository;
use App\Repositories\Tasks\DbTaskRepository;
use App\Routine;
use App\Stack;
use App\Task;
use Illuminate\Http\Request;

abstract class GlobalRepositoryAbstract
{


    /**
     * GlobalRepositoryAbstract constructor.
     */
    public function __construct()
    {
        $this->tasks = new DbTaskRepository((new Task()));
        $this->contexts = new DbContextRepository((new Context));
        $this->blocks = new DbBlockRepository((new Block));
        $this->categories = new DbCategoryRepository((new Category));
        $this->checklists = new DbChecklistRepository((new Checklist));
        $this->points = new DbPointRepository((new Point));
        $this->goals = new DbGoalRepository((new Goal));
        $this->moods = new DbMoodRepository((new Mood));
        $this->projects = new DbProjectRepository((new Project));
        $this->routines = new DbRoutineRepository((new Routine));
        $this->stack = new DbStackRepository((new Stack));
    }
}