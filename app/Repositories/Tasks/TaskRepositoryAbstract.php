<?php


namespace App\Repositories\Tasks;

use App\Repositories\DbRepositoryAbstract;
use App\Task;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

abstract class TaskRepositoryAbstract extends DbRepositoryAbstract
{
    public $model;

    /**
     * DbTaskRepository constructor.
     *
     * @param Task $model
     */
    public function __construct(Task $model)
    {
        $this->model = $model;
    }

    public function getQuery()
    {
        return $this->model->where('user_id', Auth::user()->id);
    }

    public function late()
    {
        $tasks = $this->getQuery();
        $today = Carbon::today()->toDateString();
        $tasks->where('end', '<', $today);
        $tasks->where('status', '=', 'active');
        return $tasks->get();
    }

    public function due()
    {
        $tasks = $this->getQuery();
        $today = Carbon::today()->toDateString();
        $tasks->where('end', '>=', $today);
        $tasks->where('start', '<=', $today);
        $tasks->where('status', '=', 'active');
        return $tasks->get();
    }

    public function timeless()
    {
        $tasks = $this->getQuery();
        $tasks->whereNull('end');
        $tasks->whereNull('start');
        return $tasks->get();
    }
}
