<?php


namespace App\Repositories\Tasks\Filter;

use App\Block;
use App\Context;

class TaskFilter
{
    /**
     * @var Context
     */
    private $context;
    /**
     * @var Block
     */
    private $block;


    /**
     * TaskFilter constructor.
     *
     * @param Context $context
     * @param Block   $block
     */
    public function __construct(Context $context, Block $block)
    {
        $this->context = $context;
        $this->block = $block;
        $this->models = [$this->context, $this->block];
    }
}
