<?php


namespace App\Repositories\Tasks\Filter;

use App\Block;
use App\Category;
use App\Context;
use App\Mood;
use App\Project;
use App\Repositories\DbFilterRepositoryAbstract;
use App\Repositories\DbFilterRepositoryInterface;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class DbTaskFilterRepository extends DbFilterRepositoryAbstract
{
    /**
     * @var Request
     */
    public $request;
    /**
     * @var Task
     */
    public $model;
    /**
     * @var Collection
     */
    public $contexts;
    public $selectedContextsId = [];
    public $filteredCollection;
    public $collection;
    public $blocks;
    public $categories;
    public $moods;
    public $projects;

    /**
     * DbTaskFilterRepository constructor.
     *
     * @param Request $request
     * @param Task $model
     */
    public function __construct(Request $request, Task $model)
    {
        $this->request = $request;
        $this->model   = $model;
    }

    public function init()
    {
        $this->setContexts();

        $this->setCollection();
        $this->filterCollection();
        return $this;
    }


    /**
     * @internal param mixed $contexts
     */
    public function setContexts()
    {
        $this->contexts     =  Context::where('user_id', Auth::user()->id)->get();
        $this->blocks     =  Block::where('user_id', Auth::user()->id)->get();
        $this->categories     =  Category::where('user_id', Auth::user()->id)->get();
        $this->moods     =  Mood::where('user_id', Auth::user()->id)->get();
        $this->projects     =  Project::where('user_id', Auth::user()->id)->get();
    }



    public function setCollection(): void
    {
        $this->collection = $this->model
            ->where('user_id', Auth::user()->id)
            ->with('contexts')
            ->get();
    }





    public function filterCollection(): void
    {
        $this->filteredCollection = collect();
        foreach ($this->collection as $item) {
            foreach ($item->contexts as $context) {
                if (in_array($context->id, $this->selectedContextsId)) {
                    $this->filteredCollection[] = $item;
                }
            }
        }
    }
}
