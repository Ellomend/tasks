<?php

namespace App\Repositories\Tasks;


interface TaskRepositoryInterface {
	public function getAll();
	public function getOwned();
	public function getById($id);
	public function create(array $attributes);
	public function update($id, array $attributes);
	public function delete($id);
}