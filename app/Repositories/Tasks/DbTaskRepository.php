<?php

namespace App\Repositories\Tasks;

use App\Models\Status\StatusEnum;
use App\Repositories\DbRepositoryAbstract;
use App\Task;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DbTaskRepository extends TaskRepositoryAbstract implements TaskRepositoryInterface
{


    public function createFromFormRequest(Request $request)
    {
        // in case of contexts and blocks we have many to many relationship
        // so we have multiple select in form and thus we must handle attaching separately
        $data = $this->getRequestData($request);
        $task = $this->create($data);


        $this->sync($request, $task);
        $task->owner()->associate(Auth::user());
        $task->save();

        return $task;
    }

    public function updateFromFormRequest($id, Request $request)
    {
        $data = $this->getRequestData($request);
        $task = $this->getById($id);
        $this->sync($request, $task);
        $task->update($data);
        return $task;
    }

    /**
     * @param Request $request
     *
     * @return array
     * action
     */
    public function getRequestData(Request $request)
    {
        $data = $request->except('_token', 'contexts', 'blocks', '_method');
        return $data;
    }

    /**
     * @param Request $request
     * @param         $task
     * action
     */
    public function sync(Request $request, $task)
    {
        $blocks = $request->get('blocks');
        if ($blocks) {
            $task->blocks()->sync($blocks);
        }
        $contexts = $request->get('contexts');
        if ($contexts) {
            $task->contexts()->sync($contexts);
        }
        $task->save();
        return $task;
    }

    public function getOwned()
    {
        $user = Auth::user();
        $owned = $this->model
            ->select()
            ->where('status', '!=', StatusEnum::STATUS_ARCHIVED)
            ->where('user_id', $user->id)
            ->with('category')
            ->with('contexts')
            ->with('blocks')
            ->with('mood')
            ->with('project')
            ->get();
        return $owned;
    }

    public function getOwnedWithArchived()
    {
        $user = Auth::user();
        $owned = $this->model
            ->select()
            ->where('user_id', $user->id)
            ->with('category')
            ->with('contexts')
            ->with('blocks')
            ->with('mood')
            ->with('project')
            ->get();
        return $owned;
    }
    public function getToday()
    {
        $tasks = $this->getOwned();
        $today = $tasks->filter(function ($task) {
            $now = Carbon::today();
            $taskStart = Carbon::parse($task->start);
            $taskEnd = Carbon::parse($task->end);
            if ($taskStart->lte($now) && $taskEnd->gte($now)) {
                return true;
            }
            return false;
        });
        return $today;
    }
}
