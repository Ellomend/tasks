<?php


namespace App\Repositories\Project;


use App\Project;
use App\Repositories\DbRepositoryAbstract;
use App\Repositories\ResourceRepositoryInterface;

class DbProjectRepository extends DbRepositoryAbstract implements ResourceRepositoryInterface {


	/**
	 * DbContextRepository constructor.
	 *
	 * @param Project $model
	 */
	public function __construct(Project $model) {
		$this->model = $model;
	}

}