<?php


namespace App\Repositories\Checklist;

use App\Checklist;
use App\Repositories\DbRepositoryAbstract;
use App\Repositories\ResourceRepositoryInterface;
use Illuminate\Http\Request;

class DbChecklistRepository extends DbRepositoryAbstract implements ResourceRepositoryInterface
{
    protected $startOrder;
    protected $sortStart;


    /**
     * DbChecklistRepository constructor.
     * @param Checklist $model
     */
    public function __construct(Checklist $model)
    {
        $this->model = $model;
    }

    public function createByRequest(Request $request)
    {
        $data    = $this->getRequestData($request);
        $checklist = $this->model->create($data);
        $this->assignOwner($checklist);
        $checklist->type = 0;
        $checklist->save();
        return $checklist;
    }

    public function getLastItemOrder($id)
    {
        $allPoints = $this->getById($id)->points()->orderBy('order')->get();

        $allPoints = $this->reorderPoints($allPoints);

        $allPoints->each(function ($point) {
            $point->save();
        });
        $lastPoint = $allPoints->max('order');
        return $lastPoint;
    }
    public function getLastItemOrderCheap($id)
    {
        $allPoints = $this->getById($id)->points()->orderBy('order')->get();
        $lastPoint = $allPoints->sortBy('order')->last();

        return $lastPoint;
    }

    /**
     * @param $allPoints
     * @return mixed
     * action
     */
    public function reorderPoints($allPoints)
    {
        $allPoints = $this->fixOrder($allPoints);
        $this->brushSort($allPoints);
        return $allPoints;
    }

    /**
     * @param $allPoints
     * action
     */
    public function brushSort($allPoints): void
    {
        $this->sortStart = 0;
        $sorted = $allPoints->sortBy('order');
        $sorted = $sorted->each(function ($point) {
            $point->order = $this->sortStart;
            $this->sortStart = $this->sortStart + 10;
        });
    }

    /**
     * @param $allPoints
     * @return mixed
     * action
     */
    public function fixOrder($allPoints)
    {
        $this->startOrder = 10;
        $allPoints = $allPoints->each(function ($point) {
            if ($point->order == 0) {
                $point->order = $this->startOrder;
                $this->startOrder = $this->startOrder + 10;
            } else {
                $point->order = $point->order + $this->startOrder;
            }
        });
        return $allPoints;
    }

    public function getPreviousPoint($checklist, $point)
    {
        $allPoints = $this->getById($checklist->id)->points()->orderBy('order')->get();
        $last = $allPoints->filter(function ($item) use ($point) {
            if ($item->order < $point->order) {
                //                var_dump($item->order);
                return true;
            }
            return false;
        });
        $last = $last->sortBy('order')->last();
        return $last;
    }

    public function getOwnRunning()
    {
        $checklists = $this->getOwned();
        $running = $checklists->filter(function ($item) {
            if ($item->status == 'running') {
                return true;
            }
        });
        return $running;
    }
}
