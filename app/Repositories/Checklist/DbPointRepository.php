<?php


namespace App\Repositories\Checklist;

use App\Point;
use App\Repositories\DbRepositoryAbstract;
use App\Repositories\ResourceRepositoryInterface;
use Illuminate\Http\Request;
use App\Checklist;
use Illuminate\Support\Facades\Auth;

class DbPointRepository extends DbRepositoryAbstract implements ResourceRepositoryInterface
{
    /**
     * @var DbChecklistRepository
     */
    private $checklistRepository;
    /**
     * @var Checklist
     */
    private $checklist;


    /**
     * DbPointRepository constructor.
     * @param Point $model
     * @param DbChecklistRepository $checklistRepository
     * @internal param Checklist $checklist
     * @internal param DbChecklistRepository $checklistRepository
     */
    public function __construct(Point $model)
    {
        $this->model = $model;
        $this->checklistRepository = new DbChecklistRepository(new Checklist());
    }

    public function createByRequest(Request $request)
    {

        $data    = $this->getRequestData($request);
        $point = $this->model->create($data);
        $getLastOrder = $this->checklistRepository->getLastItemOrder($point->checklist->id);
        $nextOrder = $getLastOrder+10;
        $point->order = $nextOrder;
        $point->save();
        return $point;
    }

    public function getOwned()
    {
        $owned = collect();
        $user = Auth::user();
        $ownedChecklists = $this->checklistRepository->getOwned();
        foreach ($ownedChecklists as $checklist) {
            foreach ($checklist->points as $point) {
                $owned->push($point);
            }
        }
        return $owned;
    }

    public function moveUp($id)
    {

        $point = $this->findById($id);
        $checklist = $point->checklist;
        $previous = $this->checklistRepository->getPreviousPoint($checklist, $point);

        $point->order = $point->order - 10;
        $previous->order = $previous->order + 10;
        $point->save();
        $previous->save();
    }

    public function moveDown($id)
    {
        $pointToDown = $this->findById($id);
        $nextOrder = $pointToDown->order + 10;
        $pointToUp = $this->getOwned()->where('checklist', '=', $pointToDown->checklist)
        ->where('order', '=', $nextOrder);
        $pointToUp = $pointToUp->first();
        $pointToDown->order = $pointToDown->order + 10;
        $pointToUp->order = $pointToUp->order - 10;
        $pointToUp->save();
        $pointToDown->save();
    }


}