<?php


namespace App\Repositories\Mood;


use App\Mood;
use App\Repositories\DbRepositoryAbstract;
use App\Repositories\ResourceRepositoryInterface;

class DbMoodRepository extends DbRepositoryAbstract implements ResourceRepositoryInterface {


	/**
	 * DbContextRepository constructor.
	 *
	 * @param Mood $model
	 */
	public function __construct(Mood $model) {
		$this->model = $model;
	}

}