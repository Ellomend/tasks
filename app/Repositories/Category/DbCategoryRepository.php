<?php


namespace App\Repositories\Category;

use App\Category;
use App\Repositories\DbRepositoryAbstract;
use App\Repositories\ResourceRepositoryInterface;

class DbCategoryRepository extends DbRepositoryAbstract implements ResourceRepositoryInterface
{


    /**
     * DbCategoryRepository constructor.
     *
     * @param Category $model
     */
    public function __construct(Category $model)
    {
        $this->model = $model;
    }
}
