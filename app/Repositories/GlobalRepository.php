<?php


namespace App\Repositories;

class GlobalRepository extends GlobalRepositoryAbstract
{
    public function getFilterObjects()
    {
        $filters['blocks'] = $this->blocks->getOwned();
        $filters['categories'] = $this->categories->getOwned();
        $filters['contexts'] = $this->contexts->getOwned();
        $filters['moods'] = $this->moods->getOwned();
        $filters['projects'] = $this->projects->getOwned();

        return $filters;
    }

}