<?php


namespace App\Repositories\Goal;

use App\Goal;
use App\Repositories\DbRepositoryAbstract;
use App\Repositories\ResourceRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DbGoalRepository extends DbRepositoryAbstract implements ResourceRepositoryInterface
{

    /**
     * DbGoalRepository constructor.
     * @param Goal $model
     */
    public function __construct(Goal $model)
    {
        $this->model = $model;
    }

    public function getRequestData(Request $request)
    {
        $data = $request->except('_token', '_method');
        return $data;
    }

    public function updateByRequest($id, Request $request)
    {
        $data = $this->getRequestData($request);
        $task = $this->getById($id);
        $this->sync($request, $task);
        unset($data['blocks']);
        $task->update($data);
        return $task;
    }

    public function createFromFormRequest(Request $request)
    {
        $data = $this->getRequestData($request);
        $createData = $data;
        unset($createData['blocks']);
        $goal = $this->model->create($createData);
        $this->assignOwner($goal);
        $this->sync($request, $goal);
        return $goal;
    }


    private function sync($request, $task)
    {
        $blocks = $request->get('blocks');
        if ($blocks) {
            $task->blocks()->sync($blocks);
        }
        $contexts = $request->get('contexts');
        if ($contexts) {
            $task->contexts()->sync($contexts);
        }
        $task->save();
        return $task;
    }

    public function completeCurrentStep($id)
    {
        $goal = $this->getById($id);
        $currentStep = $this->getCurrentStep($goal);
        if ($currentStep) {
            $currentStep->finished = Carbon::today()->toDateString();
            $currentStep->status = 'done';
            $currentStep->save();
            return $currentStep;
        }
        return false;
    }

    private function getCurrentStep($goal)
    {
        $today = Carbon::today()->toDateString();
        $steps = $goal->steps()
            ->where('start_date', '<=', $today)
//            ->Where('end_date', '>=', $today)
            ->Where('status', '=', 'active')
            ->get()->first();
        return $steps;
    }
}
