<?php


namespace App\Repositories\Goal;

use App\Repositories\DbRepositoryAbstract;
use App\Repositories\ResourceRepositoryInterface;
use App\Repositories\Routine\DbRoutineRepository;
use App\Routine;
use App\Step;
use Illuminate\Http\Request;

class DbStepRepository extends DbRepositoryAbstract implements ResourceRepositoryInterface
{

    /**
     * DbStepRepository constructor.
     * @param Step $model
     */
    public function __construct(Step $model)
    {
        $this->model = $model;
    }

    public function getOwned()
    {
        $owned = Step::filter(['goal' => 'true'])->get();
        return $owned;
    }

    public function createByRequest(Request $request)
    {
        $data    = $this->getRequestData($request);
        $context = $this->model->create($data);
        return $context;
    }


}
