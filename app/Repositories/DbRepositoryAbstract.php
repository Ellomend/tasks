<?php
/**
 * {namespace}_LogMailer
 *
 * @category    cyberhull
 * @package     Cyberhull_LogMailer
 * @author      aleksander.bogonosov@cyberhull.com
 */

namespace App\Repositories;

use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * @property  Task model
 */
abstract class DbRepositoryAbstract
{

    /**
     * @param $id
     *
     * @return mixed
     * action
     */
    public function getById($id)
    {
        return $this->model->find($id);
    }

    public function findById($id)
    {
        return $this->getById($id);
    }

    public function getAll()
    {
        return $this->model->all();
    }

    public function getOwned()
    {
        $user = Auth::user();
        $owned = $this
            ->model
            ->select()
            ->where('user_id', $user->id)
            ->get();
        return $owned;
    }

    public function create(array $attributes)
    {
        return $this->model->create($attributes);
    }

    public function update($id, array $attributes)
    {
        $entity = $this->model->findOrFail($id);
        $entity->udpate($attributes);
        return $entity;
    }

    public function delete($id)
    {
        $entity = $this->model->findOrFail($id);
        $entity->delete();
        return true;
    }


    public function getRequestData(Request $request)
    {
        $data = $request->except('_token', '_method');

        return $data;
    }


    public function assignOwner($entity)
    {
        $entity->user_id = Auth::user()->id;
        $entity->save();
    }

    public function createByRequest(Request $request)
    {
        $data    = $this->getRequestData($request);
        $context = $this->model->create($data);
        $this->assignOwner($context);
        return $context;
    }


    public function updateByRequest($id, Request $request)
    {
        $data    = $this->getRequestData($request);
        $context = $this->getById($id);
        $context->update($data);

        return $context;
    }

    public function createFromFormRequest(Request $request)
    {
        $context = $this->createByRequest($request);

        return $context;
    }

    public function updateFromFormRequest($id, Request $request)
    {
        return $this->updateByRequest($id, $request);
    }
}
