<?php


namespace App\Repositories\Block;

use App\Block;
use App\Repositories\DbRepositoryAbstract;
use App\Repositories\ResourceRepositoryInterface;
use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class DbBlockRepository extends DbRepositoryAbstract implements ResourceRepositoryInterface
{


    /**
     * DbBlockRepository constructor.
     *
     * @param Block $model
     */
    public function __construct(Block $model)
    {
        $this->model = $model;
    }



    public function getCurrentBlocks()
    {
        /** @var Collection $allBlocks */
        $allBlocks = $this->getOwned();
        $currentTime = Carbon::now();

        $currentBlocks = $allBlocks->filter(function ($item) use ($currentTime) {
            $blockStartTime = Carbon::parse($item->start);
            $blockEndTime = Carbon::parse($item->end);
            if ($currentTime->lessThanOrEqualTo($blockEndTime) && $currentTime->greaterThanOrEqualTo($blockStartTime)) {
                return true;
            }
            return false;
        });

        return $currentBlocks;
    }

    public function getOwned()
    {
        $user = Auth::user();
        $owned = $this
            ->model
            ->select()
            ->where('user_id', $user->id)
            ->with('routines')
            ->with('goals')
            ->with('tasks')
            ->with('owner')
            ->get();
        return $owned;
    }


}
