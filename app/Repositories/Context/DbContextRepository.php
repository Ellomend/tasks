<?php


namespace App\Repositories\Context;


use App\Context;
use App\Repositories\DbRepositoryAbstract;
use App\Repositories\ResourceRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DbContextRepository extends DbRepositoryAbstract implements ResourceRepositoryInterface {


	/**
	 * DbContextRepository constructor.
	 *
	 * @param Context $model
	 */
	public function __construct(Context $model) {
		$this->model = $model;
	}



}