<?php namespace App\ModelFilters;

use Carbon\Carbon;
use EloquentFilter\ModelFilter;
use Illuminate\Support\Facades\Auth;

class RoutineFilter extends ModelFilter
{
    /**
    * Related Models that have ModelFilters as well as the method on the ModelFilter
    * As [relatedModel => [input_key1, input_key2]].
    *
    * @var array
    */
    public $relations = [];

    public function setup()
    {
        return $this->where('user_id', Auth::user()->id);
    }
    public function actual()
    {
        $currentTime = Carbon::now()->toDateTimeString();
        return $this->where('next', '<=', $currentTime)
            ->orWhere('next', null);
    }
}
