<?php namespace App\ModelFilters;

use EloquentFilter\ModelFilter;
use Illuminate\Support\Facades\Auth;

class TaskFilter extends ModelFilter
{
    /**
    * Related Models that have ModelFilters as well as the method on the ModelFilter
    * As [relatedModel => [input_key1, input_key2]].
    *
    * @var array
    */
    public $relations = [];

    public function status($status)
    {
        return $this->where(function ($q) use ($status) {
            return $q->where('status', 'LIKE', "%$status%");
        });
    }

    public function setup()
    {
        return $this->where('user_id', Auth::user()->id);
    }

    public function start($start)
    {
        return $this->where('start', '>=', $start);
    }
    public function end($end)
    {
        return $this->where('end', '<=', $end);
    }

    public function contexts($ids)
    {
        return $this->whereHas('contexts', function ($query) use ($ids) {
            return $query->whereIn('context_id', $ids);
        });
    }

    public function blocks($ids)
    {
        return $this->whereHas('blocks', function ($query) use ($ids) {
            return $query->whereIn('block_id', $ids);
        });
    }

    public function categoryId($id)
    {
        return $this->where('category_id', $id);
    }

    public function moodId($id)
    {
        return $this->where('mood_id', $id);
    }

    public function projectId($id)
    {
        return $this->where('project_id', $id);
    }
}
