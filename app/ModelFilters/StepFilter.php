<?php namespace App\ModelFilters;

use EloquentFilter\ModelFilter;
use Illuminate\Support\Facades\Auth;

class StepFilter extends ModelFilter
{
    public function goal()
    {
        $id = Auth::user()->id;
        $this->related('goal', 'user_id', $id);
        return $this;
    }
}
