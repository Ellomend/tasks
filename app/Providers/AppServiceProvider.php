<?php

namespace App\Providers;

use App\Models\Filter;
use App\Models\Master;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->singleton('Master', Master::class);


        view()->composer('resources.entities.task.modal.fields.contexts', function($view){
            $view->with('contexts', app()->make('Master')->getContexts());
        });
        view()->composer('resources.entities.task.modal.fields.blocks', function($view){
            $view->with('blocks', app()->make('Master')->getBlocks());
        });

        view()->composer('resources.entities.task.modal.fields.categories', function($view){
            $view->with('categories', app()->make('Master')->getCategories());
        });

        view()->composer('resources.entities.task.modal.fields.moods', function($view){
            $view->with('moods', app()->make('Master')->getMoods());
        });

        view()->composer('resources.entities.task.modal.fields.projects', function($view){
            $view->with('projects', app()->make('Master')->getProjects());
        });

        view()->composer('*', function ($view) {
           $view->with('master', app()->make('Master'));
        });





    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
