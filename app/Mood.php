<?php

namespace App;

use App\Models\Traits\HasOwner;
use Illuminate\Database\Eloquent\Model;

class Mood extends Model
{

    use HasOwner;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'moods';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'value'];

	public function owner() {
		return $this->belongsTo('App\User', 'user_id');
	}


}
