<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    //
	protected $guarded = [];

	public function stack() {
		return $this->belongsTo('App\Stack', 'stack_id');
	}
}
