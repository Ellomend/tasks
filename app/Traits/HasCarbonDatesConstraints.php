<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 22.02.2017
 * Time: 4:35
 */

namespace App\Traits;


use Carbon\Carbon;

trait HasCarbonDatesConstraints
{
    public function getCarbonStartAttribute()
    {
        return Carbon::parse($this->start);
    }

    public function getCarbonEndAttribute()
    {
        return Carbon::parse($this->end);
    }
}