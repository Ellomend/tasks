<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 09.03.2017
 * Time: 21:34
 */

namespace App\Models\Status;


interface StatusInterface
{

    public function getStatus();
    public function setStatus(string $status);

}