<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 09.03.2017
 * Time: 21:47
 */

namespace App\Models\Status;


class StatusEnum
{
    const STATUS_NEW = 'active';
    const STATUS_ACTIVE = 'active';
    const STATUS_IN_PROGRESS = 'in progress';
    const STATUS_COMPLETE = 'complete';
    const STATUS_PARTIALLY_COMPLETE = 'incomplete';
    const STATUS_SKIPPED = 'skipped';
    const STATUS_FAILED = 'failed';
    const STATUS_CANCELLED = 'cancelled';
    const STATUS_ARCHIVED = 'archived';
}