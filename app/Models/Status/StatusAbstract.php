<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 09.03.2017
 * Time: 21:34
 */

namespace App\Models\Status;


use Illuminate\Database\Eloquent\Model;

abstract class StatusAbstract implements StatusInterface
{

    public $availableStatuses = [
        StatusEnum::STATUS_ACTIVE,
        StatusEnum::STATUS_COMPLETE,
        StatusEnum::STATUS_PARTIALLY_COMPLETE,
        StatusEnum::STATUS_CANCELLED,
        StatusEnum::STATUS_FAILED,
        StatusEnum::STATUS_ARCHIVED
    ];
    /**
     * @var Model
     */
    public $model;

    /**
     * StatusAbstract constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function getStatus()
    {
        return $this->model->status;
    }

    public function setStatus(string $status)
    {
        $this->model->status = $status;
        $this->model->save();
        return $this->model->status;
    }
}