<?php

namespace App\Models\Helpers;


use Carbon\Carbon;
use Recurr\Rule;
use Recurr\Transformer\ArrayTransformer;

abstract class RecurrenceAbstract
{

    public $goalFreqs = array(
        'monthly' => 'MONTHLY',
        'weekly'  => 'WEEKLY',
        'daily'   => 'DAILY',
    );
    public $days = array(
        'monday'    => 'MO',
        'tuesday'   => 'TU',
        'wednesday' => 'WE',
        'thursday'  => 'TH',
        'friday'    => 'FR',
        'saturday'  => 'SA',
        'sunday'    => 'SU'
    );

    /**
     * @var Carbon $start
     */
    public $start;
    public $freq;
    public $count;
    public $byDay;
    public $byDayFlag;
    public $rule;
    /**
     * @var Carbon $end
     */
    public $end;

    /**
     * RecurrenceAbstract constructor.
     */
    public function __construct()
    {
        $this->rule = new Rule();
        $this->byDayFlag = false;
    }


    public function setFreq($freq)
    {
        $frequency = $this->goalFreqs[$freq];
        $this->freq = $frequency;
        return $this;
    }

    public function setByDay($byDay)
    {

            foreach ($byDay as $dayKey) {
                $this->byDay[] = $this->days[$dayKey];
            }
        return $this;
    }


    public function setStart(Carbon $start)
    {
        $this->start = $start;
        return $this;
    }

    public function setEnd(Carbon $end)
    {
        $this->end = $end;
    }

    public function setCount($count)
    {
        $this->count = $count;
    }

    public function build()
    {
        $rule = $this->rule;
        $rule->setStartDate($this->start);
        $rule->setCount($this->count);
        $rule->setFreq($this->freq);
        $rule->setInterval(1);
        if ($this->byDayFlag) {
            $rule->setByDay($this->byDay);
        }
        $transformer = new ArrayTransformer();
        return $transformer->transform($rule);
    }

    /**
     * @param int $duration
     * @return $this
     * action
     */
    public function setDuration(int $duration)
    {
        $end = clone $this->start;
        $end->addDays($duration);
        $this->end = $end;
        $this->rule->setEndDate($end);
        return $this;
    }

}