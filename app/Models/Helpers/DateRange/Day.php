<?php


namespace App\Models\Helpers\DateRange;

use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;

class Day
{
    /**
     * @var Carbon
     */
    public $date;
    /**
     * @var array
     */
    public $events;

    /**
     * Day constructor.
     * @param Carbon $date
     */
    public function __construct(Carbon $date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * @param mixed $events
     * @return array
     */
    public function setEvents(\Iterator $events)
    {
        foreach ($events as $event) {
            $this->events[] = $event;
        }
        return $this->events;
    }

    public function setEvent($event)
    {
        $this->events[] = $event;
    }
}
