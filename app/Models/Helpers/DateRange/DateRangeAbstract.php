<?php


namespace App\Models\Helpers\DateRange;


use Carbon\Carbon;

class DateRangeAbstract
{

    public $endDate;
    public $dates;
    public $startDate;

    /**
     * @return mixed
     * action
     */
    public function getDates()
    {
        return $this->dates;
    }

    /**
     * @param $events
     * @return array
     * action
     */
    public function assignEvents($events)
    {
        $assignedEvents = [];
        foreach ($this->dates as $day) {
            foreach ($events as $event) {
                $eventStart = $event->carbon_start;
                $eventEnd = $event->carbon_end;
                if ($eventStart && $eventEnd) {
                    if ($eventEnd->gte($day->date) && $eventStart->lte($day->date)) {
                        $day->setEvent($event);
                    }
                }
                if ($event->assign_to && Carbon::parse($event->assign_to)->isSameDay($day->date)) {
                    $day->setEvent($event);
                }
            }
        }
        return $assignedEvents;
    }
}