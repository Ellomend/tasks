<?php
namespace App\Models\Helpers\DateRange;

interface RangeInterface
{
    public function getDates();

    public function assignEvents($events);
}