<?php


namespace App\Models\Helpers\DateRange;

use Carbon\Carbon;

class Week extends DateRangeAbstract implements RangeInterface
{


    /**
     * Week constructor.
     */
    public function __construct()
    {
        $this->startDate = Carbon::parse(date('Y-m-d', strtotime('monday this week')));
        $this->endDate = Carbon::parse(date('Y-m-d', strtotime('sunday this week')));
        for ($date = $this->startDate; $date->lte($this->endDate); $date->addDay()) {
            $this->dates[] = new Day(Carbon::parse($date->toDateString()));
        }
    }


}
