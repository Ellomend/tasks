<?php


namespace App\Models\Helpers\DateRange;

use Carbon\Carbon;

class Month extends DateRangeAbstract implements RangeInterface
{
    /**
     * Month constructor.
     */
    public function __construct()
    {
        $this->startDate = Carbon::parse(date('Y-m-d', strtotime('first day of this month')));
        $this->endDate = Carbon::parse(date('Y-m-d', strtotime('last day of this month')));
        for ($date = $this->startDate; $date->lte($this->endDate); $date->addDay()) {
            $this->dates[] = new Day(Carbon::parse($date->toDateString()));
        }
    }

}