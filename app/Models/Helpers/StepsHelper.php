<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 17.11.2016
 * Time: 17:51
 */

namespace App\Models\Helpers;


use App\Step;
use App\Goal;
use Carbon\Carbon;
use Recurr\RecurrenceCollection;

class StepsHelper
{

    /**->
     * @param Goal $goal
     * @return RecurrenceCollection
     * action
     * @inheritdoc https://github.com/jpmurray/laravel-rrule
     */
    public static function getRecurrences(Goal $goal)
    {

        $recurrence = new Recurr();

        $recurrence->setFreq($goal->type);
        $recurrence->setStart($goal->carbon_start);
        $recurrence->setDuration($goal->duration);
        $recurrence->setCount($goal->count);
        if ($goal->type == $goal::DAILY) {

            $recurrence->setByDay($goal->weekdays);
            $recurrence->byDayFlag = true;
        }
        $recurrences = $recurrence->build();
        return $recurrences;
    }

    /**
     * @param RecurrenceCollection $recurrences
     * @param Goal $goal
     * @return bool action
     * action
     * @internal param $id
     */
    public static function createSteps($recurrences, $goal)
    {


        $collection = collect();


        foreach ($goal->steps as $step) {
            $step->delete();
        }


        foreach ($recurrences->toArray() as $step) {

            $stepCreate = new Step([
                'name' => $goal->name . ' - ' . rand(1000, 100000),
                'description' => $goal->description,
                'start_date' => $step->getStart()->format('Y-m-d'),
                'end_date' => $step->getEnd()->format('Y-m-d'),
                'goal_id' => $goal->id,
                'status' => 'active'
            ]);
            $stepCreate->save();
            $collection->push($stepCreate);
        }


        return $collection;
    }

    public static function skipSteps($steps)
    {
        $skippedSteps = collect();
        foreach ($steps as $step) {
            $endDate = Carbon::parse($step->end_date);
            if ($endDate < Carbon::today()) {
                $step->status = 'skipped';
                $step->finished = Carbon::today()->subDay(1)->toDateString();
                $step->save();
                $skippedSteps->push($step);
            }
        }
        return $skippedSteps;
    }

    public static function getSkippedSteps($steps)
    {
        $skippedSteps = collect();
        foreach ($steps as $step) {
            if ($step->status == 'skipped' && !$skippedSteps->contains('id', $step->id)) {
                $skippedSteps->push($step);
            }
        }
        return $skippedSteps;
    }

    public static function getCompletedSteps($steps)
    {
        $completedSteps = collect();
        foreach ($steps as $step) {
            $lastDay = Carbon::today()->subDay(1)->toDateString();
            if ($step->end_date == $lastDay && in_array($step->status, $step->done_statuses) && !$completedSteps->contains('id', $step->id)) {
                $completedSteps->push($step);
            }
        }
        return $completedSteps;
    }
}