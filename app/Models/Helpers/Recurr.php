<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 04.03.2017
 * Time: 16:51
 */

namespace App\Models\Helpers;


use Carbon\Carbon;
use Recurr\Rule;

class Recurr extends RecurrenceAbstract implements RecurrenceInterface
{

    public function __construct()
    {
        $this->rule = new Rule();
        return $this;
    }

}