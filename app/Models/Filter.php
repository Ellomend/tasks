<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 21.02.2017
 * Time: 21:58
 */

namespace App\Models;


use App\Block;
use App\Category;
use App\Context;
use App\Mood;
use App\Project;

class Filter
{
    public $blocks;
    public $contexts;
    public $categories;
    public $moods;
    public $projects;


    /**
     * Filter constructor.
     */
    public function __construct()
    {
        $this->blocks = Block::class;
        $this->contexts = Context::class;
        $this->categories = Category::class;
        $this->moods = Mood::class;
        $this->projects = Project::class;
    }

    public function getInstance($entity)
    {
        return new $this->$entity;
    }
}