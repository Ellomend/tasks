<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 09.03.2017
 * Time: 1:32
 */

namespace App\Models\State;


use Carbon\Carbon;

interface StateInterface
{
    /**
     * Get string representation of state related to now
     * @return mixed
     */
    public function getDateDiff();

    /**
     * Get string representation of state related to date
     * @param Carbon $date
     * @return string
     */
    public function getDateDiffToDate(Carbon $date);


}