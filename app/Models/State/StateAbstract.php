<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 09.03.2017
 * Time: 1:33
 */

namespace App\Models\State;


use Carbon\Carbon;

abstract class StateAbstract implements StateInterface
{
    const STATUS_COMPLETE = 'done';
    const STATE_EARLY = 'early';
    const STATE_ERROR = 'error';
    const STATUS_DEFAULT = 'active';
    const STATE_LATE = 'late';
    const STATE_ACTUAL = 'actual';
    const STATUS_ACTIVE = 'active';
    const STATUS_CANCELLED = 'cancelled';
    /**
     * @var Carbon $start
     */
    protected $start;
    /**
     * @var Carbon $end
     */
    protected $end;
    protected $model;

    /**
     * TaskState constructor.
     * @param $model
     */
    public function __construct($model)
    {
        $this->model = $model;
        $this->start = $this->model->carbon_start;
        $this->end = $this->model->carbon_end;
    }

    /**
     * Get string representation of state related to date
     * @param Carbon $date
     * @return mixed
     */
    public function getDateDiffToDate(Carbon $date)
    {
        if ($date->lt($this->start)) {
            return $this::STATE_EARLY;
        }
        if ($date->gt($this->end)) {
            return $this::STATE_LATE;
        }
        if ($date->gte($this->start) && $date->lte($this->end)) {
            return $this::STATE_ACTUAL;
        }
        return $this::STATE_ERROR;
    }

    /**
     * Get string representation of state related to now
     * @return mixed
     */
    public function getDateDiff()
    {
        return $this->getDateDiffToDate(Carbon::now());
    }
}