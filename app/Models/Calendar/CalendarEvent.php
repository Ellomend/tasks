<?php

namespace App\Models\Calendar;

interface CalendarEvent
{
    public function getCalendarTitleAttribute();

    public function getCalendarStartAttribute();

    public function getCalendarEndAttribute();

    public function getCarbonStartAttribute();

    public function getCarbonEndAttribute();

}