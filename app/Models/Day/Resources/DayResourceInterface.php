<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 25.02.2017
 * Time: 8:30
 */

namespace App\Models\Day\Resources;


use Carbon\Carbon;

interface DayResourceInterface
{
    public function render();
    public function setView($view_name);
    public function setViewClass(string $view_class);
    public function setDate(Carbon $day);
    public function getDate();
}