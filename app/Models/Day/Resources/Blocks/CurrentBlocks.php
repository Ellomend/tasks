<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 25.02.2017
 * Time: 9:28
 */

namespace App\Models\Day\Resources\Blocks;


use App\Models\Day\DayOverview;
use App\Models\Day\Resources\DayResourceAbstract;
use App\Models\Day\Resources\DayResourceInterface;
use Carbon\Carbon;

class CurrentBlocks extends DayResourceAbstract implements DayResourceInterface
{
    /**
     * @var DayOverview
     */
    public $dayOverview;
    /**
     * @var Carbon
     */
    public $date;
    public $currentDateTime;
    public $blocks;
    public $master;
    public $currentBlocks;
    public $tasks;
    public $goals;
    public $routines;


    /**
     * CurrentBlock constructor.
     * @param Carbon $date
     */
    public function __construct( Carbon $date)
    {
        $this->date = $date;
        $this->currentDateTime = $date->setTimeFromTimeString(Carbon::now()->toTimeString());
        $this->master = app()->make('Master');
        $this->view = 'pages.day._parts._day_parts.tasks._today_current_block';
        $this->view_class = 'day_current_block';
        $this->blocks = $this->master->blocks;
        $this->tasks = $this->master->tasks;
        $this->goals = $this->master->goals;
        $this->routines = $this->master->routines;
        $this->currentBlocks = collect();
    }

    public function getCurrentBlocks() {
        foreach ($this->blocks as $block) {
            if ($block->isActual($this->currentDateTime)) {
                $this->addBlock($block);
            }
        }

        return $this->currentBlocks;
    }

    private function addBlock($block)
    {
        $this->currentBlocks->push(new BlockItem($block, $this->date));
    }

}