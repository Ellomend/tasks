<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 25.02.2017
 * Time: 10:34
 */

namespace App\Models\Day\Resources\Blocks;

use App\Block;
use Carbon\Carbon;

class BlockItem
{
    public $tasks;
    /**
     * @var Block
     */
    public $block;
    /**
     * @var Carbon
     */
    private $date;

    /**
     * BlockItem constructor.
     * @param Block $block
     * @param Carbon $date
     */
    public function __construct(Block $block, Carbon $date = null)
    {
        $this->block = $block;
        $this->tasks = $block->tasks;
        $this->goals = $block->goals;
        $this->routines = $block->routines;
        $this->date = $date;
    }

    public function getTasks()
    {
        return $this->tasks;
    }

    public function getActiveTasks()
    {
        $activeTasks = $this->tasks->filter(function ($task) {
            if ($task->state() == 'active') {
                return true;
            }
            return false;
        });
        return $activeTasks;
    }

    public function getLateTasks()
    {
        $lateTasks = $this->tasks->filter(function ($task) {
            if ($task->state() == 'late') {
                return true;
            }
            return false;
        });
        return $lateTasks;
    }


    public function getGoals()
    {
        return $this->goals;
    }

    public function getActiveGoals()
    {
        $activeGoals = collect();
        foreach ($this->goals as $goal) {
            if ($goal->is_active) {
                $activeGoals->push($goal);
            }
        }
        return $activeGoals;
    }

    public function getActiveSteps()
    {

        $activeSteps = collect();
        foreach ($this->goals as $goal) {
            foreach ($goal->steps as $step) {
                if ($step->state == 'active') {
                    $activeSteps->push($step);
                }
            }
        }
        return $activeSteps;
    }

    public function getLateSteps()
    {
        $activeGoals = $this->getActiveGoals();
        $lateSteps = collect();
        foreach ($activeGoals as $goal) {
            foreach ($goal->steps as $step) {
                if ($step->state == 'late') {
                    $lateSteps->push($step);
                }
            }
        }
        return $lateSteps;
    }


    public function getActiveRoutines()
    {
        $activeRoutines = collect();
        foreach ($this->routines as $routine) {
            if ($routine->state == 'active') {
                $activeRoutines->push($routine);
            }
        }
        return $activeRoutines;
    }

    public function getRoutinesCompletedToday()
    {
        $completedToday = collect();
        foreach ($this->routines as $routine) {
            if (Carbon::parse($routine->last_complete)->isSameDay($this->date)  ) {
                $completedToday->push($routine);
            }
        }

        return $completedToday;
    }


}