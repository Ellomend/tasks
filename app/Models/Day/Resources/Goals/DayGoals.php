<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 24.02.2017
 * Time: 12:25
 */

namespace App\Models\Day\Resources\Goals;


use App\Models\Day\DayOverview;
use App\Models\Day\Resources\DayResourceAbstract;
use App\Models\Day\Resources\DayResourceInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class DayGoals extends DayResourceAbstract implements DayResourceInterface
{
    /**
     * @var DayOverview
     */
    public $day;
    public $stepsByGoal;
    /**
     * @var Collection $goals
     */
    public $goals;
    public $master;
    public $tasks;
    /**
     * @var DayOverview
     */
    private $dayOverview;


    /**
     * TodayGoals constructor.
     * @param DayOverview $dayOverview
     * @param DayOverview|Carbon $day
     */
    public function __construct(DayOverview $dayOverview, Carbon $day)
    {
        $this->day = $day;
        $this->dayOverview = $dayOverview;
        $this->master = app()->make('Master');
        $this->goals = $this->master->goals;
        $this->view_class = 'day_tasks_review';
        $this->view = 'pages.day._parts._day_parts.tasks._today_goals_review';
    }

    public function thruToday()
    {
        $thruSteps = collect();
        foreach ($this->goals as $goal) {
            foreach ($goal->steps as $step) {

                if ($step->carbon_start->lt($this->dayOverview->day) && $step->carbon_end->gt($this->dayOverview->day)) {
                    $thruSteps->push($step);
                }
            }
        }
        return $thruSteps;
    }

    public function todayOnly()
    {
         $todayOnly = collect();
        foreach ($this->goals as $goal) {
            foreach ($goal->steps as $step) {

                if ($step->carbon_start == $this->dayOverview->day && $step->carbon_end == $this->dayOverview->day) {
                    $todayOnly->push($step);
                }
            }
        }
        return $todayOnly;
    }

    public function startToday()
    {
        $startToday = collect();
        foreach ($this->goals as $goal) {
            foreach ($goal->steps as $step) {

                if ($step->carbon_start == $this->dayOverview->day && $step->carbon_end->gt($this->dayOverview->day)) {
                    $startToday->push($step);
                }
            }
        }
        return $startToday;
    }
    public function endToday()
    {
        $endToday = collect();
        foreach ($this->goals as $goal) {
            foreach ($goal->steps as $step) {

                if ($step->carbon_start->lte($this->dayOverview->day)  && $step->carbon_end == $this->dayOverview->day) {
                    $endToday->push($step);
                }
            }
        }
        return $endToday;
    }

    public function skippedForToday()
    {
         $endToday = collect();
        foreach ($this->goals as $goal) {
            foreach ($goal->steps as $step) {

                if ($step->state == 'late') {
                    $endToday->push($step);
                }
            }
        }
        return $endToday;
    }

    public function stepsCompletedToday()
    {
         $completed = collect();
        foreach ($this->goals as $goal) {
            foreach ($goal->steps as $step) {

                if ($step->finished && Carbon::parse($step->finished)->isSameDay($this->day) && $step->status != 'skipped') {

                    $completed->push($step);
                }
            }
        }
        return $completed;
    }
    public function getActiveGoals()
    {
        $activeGoals = collect();
        foreach ($this->goals as $goal) {
            if ($goal->is_active) {
                $activeGoals->push($goal);
            }
        }
        return $activeGoals;
    }
    public function getYesterdaySteps()
    {
        $activeGoals = $this->getActiveGoals();
        $lateSteps = collect();
        foreach ($activeGoals as $goal) {
            foreach ($goal->steps as $step) {
                /** @var Carbon $stepEnd */
                $stepEnd = $step->carbon_end;
                if ($stepEnd->isYesterday()) {
                    $lateSteps->push($step);
                }
            }
        }
        return $lateSteps;
    }


}