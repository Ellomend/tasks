<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 25.02.2017
 * Time: 8:31
 */

namespace App\Models\Day\Resources;


use Carbon\Carbon;

class DayResourceAbstract
{
    public $view_class;
    public $view;
    public $resource;
    public $day;

    public function render()
    {
        $view = $this->view;
        $view_class = $this->view;
        $resource = $this;
        return view($view, compact('resource','view_class'))->render();
    }
    public function setView($view_name) {
        $this->view = $view_name;
    }
    public function setViewClass(string $view_class) {
        $this->view_class = $view_class;
    }
    public function setDate(Carbon $date) {
        $this->day = $date;
    }
    public function getDate() {
        return $this->day;
    }
}