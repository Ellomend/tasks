<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 26.02.2017
 * Time: 5:26
 */

namespace App\Models\Day\Resources\Routines;


use App\Models\Day\DayOverview;
use App\Models\Day\Resources\DayResourceAbstract;
use App\Models\Day\Resources\DayResourceInterface;
use App\Models\Master;
use Carbon\Carbon;

class DayRoutines  extends DayResourceAbstract implements DayResourceInterface
{
    public $dayOverview;
    public $day;
    /**
     * @var Master $master
     */
    public $master;
    public $routines;
    public $date;

    /**
     * DayRoutines constructor.
     * @param DayOverview $dayOverview
     * @param Carbon $day
     */
    public function __construct(DayOverview $dayOverview, Carbon $day)
    {
        $this->day = $day;
        $this->date = $day;
        $this->dayOverview = $dayOverview;
        $this->view = 'pages.day._parts._day_parts.tasks._today_routines_review';
        $this->view_class = 'day_tasks_review';
        $this->master = app()->make('Master');
        $this->routines = $this->master->routines;
    }

    public function getActiveRoutines()
    {
        $activeRoutines = collect();
        foreach ($this->routines as $routine) {
            if ($routine->state == 'active') {
                $activeRoutines->push($routine);
            }
        }
        return $activeRoutines;
    }

    public function getRoutinesCompletedToday()
    {
        $completedToday = collect();
        foreach ($this->routines as $routine) {
            if (Carbon::parse($routine->last_complete)->isSameDay($this->date)  ) {
                $completedToday->push($routine);
            }
        }

        return $completedToday;
    }
}