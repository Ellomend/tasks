<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 25.02.2017
 * Time: 8:19
 */

namespace App\Models\Day\Resources\Tasks;


use App\Models\Day\DayOverview;
use App\Models\Day\Resources\DayResourceAbstract;
use App\Models\Day\Resources\DayResourceInterface;
use App\Models\Master;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class DayTasks extends DayResourceAbstract implements DayResourceInterface
{
    /**
     * @var Carbon
     */
    public $day;
    public $master;
    /**
     * @var Collection
     */
    public $tasks;
    /**
     * @var DayOverview
     */
    private $dayOverview;

    /**
     * DayTasks constructor.
     * @param DayOverview $dayOverview
     * @param DayOverview|Carbon $day
     */
    public function __construct(DayOverview $dayOverview, Carbon $day)
    {
        $this->day = $day;
        $this->dayOverview = $dayOverview;
        $this->view = 'pages.day._parts._day_parts.tasks._today_tasks_review';
        $this->view_class = 'day_tasks_review';
        $this->master = app()->make('Master');
        $this->tasks = $this->master->tasks;
    }

    /**
     * @return Carbon
     */
    public function getDay(): Carbon
    {
        return $this->day;
    }

    /**
     * @param Carbon $day
     */
    public function setDay(Carbon $day)
    {
        $this->day = $day;

    }


    public function getThruTasks()
    {
        $thruTasks = $this->tasks->filter(function ($task) {
            if ($task->carbon_start->lt($this->day) && $task->carbon_end->gt($this->day)) {
                return true;
            }
            return false;
        });
        return $thruTasks;
    }
    public function getTodayOnlyTasks()
    {
        $todayOnly = $this->tasks->filter(function ($task) {
            if ($task->carbon_start == $this->day && $task->carbon_end == $this->day) {
                return true;
            }
            return false;
        });
        return $todayOnly;
    }
    public function startTodayTasks()
    {
        $startToday = $this->tasks->filter(function ($task) {
            if ($task->carbon_start == $this->day && $task->carbon_end->gte($this->day)) {
                return true;
            }
            return false;
        });
        return $startToday;
    }
    public function endTodayTasks()
    {
        $endToday = $this->tasks->filter(function ($task) {
            if ($task->carbon_start->lte($this->day) && $task->carbon_end == $this->day) {
                return true;
            }
            return false;
        });
        return $endToday;
    }
    public function lateTasks()
    {
        $endToday = $this->tasks->filter(function ($task) {
            if ($task->carbon_start->lt($this->day) && $task->carbon_end->lt($this->day) && $task->state() != 'done') {
                return true;
            }
            return false;
        });
        return $endToday;
    }

    public function getCompletedToday()
    {
        $completed = $this->tasks->filter(function ($task) {


            if ($task->finished && Carbon::parse($task->finished)->isSameDay($this->day)) {
                return true;
            }
            return false;
        });
        return $completed;
    }


}