<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 22.02.2017
 * Time: 4:30
 */

namespace App\Models\Day;


use App\Models\Day\Resources\Blocks\AllBlocks;
use App\Models\Day\Resources\Blocks\CurrentBlocks;
use App\Models\Day\Resources\Goals\DayGoals;
use App\Models\Day\Resources\Routines\DayRoutines;
use App\Models\Day\Resources\Tasks\DayTasks;
use App\Models\Master;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class DayOverview implements DayOverviewInterface
{
    /**
     * @var Master
     */
    public $master;
    /**
     * @var Carbon
     */
    public $day;
    public $goals;


    /**
     * DayOverview constructor.
     * @param Master $master
     * @param Carbon $day
     */
    public function __construct(Master $master, Carbon $day)
    {

        $this->master = $master;
        $this->day = $day;

    }

    public function getTasks()
    {
        return new DayTasks($this, $this->day);
    }

    public function dayGoals()
    {
        $goals = new DayGoals($this, $this->day);
        return $goals;
    }

    public function getCurrentBlocks()
    {
        $currentBlocks = new CurrentBlocks($this->day);
        return $currentBlocks;
    }

    public function getAllBlocks()
    {
        $allBlocks = new AllBlocks($this->day);
        return $allBlocks;
    }

    public function getRoutines()
    {
        return new DayRoutines($this, $this->day);
    }

}