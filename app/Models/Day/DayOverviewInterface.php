<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 26.02.2017
 * Time: 6:49
 */
namespace App\Models\Day;

interface DayOverviewInterface
{
    public function getTasks();

    public function dayGoals();

    public function getCurrentBlocks();

    public function getAllBlocks();

    public function getRoutines();
}