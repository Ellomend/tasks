<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 21.02.2017
 * Time: 23:55
 */

namespace App\Models;


use App\Block;
use App\Category;
use App\Checklist;
use App\Context;
use App\Goal;
use App\Models\Day\DayOverview;
use App\Mood;
use App\Project;
use App\Routine;
use App\Stack;
use App\Task;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class Master
{
    public $routines;
    public $tasks;
    public $blocks;
    public $moods;
    public $contexts;
    public $categories;
    public $projects;
    public $stacks;
    public $checklists;
    public $goals;
    public $user;


    /**
     * Master constructor.
     */
    public function __construct()
    {
        $this->tasks = Task::owned()
            ->with('blocks')->get();
        $this->contexts = Context::owned()->get();
        $this->blocks = Block::owned()
            ->with('tasks')
            ->with('goals')
            ->with('routines')
            ->with('goals.steps')
            ->get();
        $this->categories = Category::owned()->get();
        $this->moods = Mood::owned()->get();
        $this->projects = Project::owned()->get();
        $this->stacks = Stack::owned()->get();
        $this->checklists = Checklist::owned()->get();
        $this->goals = Goal::owned()
            ->with('blocks')
            ->with('steps')
            ->get();
        $this->routines = Routine::owned()->with('blocks')->get();

    }

    /**
     * @return mixed
     */
    public function getRoutines()
    {
        return $this->filterOwned($this->routines);
    }

    /**
     * @return mixed
     */
    public function getTasks()
    {
        return $this->filterOwned($this->tasks);
    }

    /**
     * @return mixed
     */
    public function getBlocks()
    {
        return $this->filterOwned($this->blocks);
    }

    /**
     * @return mixed
     */
    public function getMoods()
    {
        return $this->filterOwned($this->moods);
    }

    /**
     * @return mixed
     */
    public function getContexts()
    {
        return $this->filterOwned($this->contexts);
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->filterOwned($this->categories);
    }

    /**
     * @return mixed
     */
    public function getProjects()
    {
        return $this->filterOwned($this->projects);
    }

    /**
     * @return mixed
     */
    public function getStacks()
    {
        return $this->filterOwned($this->stacks);
    }

    /**
     * @return mixed
     */
    public function getChecklists()
    {
        return $this->filterOwned($this->checklists);
    }

    /**
     * @return mixed
     */
    public function getGoals()
    {
        return $this->filterOwned($this->goals);
    }

    public function setCurrentUser($user)
    {
        $this->user = $user;
    }

    /**
     * @param Collection $collection
     * @return Collection
     * action
     */
    public function filterOwned($collection)
    {
        if ($this->user == null) {
            return $collection;
        }
        $owned = $collection->filter(function ($item) {
            if ($item->user_id == $this->user->id) {
                return true;
            }
            return false;
        });
        return $owned;
    }

    public function getTodayOverview()
    {
        return $this->getDayOverview(Carbon::today());
    }

    public function getDayOverview(Carbon $day)
    {
        return new DayOverview($this, $day);
    }
}