<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 21.02.2017
 * Time: 22:11
 */

namespace App\Models\Traits;


use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;

trait HasOwner
{
    public function getOwned()
    {
        $result = $this->select()
            ->where('user_id', Auth::user()->id)
            ->get();
        return $result;
    }

    public function owner()
    {
        return $this->belongsTo('App\User', 'user_id');
    }



    public static function owned()
    {
        if (Auth::user()) {
            return self::select()
                ->where('user_id', Auth::user()->id);
        }
        return self::select()
                ->where('user_id', null);
    }
}