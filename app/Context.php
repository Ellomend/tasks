<?php

namespace App;

use App\Models\Traits\HasOwner;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Context extends Model
{
    use HasOwner;
    protected $guarded = [];

    public function tasks()
    {
        return $this->belongsToMany('\App\Task');
    }
    public function owner()
    {
        return $this->belongsTo('\App\User', 'user_id');
    }

}
