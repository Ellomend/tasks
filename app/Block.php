<?php

namespace App;

use App\Models\Traits\HasOwner;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    use HasOwner;
    protected $guarded = [];



    public function owner() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function tasks()
    {
        return $this->belongsToMany('\App\Task');
    }

    public function goals()
    {
        return $this->belongsToMany('\App\Goal');
    }
    public function routines()
    {
        return $this->belongsToMany('\App\Routine');
    }

    public function getIsCurrentAttribute()
    {
        $currentTime = Carbon::now();
        $blockStart = Carbon::parse($this->start);
        $blockEnd = Carbon::parse($this->end);
        if ($currentTime->lt($blockEnd) && $currentTime->gt($blockStart)) {
            return true;
        }
        return false;
    }

    public function isActual(Carbon $dateTime)
    {
        $currentTime = $dateTime;
        $blockStart = Carbon::parse($this->start);
        $blockEnd = Carbon::parse($this->end);
        if ($currentTime->lt($blockEnd) && $currentTime->gt($blockStart)) {
            return true;
        }
        return false;
    }

}
