<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use tests\BrowserKitTest;

class GuestResourcesAccessTest extends BrowserKitTest
{

    use DatabaseMigrations;
    use DatabaseTransactions;
    private $user;


    /**
     * @before
     */
    public function prepare()
    {
        $user = factory(App\User::class)->create([
            'name'     => 'test user',
            'email'    => 'test@test.com',
            'password' => bcrypt('testtest')
        ]);

        $this->user = $user;
    }

    

    /**
     * Testing:
     * user cant access tasks and resources
     * @test
     */
    public function user_cant_access_tasks_and_resources()
    {
        $this->visit('/tasks/task');
        $this->seePageIs('/login');
        $this->visit('/tasks/context');
        $this->seePageIs('/login');
        $this->visit('/tasks/block');
        $this->seePageIs('/login');
        $this->visit('/category');
        $this->seePageIs('/login');
        $this->visit('/project');
        $this->seePageIs('/login');
        $this->visit('/mood');
        $this->seePageIs('/login');
    }
}
