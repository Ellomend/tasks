<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use tests\BrowserKitTest;

class GuestTest extends BrowserKitTest
{
    use DatabaseMigrations;
    use DatabaseTransactions;

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $this->visit('/')
             ->see('Welcome to Blog app');
    }

    /**
     * Testing:
     * user can be created
     * @test
     */
    public function user_can_be_created()
    {
        $user = factory(App\User::class, 1)->create(['name' => 'test user'])->first();
        $this->assertEquals('test user', $user->name);
        $this->seeInDatabase('users', ['name' => 'test user']);
    }

    /**
     * Testing:
     * guest cant see user specified menu items
     * @test
     */
    public function guest_cant_see_user_menu_items()
    {
        $this->markTestSkipped('find and remove logout for guest');

        $this->visit('/')
            //todo fix this part of test
//			->dontSee('Blog')
            ->dontSee('Tasks')
            ->dontSee('User area')
            ->dontSee('logout');
    }
    /**
     * Testing:
     * registered user can see menu items
     * @test
     */
    public function user_can_see_menu_items()
    {
        $user = factory(App\User::class)->create();
        $this->actingAs($user);
        $this->visit('/')
            ->see('Blog')
            ->see('Tasks')
            ->see('User area')
            ->see('logout');
    }

    /**
     * Testing:
     * guest can register
     * @test
     */
    public function guest_can_register()
    {
        $this->visit('/')
            ->click('Register');
        $this->type('test register user', 'name');
        $this->type('test@test.com', 'email');
        $this->type('testtest', 'password');
        $this->type('testtest', 'password_confirmation');
        $this->press('Register');
        $this->see('You are logged in!');

        $this->seeInDatabase('users', ['name' => 'test register user']);
    }

    /**
     * Testing:
     * guest can login
     * @test
     */
    public function guest_can_login()
    {
        $user = factory(App\User::class)->create([
            'email' => 'test@test.com',
            'name' => 'testLogin',
            'password' => bcrypt('testtest')
        ]);
        $this->visit('/')
            ->click('Login123');
        $this->seePageIs('/login');
        $this->type('test@test.com', 'email');
        $this->type('testtest', 'password');
        $this->press('Login');
        $this->seePageIs('/home');
        $this->see('You are logged in!');
    }

    /**
     * Testing:
     * user can logout
     * @test
     */
    public function user_can_logout()
    {
        $user = factory(App\User::class)->create();
        $this->actingAs($user);
        $this->visit('/home');
        $this->click('Logout');
        $this->see('Welcome to Blog app.');
    }

    //TODO test remember me and forgot password
}
