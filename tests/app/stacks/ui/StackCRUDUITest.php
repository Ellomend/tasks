<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StackCRUDUITest extends TestCase {
	use DatabaseMigrations;
	use DatabaseTransactions;
	private $user;


	/**
	 * @before
	 */
	public function prepare() {
		 $user = factory( App\User::class )->create( [
			'name'     => 'test user',
			'email'    => 'test@test.com',
			'password' => bcrypt( 'testtest' )
		] );

		$this->user = $user;
	}

	/**
	 * Testing:
	 * user can create stack
	 * @test
	 */
	public function user_can_create_stack() {
		$this->actingAs($this->user);
		$this->visit('/stacks/stack');
		$this->click('createStack');
		$this->seePageIs('/stacks/stack/create');
		$this->type('test g name', 'name');
		$this->type('test g desc123', 'description');
		$this->press('Submit');
		$this->seePageIs('/stacks/stack');
		$this->see('test g desc123');
	}

	/**
	 * Testing:
	 * user can delete stack
	 * @test
	 */
	public function user_can_delete_stack() {
		$stack = factory(App\Stack::class)->create([
			'name' => 'stack del'
		]);
		$this->visit('/stacks/stack');
		$this->see('stack del');
		$this->click('deleteStack'.$stack->id);
		$this->seePageIs('/stacks/stack');
		$this->dontSee('stack del');
	}

	/**
	 * Testing:
	 * user can update stack
	 * @test
	 */
	public function user_can_update_stack() {
		$this->actingAs($this->user);
		$stack = factory(App\Stack::class)->create([
			'name' => 'test stack',
			'description' => 't g d'
		]);
		$this->visit('/stacks/stack');
		$this->see('test stack');
		$this->see('t g d');
		$this->click('editStack'.$stack->id);
		$this->type('test name', 'name');
		$this->type('test desc', 'description');
		$this->press('Submit');
		$this->seePageIs('/stacks/stack');
		$this->see('test desc');
	}


}
