<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ItemCRUDUITest extends TestCase {
	use DatabaseMigrations;
	use DatabaseTransactions;
	private $user;
	private $stack;


	/**
	 * @before
	 */
	public function prepare() {
		$user = factory( App\User::class )->create( [
			'name'     => 'test user',
			'email'    => 'test@test.com',
			'password' => bcrypt( 'testtest' )
		] );

		$this->user = $user;
		$this->stack = factory(App\Stack::class)->create([
			'name' => 'test stack'
		]);
	}

	/**
	 * Testing:
	 * user can create item
	 * @test
	 */
	public function user_can_create_item() {
		$this->visit('/stacks/item');
		$this->click('createItem');
		$this->type('test item', 'name');
		$this->type('test s d', 'description');
		$this->select('1', 'stack_id');
		$this->press('Submit');
		$this->seePageIs('/stacks/item');
		$this->see('test s d');
	}

	/**
	 * Testing:
	 * user can delete item
	 * @test
	 */
	public function user_can_delete_item() {
		$item = factory(App\Item::class)->create([
			'name' => 'del item'
		]);
		$this->visit('/stacks/item');
		$this->see('del item');
		$this->click('deleteItem'.$item->id);
		$this->seePageIs('/stacks/item');
		$this->dontSee('del item');
	}

	/**
	 * Testing:
	 * user can update item
	 * @test
	 */
	public function user_can_update_item() {
		$item = factory(App\Item::class)->create([
			'name' => 'test item'
		]);
		$this->visit('/stacks/item');
		$this->click('editItem'.$item->id);
		$this->type('updt item', 'name');
		$this->type('updt item desc', 'description');
		$this->select('1', 'stack_id');
		$this->press('Submit');
		$this->seePageIs('/stacks/item');
		$this->see('updt item desc');
	}
}
