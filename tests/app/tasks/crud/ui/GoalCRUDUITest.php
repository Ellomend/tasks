<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use tests\BrowserKitTest;


class GoalCRUDUITest extends BrowserKitTest
{
    use DatabaseMigrations;
    use DatabaseTransactions;

    private $user;


    /**
     * @before
     */
    public function prepare()
    {
        $user = factory(App\User::class)->create([
            'name'     => 'test user',
            'email'    => 'test@test.com',
            'password' => bcrypt('testtest')
        ]);

        $this->user = $user;
    }

    /**
     * Testing:
     * user can create goal
     * @test
     */
    public function user_can_create_goal()
    {
        $this->actingAs($this->user);
        $this->visit('/goal');
        $this->click('createGoal');
        $this->type('test goal', 'name');
        $this->type('rtn desc', 'description');
        $this->press('Create');
        $this->seePageIs('/goal');
        $this->see('test goal');
    }

    /**
     * Testing:
     * user can delete goal
     * @test
     */
    public function user_can_delete_goal()
    {
        $this->actingAs($this->user);
//		$cat = factory(App\Category::class)->create();
        $rtn = factory(App\Goal::class)->create([
            'name' => 'del rtn',
            'category_id' => null,
        ])->first();
        $this->visit('/goal');
        $this->see('del rtn');
        $this->click('deleteGoal'.$rtn->id);
        $this->visit('/goal');
        $this->dontSee('del rtn');
    }

    /**
     * Testing:
     * user can update goal
     * @test
     */
    public function user_can_update_goal()
    {
        $this->actingAs($this->user);
        $rtn = factory(App\Goal::class)->create([
            'name' => 'test rtn'
        ]);
        $this->visit('/goal');
        $this->see('test rtn');
        $this->click('editGoal'.$rtn->id);
        $this->type('upd rtn', 'name');
        $this->type('upd desc', 'description');
        $this->press('Update');
        $this->seePageIs('/goal');
        $this->see('upd rtn');
        $this->dontSee('test rtn');
    }
}
