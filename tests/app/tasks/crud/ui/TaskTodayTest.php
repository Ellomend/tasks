<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use tests\BrowserKitTest;

class TaskTodayTest extends BrowserKitTest
{
    use DatabaseMigrations;
    use DatabaseTransactions;
    private $user;


    /**
     * @before
     */
    public function prepare()
    {
        $user = factory(App\User::class)->create([
            'name'     => 'test user',
            'email'    => 'test@test.com',
            'password' => bcrypt('testtest')
        ]);

        $this->user = $user;
    }

    /**
     * Testing:
     * late tasks showed in today page in correct place
     * @test
     */
    public function late_tasks_showed_in_correct_place_in_today_page()
    {
        $startDate = \Carbon\Carbon::today()->subDays(5)->toDateString();
        $endDate = \Carbon\Carbon::today()->subDays(2)->toDateString();
        $this->actingAs($this->user);
        $this->visit('/tasks/task');
        $this->click('createTask');
        $this->type('late task names', 'name');
        $this->type('late descr', 'description');
        $this->type($startDate, 'start');
        $this->type($endDate, 'end');
        $this->press('Submit');
        $this->seePageIs('/tasks/task');
        $this->see('late task names');
        $this->visit('/tasks/today');
        $this->seeInElement('.lateTasksWrapper', 'late task names');
        $this->dontSeeInElement('.dueTasksWrapper', 'late task names');
    }

    /**
     * Testing:
     * due tasks displayed in correct place
     * @test
     */
    public function due_tasks_placed_correctly()
    {
        $this->visit('/');
        $startDate = \Carbon\Carbon::today()->subDays(5)->toDateString();
        $endDate = \Carbon\Carbon::today()->addDays(2)->toDateString();
        $this->actingAs($this->user);
        $this->visit('/tasks/task');
        $this->click('createTask');
        $this->type('due task names', 'name');
        $this->type('due descr', 'description');
        $this->type($startDate, 'start');
        $this->type($endDate, 'end');
        $this->press('Submit');
        $this->seePageIs('/tasks/task');
        $this->see('due task names');
        $this->visit('/tasks/today');
        $this->dontSeeInElement('.lateTasksWrapper', 'due task names');
        $this->seeInElement('.dueTasksWrapper', 'due task names');
    }

    /**
     * Testing:
     * future tasks not shown
     * @test
     */
    public function future_tasks_not_shown()
    {
        $startDate = \Carbon\Carbon::today()->addDays(5)->toDateString();
        $endDate = \Carbon\Carbon::today()->addDays(8)->toDateString();
        $this->actingAs($this->user);
        $this->visit('/tasks/task');
        $this->click('createTask');
        $this->type('future task names', 'name');
        $this->type('due descr', 'description');
        $this->type($startDate, 'start');
        $this->type($endDate, 'end');
        $this->press('Submit');
        $this->seePageIs('/tasks/task');
        $this->see('future task names');
        $this->visit('/tasks/today');
        $this->dontSeeInElement('.lateTasksWrapper', 'future task names');
        $this->dontSeeInElement('.dueTasksWrapper', 'future task names');
    }
}
