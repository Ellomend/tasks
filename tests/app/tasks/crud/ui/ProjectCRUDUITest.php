<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use tests\BrowserKitTest;


class ProjectCRUDUITest extends BrowserKitTest
{
    use DatabaseMigrations;
    use DatabaseTransactions;
    private $user;


    /**
     * @before
     */
    public function prepare()
    {
        $user = factory(App\User::class)->create([
            'name'     => 'test user',
            'email'    => 'test@test.com',
            'password' => bcrypt('testtest')
        ]);

        $this->user = $user;
    }

    /**
     * Testing:
     * user can create project
     * @test
     */
    public function user_can_create_project()
    {
        $this->actingAs($this->user);
        $this->visit('/project');
        $this->click('createProject');
        $this->type('test prj', 'name');
        $this->type('desc prj', 'description');
        $this->press('Create');
        $this->seePageIs('/project');
        $this->see('test prj');
    }
    
    /**
     * Testing:
     * user can delete project
     * @test
     */
    public function user_can_delete_project()
    {
        $prj = factory(App\Project::class)->create([
            'name' => 'del prj',
            'user_id' => $this->user->id
        ]);
        $this->actingAs($this->user);

        $this->visit('/project');
        $this->see('del prj');
        $this->click('deleteProject'.$prj->id);
        $this->seePageIs('/project');
        $this->dontSee('del prj');
    }

    /**
     * Testing:
     * user can update project
     * @test
     */
    public function user_can_update_project()
    {
        $prj = factory(App\Project::class)->create([
            'user_id' => $this->user->id
        ]);
        $this->actingAs($this->user);

        $this->visit('/project');
        $this->see($prj->name);
        $this->click('editProject'.$prj->id);
        $this->type('upd prj', 'name');
        $this->type('upd descr', 'description');
        $this->press('Update');
        $this->seePageIs('/project');
        $this->see('upd prj');
    }

    /**
     * Testing:
     * assigned fields displayed correctly on index page
     * @test
     */
    public function fields_displayed_correctly()
    {
        $this->actingAs($this->user);
        $this->visit('/project');
        $this->click('createProject');
        $this->type('project name', 'name');
        $this->type('pp descr', 'description');
        $this->press('Create');
        $this->seePageIs('/project');
        $this->see('project name');
        $this->see('pp descr');
        $this->see('test@test.com');
    }
}
