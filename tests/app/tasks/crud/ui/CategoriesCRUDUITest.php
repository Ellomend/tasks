<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use tests\BrowserKitTest;

class CategoriesCRUDUITest extends BrowserKitTest
{
    use DatabaseMigrations;
    use DatabaseTransactions;
    private $user;


    /**
     * @before
     */
    public function prepare()
    {
        $user = factory(App\User::class)->create([
            'name'     => 'test user',
            'email'    => 'test@test.com',
            'password' => bcrypt('testtest')
        ]);

        $this->user = $user;
    }

    /**
     * Testing:
     * user can create category
     * @test
     */
    public function user_can_create_category()
    {
        $this->actingAs($this->user);
        $this->visit('/category/create');
        $this->type('new cat', 'name');
        $this->type('cat desc', 'description');
        $this->press('Create');
        $this->seePageIs('/category');
        $this->see('new cat');
        $this->see('cat desc');
    }

    /**
     * Testing:
     * user can delete category
     * @test
     */
    public function user_can_delete_category()
    {
        $cat = factory(App\Category::class)->create([
            'name' => 'abracadabra',
            'user_id' => $this->user->id
        ]);
        $this->actingAs($this->user);
        $this->visit('/category');
        $this->see('abracadabra');
        $this->seeInDatabase('categories', [
            'name' => 'abracadabra'
        ]);
        $this->click('deleteCategory'.$cat->id);
        $this->seePageIs('/category');
        $this->dontSeeInDatabase('categories', [
            'name' => 'abracadabra'
        ]);
        $this->dontSeeInDatabase('categories', [
            'name' => 'abracadabra'
        ]);
        $this->dontSee('abracadabra');
    }

    /**
     * Testing:
     * user_can_update_category
     * @test
     */
    public function user_can_update_category()
    {
        $cat = factory(App\Category::class)->create([
            'name' => 'cat name',
            'user_id' => $this->user->id
        ]);
        $this->actingAs($this->user);
        $this->visit('/category');
        $this->see('cat name');
        $this->click('editCategory'.$cat->id);
        $this->type('updated cat', 'name');
        $this->type('cat desc', 'description');
        $this->press('Update');
        $this->seePageIs('/category');
        $this->see('updated cat');
        $this->see('cat desc');
        $this->dontSee('cat name');
    }

    /**
     * Testing:
     * assigned fields displayed on index page
     * @test
     */
    public function fields_displayed_correctly()
    {
        $this->actingAs($this->user);
        $this->visit('/category');
        $this->click('createCategory');
        $this->type('test catt', 'name');
        $this->type('test cat descr', 'description');
        $this->press('Create');
        $this->seePageIs('/category');
        $this->see('test catt');
        $this->see('test cat descr');
        $this->see('test@test.com');
    }
}
