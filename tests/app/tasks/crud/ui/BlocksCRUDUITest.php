<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use tests\BrowserKitTest;

class BlocksCRUDUITest extends BrowserKitTest
{
    use DatabaseMigrations;
    use DatabaseTransactions;
    private $user;


    /**
     * @before
     */
    public function prepare()
    {
        $user = factory(App\User::class)->create([
            'name'     => 'test user',
            'email'    => 'test@test.com',
            'password' => bcrypt('testtest')
        ]);

        $this->user = $user;
    }
    /**
     * Testing:
     * user can create block
     * @test
     */
    public function user_can_create_block()
    {
        $this->actingAs($this->user);



        $this->visit('/tasks/block');
        $this->click('createBlock');
        $this->seePageIs('/tasks/block/create');
        $this->type('test bb', 'name');
        $this->type('test dd', 'description');
        $this->type('23:40:50', 'start');
        $this->type('00:40:50', 'end');
        $this->press('Create');
        $this->seePageIs('/tasks/block');
        $this->see('test bb');
    }

    /**
     * Testing:
     * user_can_delete_block
     * @test
     */
    public function user_can_delete_block()
    {
        $this->actingAs($this->user);

        $block = factory(App\Block::class)->create([
            'name' => 'test block'
        ]);
        $this->visit('/tasks/block');
        $this->see($block->name);
        $this->click('deleteBlock'.$block->id);
        $this->visit('/tasks/block');
        $this->dontSee($block->name);
    }

    /**
     * Testing:
     * user can update block
     * @test
     */
    public function user_can_update_block()
    {
        $this->actingAs($this->user);

        $block = factory(App\Block::class)->create([
            'name' => 'test block'
        ]);
        $this->visit('/tasks/block');
        $this->see($block->name);
        $this->click('editBlock'.$block->id);
        $this->type('updated', 'name');
        $this->type('upd desc', 'description');
        $this->press('Update block');
        $this->seePageIs('/tasks/block');
        $this->see('updated');
    }

    /**
     * Testing:
     * fields assigned correctly and displayed on index page
     * @test
     */
    public function fields_assigned_correctly()
    {
        $this->actingAs($this->user);
        $this->visit('/tasks/block');
        $this->click('Create block');
        $this->seePageIs('/tasks/block/create');
        $this->type('test block', 'name');
        $this->type('descr for test block', 'description');
        $this->type('11:04:14', 'start');
        $this->type('12:04:14', 'end');
        $this->press('Create');
        $this->seePageIs('/tasks/block');
        $this->see('test block');
        $this->see('descr for test block');
        $this->see('11:04:14');
        $this->see('12:04:14');
        $this->see('test@test.com');
    }
}
