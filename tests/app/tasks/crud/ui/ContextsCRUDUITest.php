<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use tests\BrowserKitTest;

class ContextsCRUDUITest extends BrowserKitTest
{
    use DatabaseMigrations;
    use DatabaseTransactions;
    private $user;

    /**
     * @before
     */
    public function prepare()
    {
        $user = factory(App\User::class)->create([
            'name'     => 'test user',
            'email'    => 'test@test.com',
            'password' => bcrypt('testtest')
        ]);

        $this->user = $user;
    }

    /**
     * Testing:
     * user can create context
     * @test
     */
    public function user_can_create_context()
    {
        $this->actingAs($this->user);
        $this->visit('/tasks/context');
        $this->visit('/tasks/context/create');
        $this->type('test context', 'name');
        $this->type('tc desc', 'description');
        $this->press('Submit');
        $this->visit('/tasks/context');
        $this->see('test context');
    }

    /**
     * Testing:
     * user can delete context
     * @test
     */
    public function user_can_delete_context()
    {
        $context = factory(App\Context::class)->create([
            'name' => 'del cont'
        ]);
        $this->actingAs($this->user);
        $this->visit('/tasks/context');
        $this->see('del cont');
        $this->click('deleteContext' . $context->id);
        $this->visit('/tasks/context');
        $this->dontSee('del cont');
    }

    /**
     * Testing:
     * user can update context
     * @test
     */
    public function user_can_update_context()
    {
        $context = factory(App\Context::class)->create([
            'name' => 'upd context'
        ]);
        $this->actingAs($this->user);
        $this->visit('/tasks/context');
        $this->see('upd context');
        $this->click('editContext' . $context->id);
        $this->type('test context updated', 'name');
        $this->type('tc desc updated', 'description');
        $this->press('Submit');
        $this->visit('/tasks/context');
        $this->see('test context updated');
        $this->dontSee('upd context');
    }

    /**
     * Testing:
     * assigned fields displayed correctly on index page
     * @test
     */
    public function can_be_assigned_fields()
    {
        $this->actingAs($this->user);
        $this->visit('/tasks/context');
        $this->click('Create new context');
        $this->see('New context');
        $this->type('test context', 'name');
        $this->type('#ff0022', 'color');
        $this->type('context description', 'description');
        $this->press('Submit');
        $this->seePageIs('/tasks/context');
        $this->see('test context');
        $this->see('context description');
        $this->see('#ff0022');
        $this->see('test@test.com');
    }

    /**
     * Testing:
     * user can index only own contexts
     * @test
     */
    public function user_can_index_only_own_contexts()
    {
        $context = factory(App\Context::class)->create([
            'name' => 'cccc1',
            'user_id' => $this->user->id
        ]);
        $user2 = factory(App\User::class)->create();
        $context2 = factory(App\Context::class)->create([
            'name' => 'tttt2',
            'user_id' => $user2->id
        ]);
        $this->actingAs($this->user);
        $this->visit('/tasks/context');
        $this->see($context->name);
        $this->dontSee($context2->name);
        $this->actingAs($user2);
        $this->visit('/tasks/context');
        $this->dontSee($context->name);
        $this->see($context2->name);
    }

    /**
     * Testing:
     * user cant edit unowned context
     * @test
     */
    public function user_cant_edit_not_own_context()
    {
        $author = factory(App\User::class)->create([
            'name' => 'author'
        ]);
        $disputedContext = factory(App\Context::class)->create([
            'name' => 'original',
            'user_id' => $author->id
        ]);
        $this->actingAs($this->user);
        $this->visit('/tasks/context/'.$disputedContext->id.'/edit');
        $this->seePageIs('/error/403');
    }

    //TODO test user cant update not own context
}
