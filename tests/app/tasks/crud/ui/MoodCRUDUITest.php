<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use tests\BrowserKitTest;


class MoodCRUDUITest extends BrowserKitTest {
	use DatabaseMigrations;
	use DatabaseTransactions;
	private $user;


	/**
	 * @before
	 */
	public function prepare() {
		$user = factory( App\User::class )->create( [
			'name'     => 'test user',
			'email'    => 'test@test.com',
			'password' => bcrypt( 'testtest' )
		] );

		$this->user = $user;
	}
	/**
	 * Testing:
	 * user can create mood
	 * @test
	 */
	public function user_can_create_mood() {
		$this->actingAs($this->user);

		$this->visit('/mood');
		$this->click('createMood');
		$this->type('test mood', 'name');
		$this->type('mood description', 'description');
		$this->type('5', 'value');
		$this->press('Create');
		$this->seePageIs('/mood');
		$this->see('test mood');
	}

	/**
	 * Testing:
	 * user can delete mood
	 * @test
	 */
	public function user_can_delete_mood() {
		$mood = factory(App\Mood::class)->create([
			'name' => 'del mood',
			'user_id' => $this->user->id
		]);
		$this->actingAs($this->user);

		$this->visit('/mood');
		$this->see('del mood');
		$this->click('deleteMood'.$mood->id);
		$this->seePageIs('/mood');
		$this->dontSee('del mood');
	}

	/**
	 * Testing:
	 * user can update mood
	 * @test
	 */
	public function user_can_update_mood() {
		$mood = factory(App\Mood::class)->create([
			'name' => 'upd mood',
			'user_id' => $this->user->id

		]);
		$this->actingAs($this->user);

		$this->visit('/mood');
		$this->see('upd mood');
		$this->click('editMood'.$mood->id);
		$this->type('test mood updated', 'name');
		$this->type('mood description 1', 'description');
		$this->type('6', 'value');
		$this->press('Update');
		$this->seePageIs('/mood');
		$this->see('test mood updated');
	}

	/**
	 * Testing:
	 * fields displayed correctly on index page
	 * @test
	 */
	public function fields_displayed_correctly() {
		$this->actingAs($this->user);
		$this->visit('/mood');
		$this->click('createMood');
		$this->type('mood nanme', 'name');
		$this->type('mood descr', 'description');
		$this->type('33', 'value');
		$this->press('Create');
		$this->seePageIs('/mood');
		$this->see('mood nanme');
		$this->see('mood descr');
		$this->see('33');
		$this->see('test@test.com');
	}
}
