<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use tests\BrowserKitTest;

class TasksCRUDUITest extends BrowserKitTest {
	use DatabaseMigrations;
	use DatabaseTransactions;
	private $user;

	/**
	 * @before
	 */
	public function prepare() {
		$user = factory(App\User::class)->create([
			'name' => 'test user',
			'email'=> 'test@test.com',
			'password' => bcrypt('testtest')
		]);

		$this->user = $user;
	}
	/**
	* A basic functional test example.
	* @test
	* @return void
	*/
	public function user_can_create_tasks()
	{
		$this->actingAs($this->user);
		$this->visit('/tasks/task/create');
		$this->type('test task name', 'name');
		$this->type('test task desc', 'description');
		$this->press('Submit');
		$this->see('test task desc');
		$this->visit('/tasks/task');
		$this->see('test task name');
	}

	/**
	 * Testing:
	 * user can delete task
	 * @test
	 */
	public function user_can_delete_task() {
		$this->actingAs($this->user);
		$task = factory(App\Task::class)->create([
			'name' => 'task to delete',
			'user_id' => $this->user->id
		]);
		$this->visit('/tasks/task');
		$this->see('task to delete');
		$this->click('deleteTask'.$task->id);
		$this->seePageIs('/tasks/task');
		$this->dontSee('task to delete');
	}

	/**
	 * Testing:
	 * user can update task
	 * @test
	 */
	public function user_can_update_task() {
		$this->actingAs($this->user);
		$task = factory(App\Task::class)->create([
			'name' => 'task to update'
		]);
		$this->visit('/');
		$this->visit('/tasks/task');
		$this->click('editTask'.$task->id);
		$this->type('updated task', 'name');
		$this->type('updated task description', 'description');
		$this->press('Submit');
		$this->see('updated task');
		$this->visit('/tasks/task');
		$this->see('updated task');
	}

	/**
	 * Testing:
	 * assigned fields displayed correctly on index page
	 * @test
	 */
	public function fields_displayed_correctly() {
		$this->actingAs($this->user);
		$context = factory(App\Context::class)->create();
		$block = factory(App\Block::class)->create();
		$category = factory(App\Category::class)->create([
			'user_id' => $this->user->id
		]);
		$mood = factory(App\Mood::class)->create();
		$project = factory(App\Project::class)->create([
			'user_id' => $this->user->id
		]);
		$this->visit('/tasks/task');
		$this->click('createTask');
		$this->type('task name', 'name');
		$this->type('tassk descr', 'description');
		$this->select($context->id, 'contexts[]');
		$this->select($block->id, 'blocks[]');
		$this->select($category->id, 'category_id');
		$this->select($mood->id, 'mood_id');
		$this->select($project->id, 'project_id');
		$this->select('done', 'status');
		$this->type('2017-01-23', 'start');
		$this->type('2017-01-26', 'end');
		$this->press('Submit');
		$this->visit('/tasks/task');
		$this->see('task name');
		$this->see('tassk descr');
		$this->see('done');
		$this->see('2017-01-23');
		$this->see('2017-01-26');
		$this->see('test@test.com');
		$this->see($category->name);
		$this->see($mood->name);
		$this->see($project->name);
	}
}
