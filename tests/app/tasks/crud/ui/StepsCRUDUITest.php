<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use tests\BrowserKitTest;

class StepsCRUDUITest extends BrowserKitTest {
	use DatabaseMigrations;
	use DatabaseTransactions;
	private $user;
	private $goal;
	private $step;

	/**
	 * @before
	 */
	public function prepare() {
		$user = factory( App\User::class )->create( [
			'name'     => 'test user',
			'email'    => 'test@test.com',
			'password' => bcrypt( 'testtest' )
		] );

		$this->user = $user;
		$this->goal = $goal = factory(App\Goal::class)->create([
			'name' => 'test goal',
			'description' => 'goal desc'
		]);

		$this->step = factory(App\Step::class)->create([
			'name' => 'test chr'
		]);
	}

	/**
	 * Testing:
	 * user can regenerate steps
	 * @test
	 */
	public function user_can_regenerate_steps() {
        $this->markTestSkipped('must be revisited.');
		$this->actingAs($this->user);
		$this->visit('/goal');
		$this->see('test goal');
		$this->click('regenerateSteps'.$this->goal->id);
		$this->seeInDatabase('steps', [
			'description' => 'goal desc'
		]);
	}

	/**
	 * Testing:
	 * user can create step
	 * @test
	 */
	public function user_can_create_step() {
		$this->actingAs($this->user);

		$this->visit('/step');
		$this->click('createStep');
		$this->type('test step', 'name');
		$this->type('test desc123', 'description');
		$this->select($this->goal->id, 'goal_id');
		$this->type('2017-01-18', 'start_date');
		$this->type('2017-01-21', 'end_date');
		$this->select('active', 'status');
		$this->press('Create');
		$this->seePageIs('/step');
		$this->see('test desc123');
	}

	/**
	 * Testing:
	 * user can delete step
	 * @test
	 */
	public function user_can_delete_step() {
		$this->actingAs($this->user);

		$this->visit('/step');
		$this->see('test chr');
		$this->click('deleteStep'.$this->step->id);
		$this->seePageIs('/step');
		$this->dontSee('test chr');
	}

	/**
	 * Testing:
	 * user can update step
	 * @test
	 */
	public function user_can_update_step() {
		$this->actingAs($this->user);

		$this->visit('/step');
		$this->click('editStep'.$this->step->id);
		$this->type('step 123', 'name');
		$this->type('step 123 desc', 'description');
		$this->select('1', 'goal_id');
		$this->select('done', 'status');
		$this->type('2017-01-20', 'start_date');
		$this->type('2017-01-21', 'end_date');
		$this->type('2017-01-22', 'end_date');
		$this->type('2017-01-23', 'end_date');
		$this->press('Update');
		$this->seePageIs('/step');
		$this->see('step 123 desc');
	}
}
