<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use tests\BrowserKitTest;

class TaskFieldsUITest extends BrowserKitTest {
	use DatabaseMigrations;
	use DatabaseTransactions;
	private $user;
	private $task;


	/**
	 * @before
	 */
	public function prepare() {
		$user = factory( App\User::class )->create( [
			'name'     => 'test user',
			'email'    => 'test@test.com',
			'password' => bcrypt( 'testtest' )
		] );

		$this->user = $user;
		$this->task = factory( App\Task::class )->create( [
			'name'        => 'test task',
			'description' => 'task description',
			//        'status' => $faker->randomElement(['active' ,'done', 'inbox', 'failed', 'moved', 'canceled', 'nailed']),
			'status'      => null,
			'type'        => null,
			'start'       => null,
			'end'         => null,
			'user_id'     => App\User::all()->random()->id
		] );
	}

	private function createDummyFieldsData() {
		$dummy             = [];
		$dummy['context']  = factory( App\Context::class )->create();
		$dummy['block']    = factory( App\Block::class )->create();
		$dummy['category'] = factory( App\Category::class )->create([
			'user_id' => $this->user->id
		]);
		$dummy['mood']     = factory( App\Mood::class )->create();
		$dummy['project']  = factory( App\Project::class )->create([
			'user_id' => $this->user->id
		]);

		return $dummy;
	}

	/**
	 * Testing:
	 * user can create clear task
	 * No fields, no dates, no nothing
	 * @test
	 */
	public function user_can_create_clear_task() {
		$this->seeInDatabase( 'tasks', [
			'name' => 'test task'
		] );
	}

	/**
	 * Testing:
	 * user can assign fields
	 * TODO add more after save assertions
	 * @test
	 */
	public function user_can_assign_fields() {
		$dummy = $this->createDummyFieldsData();
		$this->actingAs($this->user);
		$this->visit( '/tasks/task' );
		$this->click( 'editTask' . $this->task->id );
		$this->type( 'updated task', 'name' );
		$this->type( 'updated task description', 'description' );
		$this->select( 'done', 'status' );
		$this->select( '1', 'contexts[]' );
		$this->select( '1', 'blocks[]' );
		$this->select( $dummy['category']->id, 'category_id' );
		$this->select( '1', 'mood_id' );
		$this->select( '1', 'project_id' );
		$this->type( '2017-01-14', 'start' );
		$this->type( '2017-01-15', 'end' );
		$this->press( 'Submit' );
		$this->see( 'updated task' );
	}

}
