<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use tests\BrowserKitTest;

class RoutineCRUDUITest extends BrowserKitTest
{
    use DatabaseMigrations;
    use DatabaseTransactions;
    private $user;


    /**
     * @before
     */
    public function prepare()
    {
        $user = factory(App\User::class)->create([
            'name'     => 'test user',
            'email'    => 'test@test.com',
            'password' => bcrypt('testtest')
        ]);

        $this->user = $user;
    }

    /**
     * Testing:
     * user can create routine
     * @test
     */
    public function user_can_create_routine()
    {
        $this->actingAs($this->user);
        $this->visit('/routine/routine');
        $this->click('createRoutine');
        $this->seePageIs('/routine/routine/create');
        $this->type('test', 'name');
        $this->type('test d', 'description');
        $this->select('2', 'interval_type');
        $this->type('3', 'interval_value');
        $this->press('Submit');
        $this->seePageIs('/routine/routine');
        $this->see('test d');
    }

    /**
     * Testing:
     * user can delete routine
     * @test
     */
    public function user_can_delete_routine()
    {
        $routine = factory(App\Routine::class)->create([
            'name' => 'routine del',
            'user_id' => $this->user->id
        ]);
        $this->actingAs($this->user);

        $this->visit('/routine/routine');
        $this->see('routine del');
        $this->click('deleteRoutine'.$routine->id);
        $this->seePageIs('/routine/routine');
        $this->dontSee('routine del');
    }

    /**
     * Testing:
     * user can update routine
     * @test
     */
    public function user_can_update_routine()
    {
        $this->actingAs($this->user);
        $routine = factory(App\Routine::class)->create([
            'name' => 'test routine',
            'description' => 't g d'
        ]);
        $this->visit('/routine/routine');
        $this->see('test routine');
        $this->see('t g d');
        $this->click('editRoutine'.$routine->id);
        $this->type('test name', 'name');
        $this->type('test desc', 'description');
        $this->press('Submit');
        $this->seePageIs('/routine/routine');
        $this->see('test desc');
    }
}
