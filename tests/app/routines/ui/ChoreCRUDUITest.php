<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use tests\BrowserKitTest;

class ChoreCRUDUITest extends BrowserKitTest
{
    use DatabaseMigrations;
    use DatabaseTransactions;
    private $user;
    private $routine;


    /**
     * @before
     */
    public function prepare()
    {
        $user = factory(App\User::class)->create([
            'name'     => 'test user',
            'email'    => 'test@test.com',
            'password' => bcrypt('testtest')
        ]);

        $this->user = $user;
        $this->routine = factory(App\Routine::class)->create([
            'name' => 'test routine'
        ]);
    }

    /**
     * Testing:
     * user can create chore
     * @test
     */
    public function user_can_create_chore()
    {
        $this->actingAs($this->user);

        $this->visit('/routine/chore');
        $this->click('createChore');
        $this->type('test chore', 'name');
        $this->type('test s d', 'description');
        $this->select('active', 'status');
        $this->select('1', 'routine_id');
        $this->type('2017-01-20 08:57:19', 'completed_at');
        $this->press('Submit');
        $this->seePageIs('/routine/chore');
        $this->see('test s d');
    }

    /**
     * Testing:
     * user can delete chore
     * @test
     */
    public function user_can_delete_chore()
    {
        $chore = factory(App\Chore::class)->create([
            'name' => 'del chore',
        ]);
        $this->actingAs($this->user);

        $this->visit('/routine/chore');
        $this->see('del chore');
        $this->click('deleteChore'.$chore->id);
        $this->seePageIs('/routine/chore');
        $this->dontSee('del chore');
    }

    /**
     * Testing:
     * user can update chore
     * @test
     */
    public function user_can_update_chore()
    {
        $chore = factory(App\Chore::class)->create([
            'name' => 'test chore'
        ]);
        $this->actingAs($this->user);

        $this->visit('/routine/chore');
        $this->click('editChore'.$chore->id);
        $this->type('updt chore', 'name');
        $this->type('updt chore desc', 'description');
        $this->select('active', 'status');
        $this->select('1', 'routine_id');
        $this->type('2017-01-21 09:06:45', 'completed_at');
        $this->press('Submit');
        $this->seePageIs('/routine/chore');
        $this->see('updt chore desc');
    }
}
