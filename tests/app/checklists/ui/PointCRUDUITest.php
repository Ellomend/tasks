<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use tests\BrowserKitTest;

class PointCRUDUITest extends BrowserKitTest {
	use DatabaseMigrations;
	use DatabaseTransactions;
	private $user;
	private $checklist;


	/**
	 * @before
	 */
	public function prepare() {
		$user = factory( App\User::class )->create( [
			'name'     => 'test user',
			'email'    => 'test@test.com',
			'password' => bcrypt( 'testtest' )
		] );

		$this->user = $user;
		$this->checklist = factory(App\Checklist::class)->create([
			'name' => 'test checklist'
		]);
	}

	/**
	 * Testing:
	 * user can create point
	 * @test
	 */
	public function user_can_create_point() {
		$this->actingAs($this->user);

		$this->visit('/checklists/point');
		$this->click('createPoint');
		$this->type('test point', 'name');
		$this->type('test s d', 'description');
		$this->select('1', 'checklist_id');
		$this->press('Submit');
		$this->seePageIs('/checklists/point');
		$this->see('test s d');
	}

	/**
	 * Testing:
	 * user can delete point
	 * @test
	 */
	public function user_can_delete_point() {
		$point = factory(App\Point::class)->create([
			'name' => 'del point'
		]);
		$this->actingAs($this->user);

		$this->visit('/checklists/point');
		$this->see('del point');
		$this->click('deletePoint'.$point->id);
		$this->seePageIs('/checklists/point');
		$this->dontSee('del point');
	}

	/**
	 * Testing:
	 * user can update point
	 * @test
	 */
	public function user_can_update_point() {
		$point = factory(App\Point::class)->create([
			'name' => 'test point'
		]);
		$this->actingAs($this->user);

		$this->visit('/checklists/point');
		$this->click('editPoint'.$point->id);
		$this->type('updt point', 'name');
		$this->type('updt point desc', 'description');
		$this->select('1', 'checklist_id');
		$this->press('Submit');
		$this->seePageIs('/checklists/point');
		$this->see('updt point desc');
	}
}
