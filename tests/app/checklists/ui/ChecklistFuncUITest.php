<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use tests\BrowserKitTest;

class ChecklistFuncUITest extends BrowserKitTest
{
    use DatabaseMigrations;
    use DatabaseTransactions;
    private $user;


    /**
     * @before
     */
    public function prepare()
    {
        $user = factory(App\User::class)->create([
            'name'     => 'test user',
            'email'    => 'test@test.com',
            'password' => bcrypt('testtest')
        ]);

        $this->user = $user;
    }

    /**
     * Testing:
     * user can assign step to checklist
     * @test
     */
    public function user_can_assign_step_to_checklist()
    {
        $this->actingAs($this->user);
        $checklist = factory(\App\Checklist::class)->create();
        $this->visit('/checklists/checklist');
        $this->click('addPoint'.$checklist->id);
        $this->type('added point', 'name');
        $this->type('added point description', 'description');
        $this->press('Submit');
        $this->seePageIs('/checklists/point');
        $this->visit('/checklists/checklist');
        $this->see('added point');
    }

    /**
     * Testing:
     * user can create template checklist
     * @test
     */
    public function user_can_create_blueprint_checklist() {
        $this->actingAs($this->user);
        $checklist = factory(\App\Checklist::class)->create();
        $this->visit('/checklists/checklist');
        $this->click('addPoint'.$checklist->id);
        $this->type('added point', 'name');
        $this->type('added point description', 'description');
        $this->press('Submit');
        $this->seePageIs('/checklists/point');
        $this->visit('/checklists/checklist');
        $this->see('added point');
    }

    /**
     * Testing:
     * user can create running checklist from blueprint
     * @test
     */
    public function user_can_create_running_checklist_from_blueprint() {
        $this->actingAs($this->user);
        $checklist = factory(App\Checklist::class)->create();
        $points = factory(App\Point::class, 5)->create([
            'checklist_id' => $checklist->id
        ]);
        $this->visit('/checklists/checklist');
        $this->see($checklist->name);
        $this->see($points->last()->name);
        $this->visit('/checklists/blueprints');
        $this->see($checklist->name);
        $this->see($points->last()->name);
        $this->click('startChecklist'.$checklist->id);
        $this->seePageIs('/checklists/checklist/2');
        $this->see($checklist->name);
        $this->see($points->last()->name);
    }
}
