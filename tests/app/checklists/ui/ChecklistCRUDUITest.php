<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use tests\BrowserKitTest;

class ChecklistCRUDUITest extends BrowserKitTest
{
    use DatabaseMigrations;
    use DatabaseTransactions;
    private $user;


    /**
     * @before
     */
    public function prepare()
    {
        $user = factory(App\User::class)->create([
            'name'     => 'test user',
            'email'    => 'test@test.com',
            'password' => bcrypt('testtest')
        ]);

        $this->user = $user;
    }

    /**
     * Testing:
     * user can create checklist
     * @test
     */
    public function user_can_create_checklist()
    {
        $this->actingAs($this->user);
        $this->visit('/checklists/checklist');
        $this->click('createChecklist');
        $this->seePageIs('/checklists/checklist/create');
        $this->type('test g name', 'name');
        $this->type('test g desc123', 'description');
        $this->press('Submit');
        $this->seePageIs('/checklists/checklist');
        $this->see('test g desc123');
        $this->seeInDatabase('checklists', [
            'name' => 'test g name'
        ]);
    }

    /**
     * Testing:
     * user can delete checklist
     * @test
     */
    public function user_can_delete_checklist()
    {
        $checklist = factory(App\Checklist::class)->create([
            'name' => 'checklist del'
        ]);
        $this->actingAs($this->user);

        $this->visit('/checklists/checklist');
        $this->see('checklist del');
        $this->click('deleteChecklist'.$checklist->id);
        $this->seePageIs('/checklists/checklist');
        $this->dontSee('checklist del');
    }

    /**
     * Testing:
     * user can update checklist
     * @test
     */
    public function user_can_update_checklist()
    {
        $this->actingAs($this->user);
        $checklist = factory(App\Checklist::class)->create([
            'name' => 'test checklist',
            'description' => 't g d'
        ]);
        $this->visit('/checklists/checklist');
        $this->see('test checklist');
        $this->see('t g d');
        $this->click('editChecklist'.$checklist->id);
        $this->type('test name', 'name');
        $this->type('test desc', 'description');
        $this->press('Submit');
        $this->seePageIs('/checklists/checklist');
        $this->see('test desc');
    }
}
