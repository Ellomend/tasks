-- MySQL dump 10.13  Distrib 5.6.33, for debian-linux-gnu (x86_64)
--
-- Host: 192.168.10.10    Database: blog
-- ------------------------------------------------------
-- Server version	5.6.33-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_menu`
--

DROP TABLE IF EXISTS `admin_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `uri` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_menu`
--

LOCK TABLES `admin_menu` WRITE;
/*!40000 ALTER TABLE `admin_menu` DISABLE KEYS */;
INSERT INTO `admin_menu` VALUES (1,0,1,'Index','fa-bar-chart','/',NULL,NULL),(2,0,2,'Admin','fa-tasks','',NULL,NULL),(3,2,3,'Users','fa-users','auth/users',NULL,NULL),(4,2,4,'Roles','fa-user','auth/roles',NULL,NULL),(5,2,5,'Permission','fa-user','auth/permissions',NULL,NULL),(6,2,6,'Menu','fa-bars','auth/menu',NULL,NULL),(7,2,7,'Operation log','fa-history','auth/logs',NULL,NULL),(8,0,8,'Tasks','fa-bars','/tasks','2016-11-26 19:59:02','2016-11-27 06:26:37'),(9,0,9,'Blocks','fa-bars','/blocks','2016-11-27 06:17:30','2016-11-27 06:26:37'),(10,0,10,'Routines','fa-bars','/routines','2016-11-27 06:17:39','2016-11-27 06:26:37'),(11,0,11,'Chores','fa-bars','chores','2016-11-27 06:17:52','2016-11-27 06:26:37'),(12,0,14,'categories','fa-bars','categories','2016-11-27 06:18:04','2016-11-27 06:26:37'),(13,0,13,'contexts','fa-bars','contexts','2016-11-27 06:18:14','2016-11-27 06:26:37'),(14,0,12,'projects','fa-bars','projects','2016-11-27 06:18:25','2016-11-27 06:26:37'),(15,0,15,'moods','fa-bars','moods','2016-11-27 06:18:33','2016-11-27 06:26:37');
/*!40000 ALTER TABLE `admin_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_operation_log`
--

DROP TABLE IF EXISTS `admin_operation_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_operation_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `method` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `input` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_operation_log_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=170 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_operation_log`
--

LOCK TABLES `admin_operation_log` WRITE;
/*!40000 ALTER TABLE `admin_operation_log` DISABLE KEYS */;
INSERT INTO `admin_operation_log` VALUES (1,1,'admin','GET','10.240.1.4','[]','2016-11-26 19:52:16','2016-11-26 19:52:16'),(2,1,'admin','GET','10.240.0.224','[]','2016-11-26 19:58:01','2016-11-26 19:58:01'),(3,1,'admin/auth/menu','GET','10.240.0.16','{\"_pjax\":\"#pjax-container\"}','2016-11-26 19:58:46','2016-11-26 19:58:46'),(4,1,'admin/auth/menu','POST','10.240.0.224','{\"parent_id\":\"0\",\"title\":\"Tasks\",\"icon\":\"fa-bars\",\"uri\":\"\\/tasks\",\"roles\":[\"\"],\"_token\":\"Ngnz1ZmC19XLToRFTfFvw3xN2fgCBKcQPMMr8WFJ\"}','2016-11-26 19:59:02','2016-11-26 19:59:02'),(5,1,'admin/auth/menu','GET','10.240.1.45','[]','2016-11-26 19:59:03','2016-11-26 19:59:03'),(6,1,'admin','GET','10.240.1.18','{\"_pjax\":\"#pjax-container\"}','2016-11-26 19:59:07','2016-11-26 19:59:07'),(7,1,'admin/auth/menu','GET','10.240.0.224','{\"_pjax\":\"#pjax-container\"}','2016-11-26 19:59:11','2016-11-26 19:59:11'),(8,1,'admin/auth/menu','GET','10.240.1.67','[]','2016-11-26 19:59:15','2016-11-26 19:59:15'),(9,1,'admin/tasks','GET','10.240.0.181','{\"_pjax\":\"#pjax-container\"}','2016-11-26 19:59:17','2016-11-26 19:59:17'),(10,1,'admin','GET','10.240.1.31','[]','2016-11-26 20:00:00','2016-11-26 20:00:00'),(11,1,'admin/tasks','GET','10.240.1.4','{\"_pjax\":\"#pjax-container\"}','2016-11-26 20:00:03','2016-11-26 20:00:03'),(12,1,'admin','GET','10.240.0.195','[]','2016-11-26 20:02:02','2016-11-26 20:02:02'),(13,1,'admin','GET','10.240.0.224','[]','2016-11-26 20:02:05','2016-11-26 20:02:05'),(14,1,'admin','GET','10.240.0.215','[]','2016-11-26 20:02:17','2016-11-26 20:02:17'),(15,1,'admin','GET','10.240.0.224','[]','2016-11-26 20:02:43','2016-11-26 20:02:43'),(16,1,'admin','GET','10.240.1.31','[]','2016-11-26 20:02:44','2016-11-26 20:02:44'),(17,1,'admin','GET','10.240.1.18','[]','2016-11-26 20:02:45','2016-11-26 20:02:45'),(18,1,'admin','GET','10.240.0.185','[]','2016-11-26 20:02:58','2016-11-26 20:02:58'),(19,1,'admin','GET','10.240.0.181','[]','2016-11-26 20:03:38','2016-11-26 20:03:38'),(20,1,'admin/tasks','GET','10.240.0.224','{\"_pjax\":\"#pjax-container\"}','2016-11-26 20:03:49','2016-11-26 20:03:49'),(21,1,'admin/tasks','GET','10.240.1.67','[]','2016-11-26 20:03:57','2016-11-26 20:03:57'),(22,1,'admin/tasks','GET','10.240.0.196','{\"_pjax\":\"#pjax-container\"}','2016-11-26 20:04:35','2016-11-26 20:04:35'),(23,1,'admin','GET','10.240.1.45','[]','2016-11-27 01:46:52','2016-11-27 01:46:52'),(24,1,'admin/tasks','GET','10.240.0.224','[]','2016-11-27 06:16:43','2016-11-27 06:16:43'),(25,1,'admin/tasks','GET','10.240.1.67','[]','2016-11-27 06:16:47','2016-11-27 06:16:47'),(26,1,'admin/auth/menu','GET','10.240.0.181','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:16:50','2016-11-27 06:16:50'),(27,1,'admin/auth/menu','POST','10.240.0.181','{\"parent_id\":\"0\",\"title\":\"Blocks\",\"icon\":\"fa-bars\",\"uri\":\"\\/blocks\",\"roles\":[\"\"],\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\"}','2016-11-27 06:17:29','2016-11-27 06:17:29'),(28,1,'admin/auth/menu','GET','10.240.0.196','[]','2016-11-27 06:17:30','2016-11-27 06:17:30'),(29,1,'admin/auth/menu','POST','10.240.0.196','{\"parent_id\":\"0\",\"title\":\"Routines\",\"icon\":\"fa-bars\",\"uri\":\"\\/routines\",\"roles\":[\"\"],\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\"}','2016-11-27 06:17:39','2016-11-27 06:17:39'),(30,1,'admin/auth/menu','GET','10.240.0.215','[]','2016-11-27 06:17:39','2016-11-27 06:17:39'),(31,1,'admin/auth/menu','POST','10.240.0.196','{\"parent_id\":\"0\",\"title\":\"Chores\",\"icon\":\"fa-bars\",\"uri\":\"chores\",\"roles\":[\"\"],\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\"}','2016-11-27 06:17:52','2016-11-27 06:17:52'),(32,1,'admin/auth/menu','GET','10.240.0.215','[]','2016-11-27 06:17:52','2016-11-27 06:17:52'),(33,1,'admin/auth/menu','POST','10.240.1.4','{\"parent_id\":\"0\",\"title\":\"categories\",\"icon\":\"fa-bars\",\"uri\":\"categories\",\"roles\":[\"\"],\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\"}','2016-11-27 06:18:04','2016-11-27 06:18:04'),(34,1,'admin/auth/menu','GET','10.240.1.4','[]','2016-11-27 06:18:05','2016-11-27 06:18:05'),(35,1,'admin/auth/menu','POST','10.240.0.225','{\"parent_id\":\"0\",\"title\":\"contexts\",\"icon\":\"fa-bars\",\"uri\":\"contexts\",\"roles\":[\"\"],\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\"}','2016-11-27 06:18:14','2016-11-27 06:18:14'),(36,1,'admin/auth/menu','GET','10.240.0.196','[]','2016-11-27 06:18:14','2016-11-27 06:18:14'),(37,1,'admin/auth/menu','POST','10.240.1.31','{\"parent_id\":\"0\",\"title\":\"projects\",\"icon\":\"fa-bars\",\"uri\":\"projects\",\"roles\":[\"\"],\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\"}','2016-11-27 06:18:25','2016-11-27 06:18:25'),(38,1,'admin/auth/menu','GET','10.240.0.195','[]','2016-11-27 06:18:26','2016-11-27 06:18:26'),(39,1,'admin/auth/menu','POST','10.240.0.16','{\"parent_id\":\"0\",\"title\":\"moods\",\"icon\":\"fa-bars\",\"uri\":\"moods\",\"roles\":[\"\"],\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\"}','2016-11-27 06:18:33','2016-11-27 06:18:33'),(40,1,'admin/auth/menu','GET','10.240.1.4','[]','2016-11-27 06:18:33','2016-11-27 06:18:33'),(41,1,'admin/auth/menu','GET','10.240.1.4','[]','2016-11-27 06:18:35','2016-11-27 06:18:35'),(42,1,'admin/projects','GET','10.240.1.4','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:18:37','2016-11-27 06:18:37'),(43,1,'admin/contexts','GET','10.240.0.215','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:18:44','2016-11-27 06:18:44'),(44,1,'admin/tasks','GET','10.240.1.18','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:18:47','2016-11-27 06:18:47'),(45,1,'admin/categories','GET','10.240.1.67','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:18:52','2016-11-27 06:18:52'),(46,1,'admin/routines','GET','10.240.1.4','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:18:57','2016-11-27 06:18:57'),(47,1,'admin/chores','GET','10.240.1.67','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:19:09','2016-11-27 06:19:09'),(48,1,'admin/chores/301,302,303,304,305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,320','DELETE','10.240.1.31','{\"_method\":\"delete\",\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\"}','2016-11-27 06:19:15','2016-11-27 06:19:15'),(49,1,'admin/chores','GET','10.240.0.181','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:19:15','2016-11-27 06:19:15'),(50,1,'admin/chores','GET','10.240.0.16','{\"_pjax\":\"#pjax-container\",\"page\":\"7\"}','2016-11-27 06:19:19','2016-11-27 06:19:19'),(51,1,'admin/chores','GET','10.240.1.4','{\"_pjax\":\"#pjax-container\",\"page\":\"9\"}','2016-11-27 06:19:21','2016-11-27 06:19:21'),(52,1,'admin/chores/481,482,483,484,485,486,487,488,489,490,491,492,493,494,495,496,497,498,499,500','DELETE','10.240.1.4','{\"_method\":\"delete\",\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\"}','2016-11-27 06:19:29','2016-11-27 06:19:29'),(53,1,'admin/chores','GET','10.240.1.31','{\"page\":\"9\",\"_pjax\":\"#pjax-container\"}','2016-11-27 06:19:30','2016-11-27 06:19:30'),(54,1,'admin/chores/501,502,503,504,505,506,507,508,509,510,511,512,513,514,515,516,517,518,519,520','DELETE','10.240.1.4','{\"_method\":\"delete\",\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\"}','2016-11-27 06:19:41','2016-11-27 06:19:41'),(55,1,'admin/chores','GET','10.240.1.31','{\"page\":\"9\",\"_pjax\":\"#pjax-container\"}','2016-11-27 06:19:41','2016-11-27 06:19:41'),(56,1,'admin/chores','GET','10.240.0.215','{\"_pjax\":\"#pjax-container\",\"page\":\"5\"}','2016-11-27 06:19:44','2016-11-27 06:19:44'),(57,1,'admin/chores/401,402,403,404,405,406,407,408,409,410,411,412,413,414,415,416,417,418,419,420','DELETE','10.240.0.195','{\"_method\":\"delete\",\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\"}','2016-11-27 06:19:52','2016-11-27 06:19:52'),(58,1,'admin/chores','GET','10.240.1.4','{\"page\":\"5\",\"_pjax\":\"#pjax-container\"}','2016-11-27 06:19:52','2016-11-27 06:19:52'),(59,1,'admin/chores/421,422,423,424,425,426,427,428,429,430,431,432,433,434,435,436,437,438,439,440','DELETE','10.240.1.4','{\"_method\":\"delete\",\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\"}','2016-11-27 06:19:57','2016-11-27 06:19:57'),(60,1,'admin/chores','GET','10.240.0.215','{\"page\":\"5\",\"_pjax\":\"#pjax-container\"}','2016-11-27 06:19:57','2016-11-27 06:19:57'),(61,1,'admin/chores/441,442,443,444,445,446,447,448,449,450,451,452,453,454,455,456,457,458,459,460','DELETE','10.240.0.185','{\"_method\":\"delete\",\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\"}','2016-11-27 06:20:02','2016-11-27 06:20:02'),(62,1,'admin/chores','GET','10.240.1.67','{\"page\":\"5\",\"_pjax\":\"#pjax-container\"}','2016-11-27 06:20:02','2016-11-27 06:20:02'),(63,1,'admin/chores/461,462,463,464,465,466,467,468,469,470,471,472,473,474,475,476,477,478,479,480','DELETE','10.240.1.31','{\"_method\":\"delete\",\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\"}','2016-11-27 06:20:06','2016-11-27 06:20:06'),(64,1,'admin/chores','GET','10.240.0.181','{\"page\":\"5\",\"_pjax\":\"#pjax-container\"}','2016-11-27 06:20:06','2016-11-27 06:20:06'),(65,1,'admin/chores','GET','10.240.1.45','{\"_pjax\":\"#pjax-container\",\"page\":\"1\"}','2016-11-27 06:20:08','2016-11-27 06:20:08'),(66,1,'admin/chores/321,322,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339,340','DELETE','10.240.0.181','{\"_method\":\"delete\",\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\"}','2016-11-27 06:20:12','2016-11-27 06:20:12'),(67,1,'admin/chores','GET','10.240.0.196','{\"page\":\"1\",\"_pjax\":\"#pjax-container\"}','2016-11-27 06:20:13','2016-11-27 06:20:13'),(68,1,'admin/chores/341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,360','DELETE','10.240.1.45','{\"_method\":\"delete\",\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\"}','2016-11-27 06:20:17','2016-11-27 06:20:17'),(69,1,'admin/chores','GET','10.240.0.195','{\"page\":\"1\",\"_pjax\":\"#pjax-container\"}','2016-11-27 06:20:18','2016-11-27 06:20:18'),(70,1,'admin/chores/361,362,363,364,365,366,367,368,369,370,371,372,373,374,375,376,377,378,379,380','DELETE','10.240.1.31','{\"_method\":\"delete\",\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\"}','2016-11-27 06:20:22','2016-11-27 06:20:22'),(71,1,'admin/chores','GET','10.240.1.45','{\"page\":\"1\",\"_pjax\":\"#pjax-container\"}','2016-11-27 06:20:22','2016-11-27 06:20:22'),(72,1,'admin/chores/381,382,383,384,385,386,387,388,389,390,391,392,393,394,395,396,397,398,399,400','DELETE','10.240.1.45','{\"_method\":\"delete\",\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\"}','2016-11-27 06:20:25','2016-11-27 06:20:25'),(73,1,'admin/chores','GET','10.240.1.31','{\"page\":\"1\",\"_pjax\":\"#pjax-container\"}','2016-11-27 06:20:25','2016-11-27 06:20:25'),(74,1,'admin/routines','GET','10.240.0.181','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:20:28','2016-11-27 06:20:28'),(75,1,'admin/routines/1/edit','GET','10.240.1.31','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:20:33','2016-11-27 06:20:33'),(76,1,'admin/routines/1','DELETE','10.240.0.181','{\"_method\":\"delete\",\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\"}','2016-11-27 06:20:44','2016-11-27 06:20:44'),(77,1,'admin/routines/1/edit','GET','10.240.0.181','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:20:44','2016-11-27 06:20:44'),(78,1,'admin/routines','GET','10.240.1.4','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:20:50','2016-11-27 06:20:50'),(79,1,'admin/routines/2','DELETE','10.240.0.181','{\"_method\":\"delete\",\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\"}','2016-11-27 06:20:54','2016-11-27 06:20:54'),(80,1,'admin/routines','GET','10.240.0.215','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:20:55','2016-11-27 06:20:55'),(81,1,'admin/routines/3','DELETE','10.240.0.181','{\"_method\":\"delete\",\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\"}','2016-11-27 06:20:58','2016-11-27 06:20:58'),(82,1,'admin/routines','GET','10.240.0.215','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:20:58','2016-11-27 06:20:58'),(83,1,'admin/routines/4','DELETE','10.240.0.181','{\"_method\":\"delete\",\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\"}','2016-11-27 06:21:05','2016-11-27 06:21:05'),(84,1,'admin/routines','GET','10.240.0.181','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:21:05','2016-11-27 06:21:05'),(85,1,'admin/blocks','GET','10.240.1.67','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:21:07','2016-11-27 06:21:07'),(86,1,'admin/blocks','GET','10.240.1.67','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:21:08','2016-11-27 06:21:08'),(87,1,'admin/routines','GET','10.240.1.4','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:21:11','2016-11-27 06:21:11'),(88,1,'admin/routines/create','GET','10.240.0.181','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:21:14','2016-11-27 06:21:14'),(89,1,'admin/routines','POST','10.240.1.31','{\"name\":\"\\u0417\\u0443\\u0431\\u044b \\u0443\\u0442\\u0440\\u043e\",\"description\":\"\\u0437\\u0443\\u0431\\u044b \\u0443\\u0442\\u0440\\u043e\",\"category_id\":\"3\",\"priority\":\"9\",\"points\":\"5\",\"duration\":\"1\",\"start\":\"2016-11-27\",\"count\":\"1\",\"type\":\"daily\",\"weekdays\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"\"],\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\"}','2016-11-27 06:21:58','2016-11-27 06:21:58'),(90,1,'admin/routines','GET','10.240.0.16','[]','2016-11-27 06:21:59','2016-11-27 06:21:59'),(91,1,'admin/routines/create','GET','10.240.0.16','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:22:07','2016-11-27 06:22:07'),(92,1,'admin/routines','POST','10.240.0.181','{\"name\":\"\\u0417\\u0443\\u0431\\u044b \\u0432\\u0435\\u0447\\u0435\\u0440\",\"description\":\"\\u0417\\u0443\\u0431\\u044b \\u0432\\u0435\\u0447\\u0435\\u0440\",\"category_id\":\"3\",\"priority\":\"1\",\"points\":\"1\",\"duration\":\"1\",\"start\":\"2016-11-27\",\"count\":\"1\",\"type\":\"daily\",\"weekdays\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"\"],\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\"}','2016-11-27 06:22:47','2016-11-27 06:22:47'),(93,1,'admin/routines','GET','10.240.1.67','[]','2016-11-27 06:22:47','2016-11-27 06:22:47'),(94,1,'admin/routines/5/edit','GET','10.240.1.31','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:22:50','2016-11-27 06:22:50'),(95,1,'admin/routines/5','PUT','10.240.1.31','{\"name\":\"\\u0417\\u0443\\u0431\\u044b \\u0443\\u0442\\u0440\\u043e\",\"description\":\"\\u0437\\u0443\\u0431\\u044b \\u0443\\u0442\\u0440\\u043e\",\"category_id\":\"3\",\"priority\":\"9\",\"points\":\"5\",\"duration\":\"1\",\"start\":\"2016-11-27\",\"count\":\"25\",\"type\":\"daily\",\"weekdays\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"\"],\"_method\":\"PUT\",\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\"}','2016-11-27 06:23:02','2016-11-27 06:23:02'),(96,1,'admin/routines','GET','10.240.1.4','[]','2016-11-27 06:23:03','2016-11-27 06:23:03'),(97,1,'admin/routines/5/edit','GET','10.240.1.31','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:23:05','2016-11-27 06:23:05'),(98,1,'admin/routines/5','PUT','10.240.0.181','{\"name\":\"\\u0417\\u0443\\u0431\\u044b \\u0443\\u0442\\u0440\\u043e\",\"description\":\"\\u0437\\u0443\\u0431\\u044b \\u0443\\u0442\\u0440\\u043e\",\"category_id\":\"3\",\"priority\":\"9\",\"points\":\"5\",\"duration\":\"1\",\"start\":\"2016-11-27\",\"count\":\"25\",\"type\":\"daily\",\"weekdays\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"\"],\"_method\":\"PUT\",\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\"}','2016-11-27 06:23:11','2016-11-27 06:23:11'),(99,1,'admin/routines','GET','10.240.1.45','[]','2016-11-27 06:23:12','2016-11-27 06:23:12'),(100,1,'admin/routines/6/edit','GET','10.240.0.181','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:23:16','2016-11-27 06:23:16'),(101,1,'admin/routines/6','PUT','10.240.1.4','{\"name\":\"\\u0417\\u0443\\u0431\\u044b \\u0432\\u0435\\u0447\\u0435\\u0440\",\"description\":\"\\u0417\\u0443\\u0431\\u044b \\u0432\\u0435\\u0447\\u0435\\u0440\",\"category_id\":\"3\",\"priority\":\"1\",\"points\":\"1\",\"duration\":\"1\",\"start\":\"2016-11-27\",\"count\":\"26\",\"type\":\"daily\",\"weekdays\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"\"],\"_method\":\"PUT\",\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\"}','2016-11-27 06:23:21','2016-11-27 06:23:21'),(102,1,'admin/routines','GET','10.240.0.16','[]','2016-11-27 06:23:21','2016-11-27 06:23:21'),(103,1,'admin/routines/create','GET','10.240.1.31','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:23:26','2016-11-27 06:23:26'),(104,1,'admin/routines','POST','10.240.0.181','{\"name\":\"\\u0427\\u0430\\u0441 \\u043f\\u0445\\u043f\",\"description\":\"\",\"category_id\":\"1\",\"priority\":\"1\",\"points\":\"1\",\"duration\":\"2\",\"start\":\"\",\"count\":\"25\",\"type\":\"daily\",\"weekdays\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"\"],\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\"}','2016-11-27 06:23:54','2016-11-27 06:23:54'),(105,1,'admin/routines','POST','10.240.1.4','{\"name\":\"\\u0427\\u0430\\u0441 \\u043f\\u0445\\u043f\",\"description\":\"\",\"category_id\":\"1\",\"priority\":\"1\",\"points\":\"1\",\"duration\":\"2\",\"start\":\"2016-11-27\",\"count\":\"25\",\"type\":\"daily\",\"weekdays\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"\"],\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\"}','2016-11-27 06:24:03','2016-11-27 06:24:03'),(106,1,'admin/routines','GET','10.240.0.16','[]','2016-11-27 06:24:03','2016-11-27 06:24:03'),(107,1,'admin','GET','10.240.1.31','[]','2016-11-27 06:24:28','2016-11-27 06:24:28'),(108,1,'admin/chores','GET','10.240.0.185','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:24:31','2016-11-27 06:24:31'),(109,1,'admin/tasks','GET','10.240.1.31','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:24:39','2016-11-27 06:24:39'),(110,1,'admin/tasks/create','GET','10.240.1.31','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:24:43','2016-11-27 06:24:43'),(111,1,'admin/tasks','POST','10.240.0.195','{\"name\":\"\\u0423\\u0431\\u0440\\u0430\\u0442\\u044c\\u0441\\u044f\",\"description\":\"\\u0423\\u0431\\u0440\\u0430\\u0442\\u044c\\u0441\\u044f \\u0432 \\u0431\\u043e\\u043b\\u044c\\u0448\\u043e\\u0439 \\u043a\\u043e\\u043c\\u043d\\u0430\\u0442\\u0435\",\"status\":\"active\",\"start\":\"2016-11-27\",\"end\":\"2016-11-27\",\"finished\":\"\",\"user_id\":\"21\",\"category_id\":\"2\",\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\"}','2016-11-27 06:25:33','2016-11-27 06:25:33'),(112,1,'admin/tasks','GET','10.240.0.195','[]','2016-11-27 06:25:34','2016-11-27 06:25:34'),(113,1,'admin/projects','GET','10.240.0.196','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:25:39','2016-11-27 06:25:39'),(114,1,'admin/tasks','GET','10.240.1.18','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:25:44','2016-11-27 06:25:44'),(115,1,'admin/auth/menu','GET','10.240.1.18','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:26:16','2016-11-27 06:26:16'),(116,1,'admin/auth/menu','POST','10.240.1.31','{\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\",\"_order\":\"[{\\\"id\\\":1},{\\\"id\\\":2,\\\"children\\\":[{\\\"id\\\":3},{\\\"id\\\":4},{\\\"id\\\":5},{\\\"id\\\":6},{\\\"id\\\":7}]},{\\\"id\\\":8},{\\\"id\\\":9},{\\\"id\\\":10},{\\\"id\\\":11},{\\\"id\\\":14},{\\\"id\\\":13},{\\\"id\\\":12},{\\\"id\\\":15}]\"}','2016-11-27 06:26:37','2016-11-27 06:26:37'),(117,1,'admin/auth/menu','GET','10.240.0.181','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:26:38','2016-11-27 06:26:38'),(118,1,'admin','GET','10.240.0.196','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:26:44','2016-11-27 06:26:44'),(119,1,'admin','GET','10.240.0.181','[]','2016-11-27 06:27:57','2016-11-27 06:27:57'),(120,1,'admin/tasks','GET','10.240.0.181','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:27:59','2016-11-27 06:27:59'),(121,1,'admin/auth/users','GET','10.240.0.215','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:28:10','2016-11-27 06:28:10'),(122,1,'admin/tasks','GET','10.240.1.31','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:28:16','2016-11-27 06:28:16'),(123,1,'admin/tasks/8/edit','GET','10.240.0.181','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:28:25','2016-11-27 06:28:25'),(124,1,'admin/tasks/8','PUT','10.240.0.181','{\"name\":\"\\u0437\\u0430\\u0434\\u0430\\u043d\\u0438\\u0435\",\"description\":\"\",\"status\":\"active\",\"start\":\"2016-11-26\",\"end\":\"2016-11-27\",\"finished\":\"\",\"user_id\":\"21\",\"category_id\":\"1\",\"_method\":\"PUT\",\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\"}','2016-11-27 06:28:49','2016-11-27 06:28:49'),(125,1,'admin/tasks','GET','10.240.0.195','[]','2016-11-27 06:28:50','2016-11-27 06:28:50'),(126,1,'admin/tasks/9/edit','GET','10.240.0.195','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:28:53','2016-11-27 06:28:53'),(127,1,'admin/tasks/9','PUT','10.240.1.67','{\"name\":\"\\u0432\\u044b\\u0441\\u0442\\u0430\\u0432\\u0438\\u0442\\u044c \\u043d\\u043e\\u0443\\u0442\\u0431\\u0443\\u043a \",\"description\":\"\",\"status\":\"active\",\"start\":\"2016-11-26\",\"end\":\"2016-11-27\",\"finished\":\"\",\"user_id\":\"21\",\"category_id\":\"2\",\"_method\":\"PUT\",\"_token\":\"HIgmlGpLV08uF9I5JDkXfuYIy8DFJYm2jDYWPX3n\"}','2016-11-27 06:29:00','2016-11-27 06:29:00'),(128,1,'admin/tasks','GET','10.240.1.67','[]','2016-11-27 06:29:01','2016-11-27 06:29:01'),(129,1,'admin/tasks','GET','10.240.1.67','[]','2016-11-27 06:29:07','2016-11-27 06:29:07'),(130,1,'admin','GET','10.240.0.181','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:29:11','2016-11-27 06:29:11'),(131,1,'admin/tasks','GET','10.240.0.195','{\"_pjax\":\"#pjax-container\"}','2016-11-27 06:55:23','2016-11-27 06:55:23'),(132,1,'admin','GET','10.240.0.225','[]','2016-11-27 07:01:53','2016-11-27 07:01:53'),(133,1,'admin','GET','10.240.0.181','[]','2016-11-27 07:01:57','2016-11-27 07:01:57'),(134,1,'admin','GET','10.240.0.181','[]','2016-11-27 07:01:59','2016-11-27 07:01:59'),(135,1,'admin','GET','10.240.0.181','[]','2016-11-27 08:18:31','2016-11-27 08:18:31'),(136,1,'admin','GET','10.240.0.215','[]','2016-11-27 08:18:42','2016-11-27 08:18:42'),(137,1,'admin','GET','192.168.10.1','[]','2016-11-27 10:22:03','2016-11-27 10:22:03'),(138,1,'admin/tasks','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2016-11-27 10:22:09','2016-11-27 10:22:09'),(139,1,'admin/tasks/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2016-11-27 10:22:21','2016-11-27 10:22:21'),(140,1,'admin/tasks','POST','192.168.10.1','{\"name\":\"starts way early ends -day\",\"description\":\"starts way early ends -day\",\"status\":\"inbox\",\"start\":\"2016-11-10\",\"end\":\"2016-11-26\",\"finished\":\"\",\"user_id\":\"21\",\"category_id\":\"1\",\"_token\":\"STOyuGFQcRYnVbzPHEgcWCvn7UjVI6Zw83cleHOa\"}','2016-11-27 10:23:02','2016-11-27 10:23:02'),(141,1,'admin/tasks','GET','192.168.10.1','[]','2016-11-27 10:23:02','2016-11-27 10:23:02'),(142,1,'admin/tasks/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2016-11-27 10:23:16','2016-11-27 10:23:16'),(143,1,'admin/tasks','POST','192.168.10.1','{\"name\":\"start -99 ends today\",\"description\":\"\",\"status\":\"active\",\"start\":\"2016-11-08\",\"end\":\"2016-11-27\",\"finished\":\"\",\"user_id\":\"21\",\"category_id\":\"1\",\"_token\":\"STOyuGFQcRYnVbzPHEgcWCvn7UjVI6Zw83cleHOa\"}','2016-11-27 10:24:00','2016-11-27 10:24:00'),(144,1,'admin/tasks','GET','192.168.10.1','[]','2016-11-27 10:24:01','2016-11-27 10:24:01'),(145,1,'admin/tasks/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2016-11-27 10:24:08','2016-11-27 10:24:08'),(146,1,'admin/tasks','POST','192.168.10.1','{\"name\":\"start -99 end +1\",\"description\":\"\",\"status\":\"inbox\",\"start\":\"2016-11-10\",\"end\":\"2016-11-28\",\"finished\":\"\",\"user_id\":\"21\",\"category_id\":\"1\",\"_token\":\"STOyuGFQcRYnVbzPHEgcWCvn7UjVI6Zw83cleHOa\"}','2016-11-27 10:24:32','2016-11-27 10:24:32'),(147,1,'admin/tasks','GET','192.168.10.1','[]','2016-11-27 10:24:32','2016-11-27 10:24:32'),(148,1,'admin/tasks/11/edit','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2016-11-27 10:25:41','2016-11-27 10:25:41'),(149,1,'admin/tasks/11','PUT','192.168.10.1','{\"name\":\"starts way early ends -day\",\"description\":\"starts way early ends -day\",\"status\":\"inbox\",\"start\":\"2016-11-10\",\"end\":\"2016-11-25\",\"finished\":\"\",\"user_id\":\"21\",\"category_id\":\"1\",\"_method\":\"PUT\",\"_token\":\"STOyuGFQcRYnVbzPHEgcWCvn7UjVI6Zw83cleHOa\"}','2016-11-27 10:25:46','2016-11-27 10:25:46'),(150,1,'admin/tasks','GET','192.168.10.1','[]','2016-11-27 10:25:47','2016-11-27 10:25:47'),(151,1,'admin/tasks','GET','192.168.10.1','[]','2016-11-27 10:48:09','2016-11-27 10:48:09'),(152,1,'admin/tasks','GET','192.168.10.1','[]','2016-11-27 10:50:12','2016-11-27 10:50:12'),(153,1,'admin/routines','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2016-11-27 10:50:36','2016-11-27 10:50:36'),(154,1,'admin/routines','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2016-11-27 10:51:17','2016-11-27 10:51:17'),(155,1,'admin/tasks','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2016-11-27 10:58:52','2016-11-27 10:58:52'),(156,1,'admin/tasks/8/edit','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2016-11-27 10:58:56','2016-11-27 10:58:56'),(157,1,'admin/tasks/8/edit','GET','192.168.10.1','[]','2016-11-27 11:01:02','2016-11-27 11:01:02'),(158,1,'admin/tasks/8/edit','GET','192.168.10.1','[]','2016-11-27 11:01:58','2016-11-27 11:01:58'),(159,1,'admin/tasks/8/edit','GET','192.168.10.1','[]','2016-11-27 11:02:28','2016-11-27 11:02:28'),(160,1,'admin/tasks/8/edit','GET','192.168.10.1','[]','2016-11-27 11:05:48','2016-11-27 11:05:48'),(161,1,'admin/tasks/8/edit','GET','192.168.10.1','[]','2016-11-27 11:06:04','2016-11-27 11:06:04'),(162,1,'admin/tasks/8/edit','GET','192.168.10.1','[]','2016-11-27 11:08:10','2016-11-27 11:08:10'),(163,1,'admin/tasks/8/edit','GET','192.168.10.1','[]','2016-11-27 11:10:24','2016-11-27 11:10:24'),(164,1,'admin/tasks/8/edit','GET','192.168.10.1','[]','2016-11-27 11:11:29','2016-11-27 11:11:29'),(165,1,'admin/tasks/8/edit','GET','192.168.10.1','[]','2016-11-27 11:13:56','2016-11-27 11:13:56'),(166,1,'admin/tasks','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2016-11-27 11:14:58','2016-11-27 11:14:58'),(167,1,'admin/tasks/8/edit','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2016-11-27 11:15:01','2016-11-27 11:15:01'),(168,1,'admin/tasks/8/edit','GET','192.168.10.1','[]','2016-11-27 11:15:08','2016-11-27 11:15:08'),(169,1,'admin/tasks/8/edit','GET','192.168.10.1','[]','2016-11-27 11:15:33','2016-11-27 11:15:33');
/*!40000 ALTER TABLE `admin_operation_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_permissions`
--

DROP TABLE IF EXISTS `admin_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_permissions_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_permissions`
--

LOCK TABLES `admin_permissions` WRITE;
/*!40000 ALTER TABLE `admin_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_role_menu`
--

DROP TABLE IF EXISTS `admin_role_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_role_menu` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_menu_role_id_menu_id_index` (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_role_menu`
--

LOCK TABLES `admin_role_menu` WRITE;
/*!40000 ALTER TABLE `admin_role_menu` DISABLE KEYS */;
INSERT INTO `admin_role_menu` VALUES (1,2,NULL,NULL),(1,2,NULL,NULL);
/*!40000 ALTER TABLE `admin_role_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_role_permissions`
--

DROP TABLE IF EXISTS `admin_role_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_role_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_permissions_role_id_permission_id_index` (`role_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_role_permissions`
--

LOCK TABLES `admin_role_permissions` WRITE;
/*!40000 ALTER TABLE `admin_role_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_role_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_role_users`
--

DROP TABLE IF EXISTS `admin_role_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_role_users` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_users_role_id_user_id_index` (`role_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_role_users`
--

LOCK TABLES `admin_role_users` WRITE;
/*!40000 ALTER TABLE `admin_role_users` DISABLE KEYS */;
INSERT INTO `admin_role_users` VALUES (1,1,NULL,NULL),(1,1,NULL,NULL);
/*!40000 ALTER TABLE `admin_role_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_roles`
--

DROP TABLE IF EXISTS `admin_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_roles`
--

LOCK TABLES `admin_roles` WRITE;
/*!40000 ALTER TABLE `admin_roles` DISABLE KEYS */;
INSERT INTO `admin_roles` VALUES (1,'Administrator','administrator','2016-11-26 19:51:16','2016-11-26 19:51:16');
/*!40000 ALTER TABLE `admin_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_user_permissions`
--

DROP TABLE IF EXISTS `admin_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_user_permissions` (
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_user_permissions_user_id_permission_id_index` (`user_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_user_permissions`
--

LOCK TABLES `admin_user_permissions` WRITE;
/*!40000 ALTER TABLE `admin_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_users`
--

DROP TABLE IF EXISTS `admin_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(190) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_users_username_unique` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_users`
--

LOCK TABLES `admin_users` WRITE;
/*!40000 ALTER TABLE `admin_users` DISABLE KEYS */;
INSERT INTO `admin_users` VALUES (1,'admin','$2y$10$.W5N5Rap6j0ABkPAOgqRVObZsLuDZSbvTsXShKz09kU.QWW6WOyeK','Administrator',NULL,'2016-11-26 19:51:16','2016-11-26 19:51:16');
/*!40000 ALTER TABLE `admin_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `block_routine`
--

DROP TABLE IF EXISTS `block_routine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `block_routine` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `block_id` int(10) unsigned NOT NULL,
  `routine_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `block_routine`
--

LOCK TABLES `block_routine` WRITE;
/*!40000 ALTER TABLE `block_routine` DISABLE KEYS */;
INSERT INTO `block_routine` VALUES (1,1,1,NULL,NULL),(2,2,1,NULL,NULL),(3,3,1,NULL,NULL),(4,4,1,NULL,NULL),(5,1,2,NULL,NULL),(6,2,2,NULL,NULL),(7,3,2,NULL,NULL),(8,4,2,NULL,NULL),(9,1,3,NULL,NULL),(10,3,3,NULL,NULL),(11,1,4,NULL,NULL),(12,2,4,NULL,NULL),(13,3,4,NULL,NULL),(14,4,4,NULL,NULL);
/*!40000 ALTER TABLE `block_routine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `block_task`
--

DROP TABLE IF EXISTS `block_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `block_task` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `block_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `block_task`
--

LOCK TABLES `block_task` WRITE;
/*!40000 ALTER TABLE `block_task` DISABLE KEYS */;
INSERT INTO `block_task` VALUES (1,1,1,NULL,NULL),(2,2,1,NULL,NULL),(3,3,1,NULL,NULL),(4,1,2,NULL,NULL),(5,2,2,NULL,NULL),(6,3,2,NULL,NULL),(7,4,2,NULL,NULL),(8,1,3,NULL,NULL),(9,2,3,NULL,NULL),(10,3,3,NULL,NULL),(11,4,3,NULL,NULL),(12,1,6,NULL,NULL),(13,2,6,NULL,NULL),(14,3,6,NULL,NULL),(15,4,6,NULL,NULL),(16,2,7,NULL,NULL),(17,3,7,NULL,NULL),(18,1,8,NULL,NULL),(19,2,8,NULL,NULL),(20,3,8,NULL,NULL),(21,4,8,NULL,NULL),(22,1,9,NULL,NULL),(23,2,9,NULL,NULL),(24,3,9,NULL,NULL),(25,4,9,NULL,NULL);
/*!40000 ALTER TABLE `block_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blocks`
--

DROP TABLE IF EXISTS `blocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blocks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `start` time NOT NULL,
  `end` time NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blocks`
--

LOCK TABLES `blocks` WRITE;
/*!40000 ALTER TABLE `blocks` DISABLE KEYS */;
INSERT INTO `blocks` VALUES (1,'morning','default block','07:00:00','12:00:00',21,'2016-11-21 04:43:07','2016-11-21 04:43:07'),(2,'day','default block','12:00:00','18:00:00',21,'2016-11-21 04:43:07','2016-11-21 04:43:07'),(3,'evening','default block','18:00:00','23:59:59',21,'2016-11-21 04:43:07','2016-11-21 04:43:07'),(4,'night','default block','00:00:00','06:59:59',21,'2016-11-21 04:43:07','2016-11-21 04:43:07');
/*!40000 ALTER TABLE `blocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Работа','','2016-11-21 04:42:19','2016-11-21 04:42:19'),(2,'Лично','','2016-11-21 04:53:28','2016-11-21 04:53:28'),(3,'Здоровье','','2016-11-21 04:54:52','2016-11-21 04:54:52'),(4,'Окружающие','Отношения с людьми','2016-11-21 04:55:44','2016-11-21 04:55:44'),(5,'Tasks project','','2016-11-21 04:56:21','2016-11-21 04:56:21');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chats`
--

DROP TABLE IF EXISTS `chats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chats` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `receiver_id` int(10) unsigned NOT NULL,
  `chat` text COLLATE utf8_unicode_ci NOT NULL,
  `read` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chats`
--

LOCK TABLES `chats` WRITE;
/*!40000 ALTER TABLE `chats` DISABLE KEYS */;
/*!40000 ALTER TABLE `chats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chores`
--

DROP TABLE IF EXISTS `chores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chores` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `finished` date DEFAULT NULL,
  `routine_id` int(11) NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=622 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chores`
--

LOCK TABLES `chores` WRITE;
/*!40000 ALTER TABLE `chores` DISABLE KEYS */;
INSERT INTO `chores` VALUES (546,'Зубы утро - 5','зубы утро','2016-11-27','2016-11-28','2016-11-29',5,'skipped','2016-11-27 06:24:17','2016-11-29 01:41:48'),(547,'Зубы утро - 5','зубы утро','2016-11-28','2016-11-29',NULL,5,'active','2016-11-27 06:24:17','2016-11-27 06:24:17'),(548,'Зубы утро - 5','зубы утро','2016-11-29','2016-11-30',NULL,5,'active','2016-11-27 06:24:17','2016-11-27 06:24:17'),(549,'Зубы утро - 5','зубы утро','2016-11-30','2016-12-01',NULL,5,'active','2016-11-27 06:24:17','2016-11-27 06:24:17'),(550,'Зубы утро - 5','зубы утро','2016-12-01','2016-12-02',NULL,5,'active','2016-11-27 06:24:17','2016-11-27 06:24:17'),(551,'Зубы утро - 5','зубы утро','2016-12-02','2016-12-03',NULL,5,'active','2016-11-27 06:24:17','2016-11-27 06:24:17'),(552,'Зубы утро - 5','зубы утро','2016-12-03','2016-12-04',NULL,5,'active','2016-11-27 06:24:17','2016-11-27 06:24:17'),(553,'Зубы утро - 5','зубы утро','2016-12-04','2016-12-05',NULL,5,'active','2016-11-27 06:24:17','2016-11-27 06:24:17'),(554,'Зубы утро - 5','зубы утро','2016-12-05','2016-12-06',NULL,5,'active','2016-11-27 06:24:17','2016-11-27 06:24:17'),(555,'Зубы утро - 5','зубы утро','2016-12-06','2016-12-07',NULL,5,'active','2016-11-27 06:24:17','2016-11-27 06:24:17'),(556,'Зубы утро - 5','зубы утро','2016-12-07','2016-12-08',NULL,5,'active','2016-11-27 06:24:17','2016-11-27 06:24:17'),(557,'Зубы утро - 5','зубы утро','2016-12-08','2016-12-09',NULL,5,'active','2016-11-27 06:24:17','2016-11-27 06:24:17'),(558,'Зубы утро - 5','зубы утро','2016-12-09','2016-12-10',NULL,5,'active','2016-11-27 06:24:17','2016-11-27 06:24:17'),(559,'Зубы утро - 5','зубы утро','2016-12-10','2016-12-11',NULL,5,'active','2016-11-27 06:24:17','2016-11-27 06:24:17'),(560,'Зубы утро - 5','зубы утро','2016-12-11','2016-12-12',NULL,5,'active','2016-11-27 06:24:17','2016-11-27 06:24:17'),(561,'Зубы утро - 5','зубы утро','2016-12-12','2016-12-13',NULL,5,'active','2016-11-27 06:24:17','2016-11-27 06:24:17'),(562,'Зубы утро - 5','зубы утро','2016-12-13','2016-12-14',NULL,5,'active','2016-11-27 06:24:17','2016-11-27 06:24:17'),(563,'Зубы утро - 5','зубы утро','2016-12-14','2016-12-15',NULL,5,'active','2016-11-27 06:24:17','2016-11-27 06:24:17'),(564,'Зубы утро - 5','зубы утро','2016-12-15','2016-12-16',NULL,5,'active','2016-11-27 06:24:17','2016-11-27 06:24:17'),(565,'Зубы утро - 5','зубы утро','2016-12-16','2016-12-17',NULL,5,'active','2016-11-27 06:24:18','2016-11-27 06:24:18'),(566,'Зубы утро - 5','зубы утро','2016-12-17','2016-12-18',NULL,5,'active','2016-11-27 06:24:18','2016-11-27 06:24:18'),(567,'Зубы утро - 5','зубы утро','2016-12-18','2016-12-19',NULL,5,'active','2016-11-27 06:24:18','2016-11-27 06:24:18'),(568,'Зубы утро - 5','зубы утро','2016-12-19','2016-12-20',NULL,5,'active','2016-11-27 06:24:18','2016-11-27 06:24:18'),(569,'Зубы утро - 5','зубы утро','2016-12-20','2016-12-21',NULL,5,'active','2016-11-27 06:24:18','2016-11-27 06:24:18'),(570,'Зубы утро - 5','зубы утро','2016-12-21','2016-12-22',NULL,5,'active','2016-11-27 06:24:18','2016-11-27 06:24:18'),(571,'Зубы вечер - 6','Зубы вечер','2016-11-27','2016-11-28','2016-11-29',6,'skipped','2016-11-27 06:24:21','2016-11-29 01:41:49'),(572,'Зубы вечер - 6','Зубы вечер','2016-11-28','2016-11-29',NULL,6,'active','2016-11-27 06:24:21','2016-11-27 06:24:21'),(573,'Зубы вечер - 6','Зубы вечер','2016-11-29','2016-11-30',NULL,6,'active','2016-11-27 06:24:21','2016-11-27 06:24:21'),(574,'Зубы вечер - 6','Зубы вечер','2016-11-30','2016-12-01',NULL,6,'active','2016-11-27 06:24:21','2016-11-27 06:24:21'),(575,'Зубы вечер - 6','Зубы вечер','2016-12-01','2016-12-02',NULL,6,'active','2016-11-27 06:24:21','2016-11-27 06:24:21'),(576,'Зубы вечер - 6','Зубы вечер','2016-12-02','2016-12-03',NULL,6,'active','2016-11-27 06:24:21','2016-11-27 06:24:21'),(577,'Зубы вечер - 6','Зубы вечер','2016-12-03','2016-12-04',NULL,6,'active','2016-11-27 06:24:21','2016-11-27 06:24:21'),(578,'Зубы вечер - 6','Зубы вечер','2016-12-04','2016-12-05',NULL,6,'active','2016-11-27 06:24:21','2016-11-27 06:24:21'),(579,'Зубы вечер - 6','Зубы вечер','2016-12-05','2016-12-06',NULL,6,'active','2016-11-27 06:24:21','2016-11-27 06:24:21'),(580,'Зубы вечер - 6','Зубы вечер','2016-12-06','2016-12-07',NULL,6,'active','2016-11-27 06:24:21','2016-11-27 06:24:21'),(581,'Зубы вечер - 6','Зубы вечер','2016-12-07','2016-12-08',NULL,6,'active','2016-11-27 06:24:21','2016-11-27 06:24:21'),(582,'Зубы вечер - 6','Зубы вечер','2016-12-08','2016-12-09',NULL,6,'active','2016-11-27 06:24:21','2016-11-27 06:24:21'),(583,'Зубы вечер - 6','Зубы вечер','2016-12-09','2016-12-10',NULL,6,'active','2016-11-27 06:24:21','2016-11-27 06:24:21'),(584,'Зубы вечер - 6','Зубы вечер','2016-12-10','2016-12-11',NULL,6,'active','2016-11-27 06:24:21','2016-11-27 06:24:21'),(585,'Зубы вечер - 6','Зубы вечер','2016-12-11','2016-12-12',NULL,6,'active','2016-11-27 06:24:21','2016-11-27 06:24:21'),(586,'Зубы вечер - 6','Зубы вечер','2016-12-12','2016-12-13',NULL,6,'active','2016-11-27 06:24:21','2016-11-27 06:24:21'),(587,'Зубы вечер - 6','Зубы вечер','2016-12-13','2016-12-14',NULL,6,'active','2016-11-27 06:24:21','2016-11-27 06:24:21'),(588,'Зубы вечер - 6','Зубы вечер','2016-12-14','2016-12-15',NULL,6,'active','2016-11-27 06:24:21','2016-11-27 06:24:21'),(589,'Зубы вечер - 6','Зубы вечер','2016-12-15','2016-12-16',NULL,6,'active','2016-11-27 06:24:21','2016-11-27 06:24:21'),(590,'Зубы вечер - 6','Зубы вечер','2016-12-16','2016-12-17',NULL,6,'active','2016-11-27 06:24:21','2016-11-27 06:24:21'),(591,'Зубы вечер - 6','Зубы вечер','2016-12-17','2016-12-18',NULL,6,'active','2016-11-27 06:24:21','2016-11-27 06:24:21'),(592,'Зубы вечер - 6','Зубы вечер','2016-12-18','2016-12-19',NULL,6,'active','2016-11-27 06:24:21','2016-11-27 06:24:21'),(593,'Зубы вечер - 6','Зубы вечер','2016-12-19','2016-12-20',NULL,6,'active','2016-11-27 06:24:21','2016-11-27 06:24:21'),(594,'Зубы вечер - 6','Зубы вечер','2016-12-20','2016-12-21',NULL,6,'active','2016-11-27 06:24:21','2016-11-27 06:24:21'),(595,'Зубы вечер - 6','Зубы вечер','2016-12-21','2016-12-22',NULL,6,'active','2016-11-27 06:24:21','2016-11-27 06:24:21'),(596,'Зубы вечер - 6','Зубы вечер','2016-12-22','2016-12-23',NULL,6,'active','2016-11-27 06:24:21','2016-11-27 06:24:21'),(597,'Час пхп - 7','','2016-11-27','2016-11-29',NULL,7,'active','2016-11-27 06:24:25','2016-11-27 06:24:25'),(598,'Час пхп - 7','','2016-11-28','2016-11-30',NULL,7,'active','2016-11-27 06:24:25','2016-11-27 06:24:25'),(599,'Час пхп - 7','','2016-11-29','2016-12-01',NULL,7,'active','2016-11-27 06:24:25','2016-11-27 06:24:25'),(600,'Час пхп - 7','','2016-11-30','2016-12-02',NULL,7,'active','2016-11-27 06:24:25','2016-11-27 06:24:25'),(601,'Час пхп - 7','','2016-12-01','2016-12-03',NULL,7,'active','2016-11-27 06:24:25','2016-11-27 06:24:25'),(602,'Час пхп - 7','','2016-12-02','2016-12-04',NULL,7,'active','2016-11-27 06:24:25','2016-11-27 06:24:25'),(603,'Час пхп - 7','','2016-12-03','2016-12-05',NULL,7,'active','2016-11-27 06:24:25','2016-11-27 06:24:25'),(604,'Час пхп - 7','','2016-12-04','2016-12-06',NULL,7,'active','2016-11-27 06:24:25','2016-11-27 06:24:25'),(605,'Час пхп - 7','','2016-12-05','2016-12-07',NULL,7,'active','2016-11-27 06:24:25','2016-11-27 06:24:25'),(606,'Час пхп - 7','','2016-12-06','2016-12-08',NULL,7,'active','2016-11-27 06:24:25','2016-11-27 06:24:25'),(607,'Час пхп - 7','','2016-12-07','2016-12-09',NULL,7,'active','2016-11-27 06:24:25','2016-11-27 06:24:25'),(608,'Час пхп - 7','','2016-12-08','2016-12-10',NULL,7,'active','2016-11-27 06:24:25','2016-11-27 06:24:25'),(609,'Час пхп - 7','','2016-12-09','2016-12-11',NULL,7,'active','2016-11-27 06:24:25','2016-11-27 06:24:25'),(610,'Час пхп - 7','','2016-12-10','2016-12-12',NULL,7,'active','2016-11-27 06:24:25','2016-11-27 06:24:25'),(611,'Час пхп - 7','','2016-12-11','2016-12-13',NULL,7,'active','2016-11-27 06:24:25','2016-11-27 06:24:25'),(612,'Час пхп - 7','','2016-12-12','2016-12-14',NULL,7,'active','2016-11-27 06:24:25','2016-11-27 06:24:25'),(613,'Час пхп - 7','','2016-12-13','2016-12-15',NULL,7,'active','2016-11-27 06:24:25','2016-11-27 06:24:25'),(614,'Час пхп - 7','','2016-12-14','2016-12-16',NULL,7,'active','2016-11-27 06:24:25','2016-11-27 06:24:25'),(615,'Час пхп - 7','','2016-12-15','2016-12-17',NULL,7,'active','2016-11-27 06:24:25','2016-11-27 06:24:25'),(616,'Час пхп - 7','','2016-12-16','2016-12-18',NULL,7,'active','2016-11-27 06:24:25','2016-11-27 06:24:25'),(617,'Час пхп - 7','','2016-12-17','2016-12-19',NULL,7,'active','2016-11-27 06:24:25','2016-11-27 06:24:25'),(618,'Час пхп - 7','','2016-12-18','2016-12-20',NULL,7,'active','2016-11-27 06:24:25','2016-11-27 06:24:25'),(619,'Час пхп - 7','','2016-12-19','2016-12-21',NULL,7,'active','2016-11-27 06:24:25','2016-11-27 06:24:25'),(620,'Час пхп - 7','','2016-12-20','2016-12-22',NULL,7,'active','2016-11-27 06:24:25','2016-11-27 06:24:25'),(621,'Час пхп - 7','','2016-12-21','2016-12-23',NULL,7,'active','2016-11-27 06:24:25','2016-11-27 06:24:25');
/*!40000 ALTER TABLE `chores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `post_id` int(10) unsigned NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `context_task`
--

DROP TABLE IF EXISTS `context_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `context_task` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `context_id` int(10) unsigned NOT NULL,
  `task_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `context_task`
--

LOCK TABLES `context_task` WRITE;
/*!40000 ALTER TABLE `context_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `context_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contexts`
--

DROP TABLE IF EXISTS `contexts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contexts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `color` text COLLATE utf8_unicode_ci NOT NULL,
  `hidden` tinyint(1) DEFAULT '0',
  `user_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contexts`
--

LOCK TABLES `contexts` WRITE;
/*!40000 ALTER TABLE `contexts` DISABLE KEYS */;
/*!40000 ALTER TABLE `contexts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_06_01_000001_create_oauth_auth_codes_table',1),(4,'2016_06_01_000002_create_oauth_access_tokens_table',1),(5,'2016_06_01_000003_create_oauth_refresh_tokens_table',1),(6,'2016_06_01_000004_create_oauth_clients_table',1),(7,'2016_06_01_000005_create_oauth_personal_access_clients_table',1),(8,'2016_11_16_135647_create_categories_table',1),(9,'2016_11_16_143410_create_moods_table',1),(10,'2016_11_16_143733_create_projects_table',1),(11,'2016_11_16_230014_create_routines_table',1),(12,'2016_11_16_234403_create_chores_table',1),(13,'2016_11_17_001118_create_block_routine_table',1),(14,'2016_11_03_125532_create_tasks_table',2),(15,'2016_11_03_173536_create_contexts_table',2),(16,'2016_11_03_173700_create_contexts_tasks_table',2),(17,'2016_11_13_202518_make_blocks_table',2),(18,'2016_11_16_122754_create_block_task_table',2),(19,'2016_11_10_213903_create_chats_table',3),(20,'2016_11_02_090557_create_posts_table',4),(21,'2016_11_02_134543_create_tags_table',4),(22,'2016_11_02_141028_create_post_tag_table',4),(23,'2016_11_03_054736_create_comments_table',4),(24,'2016_01_04_173148_create_admin_tables',5);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moods`
--

DROP TABLE IF EXISTS `moods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `value` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moods`
--

LOCK TABLES `moods` WRITE;
/*!40000 ALTER TABLE `moods` DISABLE KEYS */;
/*!40000 ALTER TABLE `moods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_auth_codes`
--

LOCK TABLES `oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_personal_access_clients`
--

LOCK TABLES `oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_tag`
--

DROP TABLE IF EXISTS `post_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_tag` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_tag`
--

LOCK TABLES `post_tag` WRITE;
/*!40000 ALTER TABLE `post_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `post_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects`
--

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `routines`
--

DROP TABLE IF EXISTS `routines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `routines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `priority` int(10) unsigned DEFAULT NULL,
  `points` int(10) unsigned DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `duration` int(11) NOT NULL,
  `start` date NOT NULL,
  `count` int(11) NOT NULL,
  `weekdays` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '["monday","tuesday","wednesday","thursday","friday","saturday","sunday"]',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `routines`
--

LOCK TABLES `routines` WRITE;
/*!40000 ALTER TABLE `routines` DISABLE KEYS */;
INSERT INTO `routines` VALUES (5,'Зубы утро','зубы утро',3,9,5,'daily',1,'2016-11-27',25,'\"1,2,3,4,5,6,7\"','2016-11-27 06:21:58','2016-11-27 06:23:11'),(6,'Зубы вечер','Зубы вечер',3,1,1,'daily',1,'2016-11-27',26,'\"1,2,3,4,5,6,7\"','2016-11-27 06:22:47','2016-11-27 06:23:21'),(7,'Час пхп','',1,1,1,'daily',2,'2016-11-27',25,'\"1,2,3,4,5,6,7\"','2016-11-27 06:24:03','2016-11-27 06:24:03');
/*!40000 ALTER TABLE `routines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start` date DEFAULT NULL,
  `end` date DEFAULT NULL,
  `finished` date DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `mood_id` int(10) unsigned DEFAULT NULL,
  `project_id` int(10) unsigned DEFAULT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks`
--

LOCK TABLES `tasks` WRITE;
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
INSERT INTO `tasks` VALUES (8,'задание','','active','2016-11-26','2016-11-27',NULL,21,1,NULL,NULL,0,'2016-11-26 04:22:31','2016-11-27 06:28:49'),(9,'выставить ноутбук ','','active','2016-11-26','2016-11-27',NULL,21,2,NULL,NULL,0,'2016-11-26 04:23:38','2016-11-27 06:29:01'),(10,'Убраться','Убраться в большой комнате','active','2016-11-27','2016-11-27',NULL,21,2,NULL,NULL,0,'2016-11-27 06:25:33','2016-11-27 06:25:33'),(11,'starts way early ends -day','starts way early ends -day','inbox','2016-11-10','2016-11-25',NULL,21,1,NULL,NULL,0,'2016-11-27 10:23:02','2016-11-27 10:25:46'),(12,'start -99 ends today','','active','2016-11-08','2016-11-27',NULL,21,1,NULL,NULL,0,'2016-11-27 10:24:00','2016-11-27 10:24:00'),(13,'start -99 end +1','','inbox','2016-11-10','2016-11-28',NULL,21,1,NULL,NULL,0,'2016-11-27 10:24:32','2016-11-27 10:24:32');
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Micheal Bailey','dstiedemann@example.com','$2y$10$USn/TGqis8gVHYDJGYFyfu4JUizEmVXJA8zui0/awA115foH51X6.',0,'lZBQqwnXruOmViiRDQPlKb1tUjL9dg9g90fEaoAfjo7q6o4fd6sLxyKLjbHF','2016-11-21 04:32:33','2016-11-27 09:38:49'),(2,'Prof. Davonte Pollich','reilly.blanche@example.org','$2y$10$USn/TGqis8gVHYDJGYFyfu4JUizEmVXJA8zui0/awA115foH51X6.',0,'CE5EB2mY3H','2016-11-21 04:32:33','2016-11-21 04:32:33'),(3,'Anissa Kunde','tavares.jaskolski@example.net','$2y$10$USn/TGqis8gVHYDJGYFyfu4JUizEmVXJA8zui0/awA115foH51X6.',0,'BuyMgIYpdf','2016-11-21 04:32:33','2016-11-21 04:32:33'),(4,'Marisa Breitenberg','irunolfsdottir@example.com','$2y$10$USn/TGqis8gVHYDJGYFyfu4JUizEmVXJA8zui0/awA115foH51X6.',0,'79rDAMMyyH','2016-11-21 04:32:33','2016-11-21 04:32:33'),(5,'Talia Cassin II','jerde.abdul@example.org','$2y$10$USn/TGqis8gVHYDJGYFyfu4JUizEmVXJA8zui0/awA115foH51X6.',0,'EZjeXrwNro','2016-11-21 04:32:33','2016-11-21 04:32:33'),(6,'Clara Wilderman PhD','gus.murray@example.net','$2y$10$USn/TGqis8gVHYDJGYFyfu4JUizEmVXJA8zui0/awA115foH51X6.',0,'Y0cgZrzg3J','2016-11-21 04:32:33','2016-11-21 04:32:33'),(7,'Rhea Fisher','hane.ethyl@example.org','$2y$10$USn/TGqis8gVHYDJGYFyfu4JUizEmVXJA8zui0/awA115foH51X6.',0,'Nrgt0sWzTt','2016-11-21 04:32:33','2016-11-21 04:32:33'),(8,'Herminia Turcotte','cgrady@example.net','$2y$10$USn/TGqis8gVHYDJGYFyfu4JUizEmVXJA8zui0/awA115foH51X6.',0,'StS5wVoDYu','2016-11-21 04:32:33','2016-11-21 04:32:33'),(9,'Eli Wiegand','itorphy@example.net','$2y$10$USn/TGqis8gVHYDJGYFyfu4JUizEmVXJA8zui0/awA115foH51X6.',0,'19bbVqHuXi','2016-11-21 04:32:33','2016-11-21 04:32:33'),(10,'Maryam Lindgren DVM','luciano.beahan@example.com','$2y$10$USn/TGqis8gVHYDJGYFyfu4JUizEmVXJA8zui0/awA115foH51X6.',0,'rWLcrXgRoL','2016-11-21 04:32:33','2016-11-21 04:32:33'),(11,'Jedidiah Christiansen','yflatley@example.net','$2y$10$USn/TGqis8gVHYDJGYFyfu4JUizEmVXJA8zui0/awA115foH51X6.',0,'rK4cGyBwX2','2016-11-21 04:32:33','2016-11-21 04:32:33'),(12,'Rebecca Parisian','matilde.lynch@example.org','$2y$10$USn/TGqis8gVHYDJGYFyfu4JUizEmVXJA8zui0/awA115foH51X6.',0,'CNW6RTQuHX','2016-11-21 04:32:33','2016-11-21 04:32:33'),(13,'Mr. Wilbert Predovic I','dino30@example.net','$2y$10$USn/TGqis8gVHYDJGYFyfu4JUizEmVXJA8zui0/awA115foH51X6.',0,'7JxiCczadS','2016-11-21 04:32:33','2016-11-21 04:32:33'),(14,'Kaylie O\'Kon II','cpfeffer@example.com','$2y$10$USn/TGqis8gVHYDJGYFyfu4JUizEmVXJA8zui0/awA115foH51X6.',0,'VgxocKxl1b','2016-11-21 04:32:33','2016-11-21 04:32:33'),(15,'Dr. Bridgette Lind','randall35@example.com','$2y$10$USn/TGqis8gVHYDJGYFyfu4JUizEmVXJA8zui0/awA115foH51X6.',0,'nGejdwGBon','2016-11-21 04:32:33','2016-11-21 04:32:33'),(16,'Bonnie Cronin','imertz@example.org','$2y$10$USn/TGqis8gVHYDJGYFyfu4JUizEmVXJA8zui0/awA115foH51X6.',0,'jmV5wFzR0c','2016-11-21 04:32:33','2016-11-21 04:32:33'),(17,'Gavin Lesch','schaefer.abby@example.net','$2y$10$USn/TGqis8gVHYDJGYFyfu4JUizEmVXJA8zui0/awA115foH51X6.',0,'eCqvwP0NUR','2016-11-21 04:32:33','2016-11-21 04:32:33'),(18,'Rosalee Corwin','wisoky.arnulfo@example.org','$2y$10$USn/TGqis8gVHYDJGYFyfu4JUizEmVXJA8zui0/awA115foH51X6.',0,'2JKyZoRoMV','2016-11-21 04:32:33','2016-11-21 04:32:33'),(19,'Remington Bailey','caesar.waelchi@example.net','$2y$10$USn/TGqis8gVHYDJGYFyfu4JUizEmVXJA8zui0/awA115foH51X6.',0,'4efbbKYwoA','2016-11-21 04:32:33','2016-11-21 04:32:33'),(20,'Alyce Morar','bins.tania@example.net','$2y$10$USn/TGqis8gVHYDJGYFyfu4JUizEmVXJA8zui0/awA115foH51X6.',0,'bg6gXPKEpV','2016-11-21 04:32:33','2016-11-21 04:32:33'),(21,'testName','test@test.com','$2y$10$k8diLBNxlMICgkWdrEjTS.v1QIw5cpt6AnH.XDa10Hc2Av.KVSqFS',0,'iLl2tAQIGgHH7KC0RbDyNR0UoPU9Odu4h6Pgd8UPnnUtJ3d9ixYq8uS5MW81','2016-11-21 04:33:06','2016-11-21 04:36:15');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-28 22:51:11
