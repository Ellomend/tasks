const {mix} = require('laravel-mix');


mix.js('resources/assets/js/app.js', 'public/js')
  //bootstrap scss
  .sass()
  .sass('resources/assets/sass/app.scss', 'public/css')
  //pages
  .sass('resources/assets/sass/pages.scss', 'public/css')
  .js('resources/assets/js/pages.js', 'public/js/pages')
  .sass('resources/assets/sass/styles.scss', 'public/css');