<?php

namespace Modules\Tasks\Database\Seeders;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Block;
use App\Context;
use App\Task;

class TasksDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call("OthersTableSeeder");
//        $blocks = factory(Block::class, 10)->create();

        $users = factory(User::class, 20)->create();
        $tasks = factory(Task::class, 120)->create();
        $contexts = factory(Context::class, 10)->create();
        $today_task1 = factory(Task::class)->make([
            'name' => 'today331',
            'start' => Carbon::now()->toDateTimeString(),
            'end' => Carbon::now()->addDay(1)->toDateTimeString()
        ])->save();
        $today_task2 = factory(Task::class)->make([
            'name' => 'today2',
            'start' => Carbon::now()->subHour(14)->toDateTimeString(),
            'end' => Carbon::now()->addDay(1)->subHour(12)->toDateTimeString()
        ])->save();
        $today_task3 = factory(Task::class)->make([
            'name' => 'today3',
            'start' => Carbon::now()->subHour(12)->toDateTimeString(),
            'end' => Carbon::now()->addDay(1)->subHour(10)->toDateTimeString()
        ])->save();
        $today_task4 = factory(Task::class)->make([
            'name' => 'today4',
            'start' => Carbon::now()->subHour(6)->toDateTimeString(),
            'end' => Carbon::now()->addDay(1)->subHour(4)->toDateTimeString()
        ])->save();
        $today_task5 = factory(Task::class)->make([
            'name' => 'today5',
            'start' => Carbon::now()->subHour(3)->toDateTimeString(),
            'end' => Carbon::now()->addDay(1)->subHour(1)->toDateTimeString()
        ])->save();
        $tomorrow_task = factory(Task::class)->make([
            'name' => 'today_tomorrow',
            'start' => Carbon::now()->addDay(1)->toDateTimeString(),
            'end' => Carbon::now()->addDay(6)->toDateTimeString()
        ])->save();
        $next_week_task = factory(Task::class)->make([
            'name' => 'next week task',
            'start' => Carbon::now()->addDay(7)->toDateTimeString(),
            'end' => Carbon::now()->addDay(12)->toDateTimeString()
        ])->save();
        $block_morning = factory(Block::class)->make([
            'name' => 'morning',
            'description' => 'default block',
            'start' => '7:00:00',
            'end' => '12:00:00',
            'user_id' => 1
        ])->save();
        $block_day = factory(Block::class)->make([
            'name' => 'day',
            'description' => 'default block',
            'start' => '12:00:00',
            'end' => '18:00:00',
            'user_id' => 1
        ])->save();
        $block_evening = factory(Block::class)->make([
            'name' => 'evening',
            'description' => 'default block',
            'start' => '18:00:00',
            'end' => '23:59:59',
            'user_id' => 1
        ])->save();
        $block_evening = factory(Block::class)->make([
            'name' => 'night',
            'description' => 'default block',
            'start' => '00:00:00',
            'end' => '6:59:59',
            'user_id' => 1
        ])->save();

        $allTasks = Task::all();
        foreach ($allTasks as $task)
        {
            $task->blocks()->sync([
                DB::table('blocks')
                    ->inRandomOrder()
                    ->first()
                    ->id
            ]);
        }
    }
}
