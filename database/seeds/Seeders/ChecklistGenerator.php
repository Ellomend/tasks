<?php

namespace Seeders;


use App\Checklist;
use App\Point;

class ChecklistGenerator
{


    /**
     * ChecklistGenerator constructor.
     */
    public function __construct()
    {
        $this->messager = new MessageHelper();
    }

    public function generateChecklists(int $count, $user_id)
    {
        $checklists = factory(Checklist::class, $count)->create([
            'user_id' => $user_id
        ]);
        foreach ($checklists as $checklist) {
            factory(Point::class, rand(3,12))->create([
                'checklist_id' => $checklist->id
            ]);
        }
        return $checklists;
    }
}