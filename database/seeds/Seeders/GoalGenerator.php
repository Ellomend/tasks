<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 24.02.2017
 * Time: 0:53
 */

namespace Seeders;


use App\Goal;
use App\Models\Helpers\StepsHelper;
use Carbon\Carbon;
use Seeders\Traits\Filterable;

class GoalGenerator
{
    use Filterable;
    /**
     * @var int
     */
    private $user_id;

    /**
     * GoalGenerator constructor.
     * @param int $user_id
     */
    public function __construct(int $user_id)
    {

        $this->messager = new MessageHelper();
        $this->user_id = $user_id;
    }

    public function generateGoal(int $count, array $params = [])
    {
        $goals = factory(Goal::class, $count)->create($params);
        $goals = Goal::all();
        foreach ($goals as $goal) {
            $recurrences = StepsHelper::getRecurrences($goal);
            $steps = StepsHelper::createSteps($recurrences, $goal);
            $now = Carbon::today();
            foreach ($steps as $step) {
                if(Carbon::parse($step->end_date) < $now && !in_array($step->status, $step->done_statuses))
                {
                    $statuses = array("active", "done");

                    var_dump('statuses');
                    var_dump($statuses);

                    $status = $statuses[rand(0,1)] ;
                    $step->status = $status;
                    $step->finished = $now->toDateString();
                    $step->save();
                }
            }
        }
        $tasks = $this->assignFilter($goals, 'App\Block', 'blocks');
        return $goals;
    }
}