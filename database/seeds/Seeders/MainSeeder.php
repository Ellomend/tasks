<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 23.02.2017
 * Time: 4:13
 */

namespace Seeders;


use App\Block;
use App\Category;
use App\Checklist;
use App\Context;
use App\Goal;
use App\Mood;
use App\Project;
use App\Routine;
use App\Stack;
use App\Task;
use App\User;
use Illuminate\Database\Eloquent\Collection;

class MainSeeder
{


    /**
     * MainSeeder constructor.
     */
    public function __construct()
    {
        $this->taskGenerator = new TaskGenerator();

    }

    public function generateContexts($count = 10, $user_id = null)
    {

        $user_id = $this->getUserId($user_id);
        $contexts = factory(Context::class, $count)->create([
            'user_id' => $user_id
        ]);

        echo "created contexts: ".count($contexts)."\n";
        foreach ($contexts as $context) {
            echo 'created context: '.$context->name . ' (id:' . $context->id . ')'."\n";
        }
        return $contexts;
    }

    public function generateBlocks($int, $user_id = null, $default = false )
    {
        $user_id = $this->getUserId($user_id);
        if ($default == false) {
            $blocks = factory(Block::class, $int)->create();
            echo 'generated blocks: ' . count($blocks) . "\n";
            foreach ($blocks as $block) {
                echo "block: " . $block->name . " (" . $block->start . " - " . $block->end . ")\n";
            }
        } else {
            $blocks = BlocksGenerator::generateDefault($user_id);
            echo "generated default blocks \n";

            foreach ($blocks as $block) {
                echo "block: " . $block->name . " (" . $block->start . " - " . $block->end . ")\n";
            }

        }
        return $blocks;
    }

    public function generateCategories($int, $user_id = null)
    {
        $user_id = $this->getUserId($user_id);
        $categories = factory(Category::class, $int)->create([
            'user_id' => $user_id
        ]);
        return $categories;
    }

    public function generateProjects($int, $user_id = null)
    {
        $user_id = $this->getUserId($user_id);
        $projects = factory(Project::class, $int)->create(['user_id'=>$user_id]);
        $this->listEntities($projects,'project');
        return $projects;
    }

    public function generateMoods($int, $user_id = null)
    {
        $user_id = $this->getUserId($user_id);
        $moods = factory(Mood::class, $int)->create(['user_id'=>$user_id]);
        $this->listEntities($moods, 'mood');
        return $moods;
    }

    public function generateTasks($int, $user_id = null)
    {
        $user_id = $this->getUserId($user_id);
        $tasks = $this->taskGenerator->setFilters($user_id)->generateTasks($int, $user_id);

        $this->listEntities($tasks, 'task');
        return $tasks;
    }

    public function generateGoals($int, $user_id = null)
    {
        $user_id = $this->getUserId($user_id);
        $goalGenerator = new GoalGenerator($user_id);
        $goals = $goalGenerator->generateGoal($int);
        $this->listEntities($goals, 'goals');
        return $goals;
    }

    public function generateStacks($int, $user_id = null)
    {
        $user_id = $this->getUserId($user_id);
        $stackGenerator = new StackGenerator();
        $stacks = $stackGenerator->generateStacksWithItems($int, $user_id);
        $this->listEntities($stacks, 'stacks');
        return $stacks;
    }

    public function generateChecklist($int, $user_id)
    {
        $user_id = $this->getUserId($user_id);
        $checklistsGenerator = new ChecklistGenerator();
        $checklists = $checklistsGenerator->generateChecklists($int, $user_id);
        $this->listEntities($checklists, 'checklists');
        return $checklists;
    }

    public function generateRoutines($int, $user_id = null)
    {
        $user_id = $this->getUserId($user_id);
        $routineGenerator = new RoutineGenerator();
        $routines = $routineGenerator->generateRoutines($int, $user_id);

        $this->listEntities($routines, 'routines');
        return $routines;
    }

    public function createDefaultUser($params = [])
    {
        $defaultUser = User::select()->where('email', '=','test@test.com')->get();
        echo 'default user count ' . count($defaultUser) . "\n";
        if (count($defaultUser) == 0)
        {
            echo "creating default user\n";
            $params['password'] = bcrypt('testtest');
            $params['email'] = 'test@test.com';
            $defaultUser = factory(User::class, 1)->create($params);
            echo 'created default user: ' . $defaultUser->first()->email . ' (id:' . $defaultUser->first()->id . ')'."\n";
        } else {
            echo 'default user already exists'."\n";
            echo $defaultUser->first()->name."\n";
        }
        $user = $defaultUser->first();

        return $user;
    }

    /**
     * @param $user_id
     * @return mixed
     * action
     */
    public function getUserId($user_id)
    {
        if ($user_id == null) {
            $user_id = User::all()->random()->id;
            return $user_id;
        }
        return $user_id;
    }
    public function printMessage(string $message) {
        echo $message."\n";
    }

    public function listEntities(Collection $collection, string $entity = '', $fields = ['name', 'id'])
    {
        $this->printMessage('==='.$entity.' created ('.count($collection).') ===');
        foreach ($collection as $item) {
            $message = '';
            foreach ($fields as $field) {
                $message.=$item->$field.' - ';
            }

            $this->printMessage($message);
        }
    }


}