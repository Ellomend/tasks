<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 23.02.2017
 * Time: 7:50
 */

namespace Seeders;


use App\Block;
use App\Category;
use App\Context;
use App\Mood;
use App\Project;
use App\Task;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Seeders\Traits\Filterable;

class TaskGenerator
{
    use Filterable;


    public $user_id;

    public $filters;

    public function __construct()
    {
        $this->filtersList = [
            Block::class,
            Context::class,
            Mood::class,
            Project::class,
            Category::class
        ];
        $this->messager = new MessageHelper();
    }

    public function setFilters($user_id)
    {
        $this->user_id = $user_id;
        $filters = [];
        foreach ($this->filtersList as $filter) {
            $filters[$filter] = $this->getFilter($filter);
        }
        $this->filters = $filters;
        return $this;
    }

    public function generateTasks($int, $user_id)
    {
        $tasks = factory(Task::class, $int)->create([
            'user_id' => $user_id
        ]);


        $tasks = $this->assignFilter($tasks, 'App\Block', 'blocks');
        $tasks = $this->assignFilter($tasks, 'App\Context', 'contexts');
        $tasks = $this->assignFilterId($tasks, 'App\Category', 'category_id');
        $tasks = $this->assignFilterId($tasks, 'App\Project', 'project_id');
        $tasks = $this->assignFilterId($tasks, 'App\Mood', 'mood_id');

        return $tasks;

    }

    /**
     * @param $user_id
     * action
     */
    public function getBlocks($user_id)
    {
        $blocks = Block::select()
            ->where('user_id', $user_id)
            ->get();
        return $blocks;
    }

    /**
     * @param $entity
     * @return Collection
     * action
     */
    private function getFilter($entity)
    {
        $filter = new $entity ;
        $filters = $filter->select()->where('user_id', $this->user_id)->get();
        return $filters;

    }

    private function assignBlocks($tasks)
    {
        $blocks = Block::select('id')->where('user_id', $this->user_id)->get();
        foreach ($tasks as $task) {
            $blocksIds = $blocks->shuffle()->take(1, count($blocks))->pluck('id')->toArray();
            $task->blocks()->sync($blocksIds);
            $task->save();
        }
        return $tasks;
    }

}