<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 24.02.2017
 * Time: 4:03
 */

namespace Seeders;


use App\Chore;
use App\Routine;
use Carbon\Carbon;
use Seeders\Traits\Filterable;

class RoutineGenerator
{

    use Filterable;
    public $user_id;

    public function generateRoutines($count, $user_id)
    {
        $this->user_id = $user_id;
        $routines = factory(Routine::class, $count)->create([
            'user_id' =>  $user_id
        ]);

        foreach ($routines as $routine) {
            $this->generateCompletedChores($routine);
        }
        $tasks = $this->assignFilter($routines, 'App\Block', 'blocks');
        return $routines;
    }

    private function generateCompletedChores($routine)
    {
        if ($routine->type == 1) {
            $subType = rand(1,50) / 2;
            $sub = 'subHours';
        } else {
            $subType = rand(1,12) ;
            $sub = 'subDays';
        }
        $completes = rand(0, 15);

        for ($i=0; $i <= $completes; $completes-- ) {
            $date = Carbon::now();
            $subdays = $subType - $completes;
            $completionDate = Carbon::parse($date->toDayDateTimeString());
            $completionDate = $completionDate->subDays($subdays);
            $routine->completeDated($completionDate);
        }


    }


}