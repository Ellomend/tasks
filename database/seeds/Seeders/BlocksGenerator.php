<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 23.02.2017
 * Time: 5:04
 */

namespace Seeders;


use App\Block;
use App\User;
use Illuminate\Database\Eloquent\Collection;

class BlocksGenerator
{

    public static function generateDefault($user_id = null)
    {
        $user = self::setRandom($user_id);
        $user_id = $user_id ? $user->id : $user_id;

        $blocks = collect();
        if (!self::isDefaultExists()) {
            $blocks->push(factory(Block::class)->create([
                'name' => 'morning',
                'description' => 'default block',
                'start' => '7:00:00',
                'end' => '12:00:00',
                'user_id' => $user_id
            ]));
            $blocks->push(factory(Block::class)->create([
                'name' => 'day',
                'description' => 'default block',
                'start' => '12:00:00',
                'end' => '18:00:00',
                'user_id' => $user_id
            ]));
            $blocks->push(factory(Block::class)->create([
                'name' => 'evening',
                'description' => 'default block',
                'start' => '18:00:00',
                'end' => '23:59:59',
                'user_id' => $user_id
            ]));
            $blocks->push(factory(Block::class)->create([
                'name' => 'night',
                'description' => 'default block',
                'start' => '00:00:00',
                'end' => '6:59:59',
                'user_id' => $user_id
            ]));
        }


        return $blocks;
    }

    /**
     * @param $user_id
     * action
     * @return User
     */
    public static function setRandom($user_id)
    {
        if ($user_id == null) {
            $user = User::all()->random();
        } else {
            $user = User::find($user_id);
        };

        return $user;
    }

    public static function isDefaultExists()
    {
        /** @var Collection $blocks */
        $blocks = Block::select()
            ->orWhere('name', 'morning')
            ->orWhere('name', 'day')
            ->orWhere('name', 'evening')
            ->orWhere('name', 'night')
            ->get();
        echo "default blocks exist:" . count($blocks) . "\n";
        if (count($blocks) > 0) {
            return true;
        }
        return false;
    }
}