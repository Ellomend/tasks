<?php

namespace Seeders;


use App\Item;
use App\Stack;

class StackGenerator
{
    public function generateStacksWithItems(int $count, int $user_id = null)
    {
        $stacks = factory(Stack::class, $count)->create([
            'user_id' => $user_id
        ]);
        foreach ($stacks as $stack) {
            $this->generateItemsForStack($stack->id);
        }
        return $stacks;
    }

    public function generateItemsForStack($stack_id)
    {
        $items = factory(Item::class, rand(5,50))->create([
            'stack_id' => $stack_id
        ]);
        return $items;
    }
}