<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 24.02.2017
 * Time: 5:09
 */

namespace Seeders\Traits;


trait Filterable
{

    protected function assignFilter($tasks, $filter, $relName)
    {
        $entities = $filter::select('id')->where('user_id', $this->user_id)->get();
        foreach ($tasks as $task) {
            $entitiesIds = $entities->shuffle()->take(rand(1, count($entities)))->pluck('id')->toArray();
            $entitiesFull = $entities->shuffle()->take(1, count($entities))->pluck('id');
            if (get_class($task->$relName()) == 'Illuminate\Database\Eloquent\Relations\BelongsToMany') {
                $task->$relName()->sync($entitiesIds);
            }
            $task->save();
        }
        return $tasks;
    }

    protected function assignFilterId($tasks, $filter, $field)
    {
        $entities = $filter::select('id')->where('user_id', $this->user_id)->get();
        foreach ($tasks as $task) {
            $task->$field = $entities->random()->id;
            $task->save();
        }
        return $tasks;
    }
}