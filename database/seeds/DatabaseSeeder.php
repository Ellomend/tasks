<?php

use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $main = new \Seeders\MainSeeder();
        //default user
        $user = $main->createDefaultUser(['email'=> 'test'])->first();

        //contexts
        $contexts = $main->generateContexts(8, $user->id);
        //blocks
        $blocks = $main->generateBlocks(3, $user->id);
        //categories
        $categories = $main->generateCategories(6, $user->id);
        //projects
        $projects = $main->generateProjects(6, $user->id);
//        //moods
        $moods = $main->generateMoods(4, $user->id);
        //tasks
        $tasks = $main->generateTasks(50, $user->id);
        //goals
        $goals = $main->generateGoals(30, $user->id);
        //stacks
        $stacks = $main->generateStacks(25, $user->id);
        //checklists
        $checklists = $main->generateChecklist(40, $user->id);
        //routines
        $routines = $main->generateRoutines(10, $user->id);



    }
}
