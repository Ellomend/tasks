<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoutinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routines', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->integer('user_id')->unsigned()->nullable();
            $table->dateTime('last_complete')->nullable();
            $table->dateTime('next')->nullable();
            $table->integer('interval_type')->unsigned()->nullable();
            $table->integer('interval_value')->unsigned()->nullable();
            //TODO block
            //TODO mood
            //TODO category
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('routines');
    }
}
