<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
	    Schema::create( 'chores', function ( Blueprint $table )
	    {
		    $table->increments( 'id' );
		    $table->string( 'name' );
		    $table->text( 'description' );
		    $table->integer( 'routine_id' )->unsigned()->nullable();
		    $table->text('status')->nullable();
		    $table->dateTime('completed_at')->nullable();
		    $table->timestamps();
	    } );
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::drop('chores');
	    //
    }
}
