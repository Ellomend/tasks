<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGoalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goals', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->integer('category_id')->nullable();
            $table->integer('priority', false, true)->nullable();
            $table->integer('points', false, true)->nullable();
            $table->string('type');
            $table->integer('duration');
            $table->date('start');
            $table->integer('count');
            $table->string('weekdays')->default('["monday","tuesday","wednesday","thursday","friday","saturday","sunday"]');
            $table->integer('user_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('goals');
    }
}
