<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name'           => $faker->name,
        'email'          => $faker->unique()->firstName.'@test.com',
        'password'       => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Task::class, function (Faker\Generator $faker) {
    $randomDate = function () {
        $random = rand(1,25);
        if ($random % 2 == 0) {
            return \Carbon\Carbon::today()->subDays($random)->format('Y-m-d');
        }
        return \Carbon\Carbon::today()->addDays($random)->format('Y-m-d');
    };

    return [
        'name'        => $faker->words(5, true),
        'description' => $faker->paragraphs(3, true),
//        'status' => $faker->randomElement(['active' ,'done', 'inbox', 'failed', 'moved', 'canceled', 'nailed']),
        'status'      => $faker->randomElement(['active', 'done']),
        'type'        => $faker->randomElement([1, 0]),
        'start'       => $randomDate,
        'end'         => $randomDate,
        'user_id'     => App\User::all()->random()->id
    ];
});

$factory->define(App\Context::class, function (Faker\Generator $faker) {
    return [
        'name'        => $faker->words(1, true),
        'description' => $faker->paragraphs(3, true),
        'color'       => '#ff0000',

        'user_id' => App\User::all()->random()->id
    ];
});

$factory->define(App\Mood::class, function (Faker\Generator $faker) {
    return [
        'name'        => $faker->words(1, true),
        'description' => $faker->paragraphs(3, true),
        'value'       => rand(1,20),

        'user_id' => App\User::all()->random()->id
    ];
});


$factory->define(App\Block::class, function (Faker\Generator $faker) {

    $startTime = \Carbon\Carbon::today()->addHours(rand(1, 9));
    $start = $startTime->format('H:i:s');
    $end = $startTime->addHours(rand(2, 9))->format('H:i:s');

    return [
        'name'        => $faker->words(1, true),
        'description' => $faker->paragraphs(3, true),
        'user_id'     => App\User::all()->random()->id,
        'start'       => $start,
        'end'         => $end

    ];
});

$factory->define(\App\Goal::class, function (Faker\Generator $faker) {
    $startRand = rand(-20, 20);
    return [
        'name'        => $faker->words(1, true),
        'description' => $faker->paragraphs(3, true),
        'category_id' => \App\Category::all()->random()->id,
        'priority'    => $faker->randomDigitNotNull,
        'points'      => $faker->randomDigitNotNull,
//        'type' => $faker->randomElement(['yearly' ,'monthly', 'weekly', 'daily', 'hourly', 'minutly', 'secondly']),
        'type'        => $faker->randomElement(['weekly', 'daily']),
        'duration'    => $faker->randomDigitNotNull,
        'count'       => $faker->randomDigitNotNull,
        'start'       => \Carbon\Carbon::now()->subDays($startRand)->toDateString(),
        'user_id'     => App\User::all()->random()->id


    ];
});

$factory->define(\App\Category::class, function (Faker\Generator $faker) {
    return [
        'name'        => $faker->words(1, true),
        'description' => $faker->paragraphs(3, true)

    ];
});

$factory->define(\App\Project::class, function (Faker\Generator $faker) {
    return [
        'name'        => $faker->words(1, true),
        'description' => $faker->paragraphs(3, true)

    ];
});

$factory->define(\App\Step::class, function (Faker\Generator $faker) {
    return [
        'name'        => $faker->words(1, true),
        'description' => $faker->paragraphs(3, true),
        'start_date'  => \Carbon\Carbon::today()->toDateString(),
        'end_date'    => \Carbon\Carbon::tomorrow()->toDateString(),
        'goal_id'     => App\Goal::all()->random()->id,
        'status'      => $faker->randomElement(['active', 'done']),
    ];
});


$factory->define(\App\Routine::class, function (Faker\Generator $faker) {
    return [
        'name'        => $faker->words(1, true),
        'description' => $faker->paragraphs(3, true),
        'user_id'     => App\User::all()->random()->id
    ];
});


$factory->define(\App\Chore::class, function (Faker\Generator $faker) {
    return [
        'name'         => $faker->words(1, true),
        'description'  => $faker->paragraphs(3, true),
        'completed_at' => \Carbon\Carbon::today()->toDateString(),
        'routine_id'   => App\Routine::all()->random()->id,
        'status'       => $faker->randomElement(['active', 'done']),
    ];
});


$factory->define(\App\Stack::class, function (Faker\Generator $faker) {
    return [
        'name'        => $faker->words(1, true),
        'description' => $faker->paragraphs(3, true),
        'user_id'     => App\User::all()->random()->id
    ];
});


$factory->define(\App\Item::class, function (Faker\Generator $faker) {
    return [
        'name'        => $faker->words(1, true),
        'description' => $faker->paragraphs(3, true),
        'stack_id'    => App\Stack::all()->random()->id,
        'status'      => $faker->randomElement(['active', 'done']),
    ];
});


$factory->define(\App\Checklist::class, function (Faker\Generator $faker) {
    return [
        'name'        => $faker->words(1, true),
        'description' => $faker->paragraphs(3, true),
        'type'        => rand(0,2),
        'user_id'     => App\User::all()->random()->id
    ];
});


$factory->define(\App\Point::class, function (Faker\Generator $faker) {
    return [
        'name'         => $faker->words(1, true),
        'description'  => $faker->paragraphs(3, true),
        'checklist_id' => App\Checklist::all()->random()->id,
        'status'       => $faker->randomElement(['active', 'complete']),
        'order'         => $faker->randomNumber(1)
    ];
});
