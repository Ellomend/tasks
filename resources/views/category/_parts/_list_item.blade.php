@component('parts.components.panel._panel')
    @slot('title')
        {{$item->name}}
    @endslot

    @component('parts.components.collapse._collapsable')
        Description: <br>
        {{$item->description}}
        <br>
        Owner: <br>
        {{$item->owner->email or 'No athor set'}}

    @endcomponent
<br>
    @component('parts.components.buttons.dropdown_button')

    <li>
        <a href="{!! route('category.edit', [$item->id]) !!}" class="btn btn-warning btn-sm"
           id="editCategory{{$item->id}}">
            <span class="glyphicon glyphicon-edit"></span>
            Edit
        </a>
    </li>
    <li>
        <a href="{!! route('category.delete', [$item->id]) !!}" id="deleteCategory{{$item->id}}"
           class="btn-sm btn btn-danger">
            <span class="glyphicon glyphicon-trash"></span>
            Delete
        </a>
    </li>
    @endcomponent

@endcomponent
