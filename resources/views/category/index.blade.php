@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">


            <a href="{{ url('/category/create') }}" id="createCategory" class="btn btn-primary btn-xs"
               title="Add New Category"><span
                        class="glyphicon glyphicon-plus" aria-hidden="true"/></a>
            <br/>
            <br/>
            @if($category != null)
                @foreach($category as $item)
                    @include('category._parts._list_item')
                @endforeach
            @else
                <h4>No categories for you.</h4>
            @endif
        </div>
    </div>
@endsection