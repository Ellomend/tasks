<div id="task_edit_modal">
    <form action="{!! route('task.update', [$task->id]) !!}" method="post" role="form">
        {{csrf_field()}}
        <input name="_method" type="hidden" value="PUT">
        <legend>Task: {{$task->name}}</legend>

        <div class="form-group">
            <label for="name">Task name</label>
            <input type="text" class="form-control" name="name" id="name" value="{{$task->name}}">
        </div>
        <div class="form-group">
            <label for="description">Task description</label>
            <textarea name="description" id="description" cols="30" rows="10"
                      class="form-control">{{$task->description}}
        </textarea>
        </div>
        <div class="form-group">
            <label for="status">Task status</label>
            <select name="status" id="status" class="form-control">
                <option value="active">Active</option>
                <option value="done">Done</option>
            </select>
        </div>
        @include('resources.entities.task.modal.fields.contexts')
        @include('resources.entities.task.modal.fields.blocks')
        @include('resources.entities.task.modal.fields.categories')
        @include('resources.entities.task.modal.fields.moods')
        @include('resources.entities.task.modal.fields.projects')

        @include('resources.entities.task.modal.fields.owner')
        <div class="form-group datetimepicker1">
            <label for="name">Start time</label>
            <input type="date" class="form-control" name="start" id="start" value="{{$task->start}}">
        </div>

        <div class="form-group datetimepicker2">
            <label for="name">End time</label>
            <input type="date" class="form-control" name="end" id="end" value="{{$task->end}}">
        </div>


        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>