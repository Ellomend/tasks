<div id="task_create_modal">
    <form action="{!! route('task.store') !!}" method="post" role="form">
        {{csrf_field()}}

        @include('tasks.pages._parts._create_form_part')

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
