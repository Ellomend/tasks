
<div class="form-group">
    <label for="mood">Mood:</label>
    <select name="mood_id" id="mood" class="form-control">
        @foreach($moods as $mood)
            <option value="{{$mood->id}}">{{$mood->name}}</option>
        @endforeach
    </select>
</div>


