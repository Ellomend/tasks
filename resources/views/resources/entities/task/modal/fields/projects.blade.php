
<div class="form-group">
    <label for="project">Project:</label>
    <select name="project_id" id="project" class="form-control">
        @foreach($projects as $project)
            <option value="{{$project->id}}">{{$project->name}}</option>
        @endforeach
    </select>
</div>


