

<label for="contexts">contexts:</label>
<select class="form-control" name="contexts[]" id="contexts" multiple="multiple">

    @foreach($contexts as $context)


        @if(isset($task) && $task->contexts->contains('id', $context->id))
            <option value="{{$context->id}}" selected>{{$context->name}}</option>
        @else
            <option value="{{$context->id}}">{{$context->name}}</option>
        @endif
    @endforeach
</select>
<!-- /#contexts -->

@push('footer_itits')

<script type="text/javascript">
    $("#contexts").select2();
</script>

@endpush

@push('header_styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
@endpush

@push('footer_scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
@endpush