@extends('layouts.master')
@section('content')
    <h1>Projects list</h1>
    <a href="{!! route('project.create') !!}" id="createProject" class="btn btn-success pull-right">Create new project</a>
    <br>
    <br>
    @if(count($projects)>0)
        @foreach($projects as $project)
            @include('project._parts._item_list')
        @endforeach
    @else
        <h4>No projects for you.</h4>
    @endif
@endsection