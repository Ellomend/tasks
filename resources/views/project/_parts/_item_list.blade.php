
@component('parts.components.panel._panel')
    @slot('title')
        {{$project->name}}

    @endslot
    @component('parts.components.collapse._collapsable')
        <p>
            Description: <br>
            {{$project->description}}
            <br>
            Owner: <br>
            {{$project->owner->email or 'No owner is set'}}
        </p>

    @endcomponent

    @component('parts.components.buttons.dropdown_button')
        <li>
            <a href="{!! route('project.edit', [$project->id]) !!}" class="btn btn-warning btn-sm"
               id="editProject{{$project->id}}">
                <span class="glyphicon glyphicon-edit"></span>
                Edit
            </a>
        </li>
        <li>
            <a href="{!! route('project.delete', [$project->id]) !!}" id="deleteProject{{$project->id}}"
               class="btn-sm btn btn-danger">
                <span class="glyphicon glyphicon-trash"></span>
                Delete
            </a>
        </li>

    @endcomponent
@endcomponent
