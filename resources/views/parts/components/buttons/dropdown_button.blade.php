<div class="btn-group">
    <button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button" aria-expanded="false">{{$button_text or 'Actions'}} <span class="caret"></span>
    </button>
    <ul role="menu" class="dropdown-menu">
        {{$list_items or ''}}
        {{$slot}}
        <li><a href="#">Action</a>
        </li>
        <li><a href="#">Another action</a>
        </li>
        <li><a href="#">Something else here</a>
        </li>
        <li class="divider"></li>
        <li><a href="#">Separated link</a>
        </li>
    </ul>
</div>