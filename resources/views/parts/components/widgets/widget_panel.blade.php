<div class="tile-stats {{$class or ''}}">

    <a href="{{$link or '#'}}">
    <div class="icon">
        <i class="fa {{$fa_icon or 'fa-caret-square-o-right'}}" style="color: darkorange"></i>
    </div>
    </a>
    <div class="count">{{$title or 'title'}}</div>

    <h3>
        {{ $subtitle or '' }}
    </h3>
    <p>
        {{ $content or '' }}
        {{ $slot }}
    </p>
</div>

