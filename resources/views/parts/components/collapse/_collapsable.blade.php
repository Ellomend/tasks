<?php
$randomId = rand(1,10000);
?>
<button class="btn btn-primary {{$button_class or ''}}" type="button" data-toggle="collapse" data-target="#collapsePart{{$randomId}}"
        aria-expanded="false" aria-controls="collapsePart{{$randomId}}">
    {{$buttonText or 'Open \ Close'}}
</button>
<div class="collapse" id="collapsePart{{$randomId}}">
  {{$slot}}
</div>