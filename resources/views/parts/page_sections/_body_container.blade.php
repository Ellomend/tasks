<body class="nav-md @yield('page_class')">
<div class="container body">
    <div class="main_container">
        <!-- Left menu -->
        @include('parts.page_sections._left_menu')
        <!-- top navigation -->
        <!-- Top navigation -->
        @include('parts.page_sections._top_navigation')
        <!-- /top navigation -->

        <!-- page content -->
        <!-- Content -->
        @include('parts.page_sections._page_content')
        <!-- /page content -->

        <!-- footer content -->
        <!-- footer content -->
        @include('parts.page_sections._page_footer')
        <!-- /footer content -->
    </div>
</div>
@include('parts.page_sections._bottom_scripts')

</body>