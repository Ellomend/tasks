<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>@yield('page_title')<span style="opacity: 0.2">--page_title</span></h3>
            </div>

            @include('parts.components._content_header_search')
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>@yield('title')<span style="opacity: 0.2">--title</span></h2>
                        @include('parts.components._subparts._panel_right_tools ')
                    </div>
                    <div class="x_content">
                        @include('parts.flash')
                        @yield('content')<span style="opacity: 0.2">--content</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
