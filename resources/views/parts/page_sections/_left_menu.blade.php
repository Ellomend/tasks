<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Gentelella Alela!</span></a>
        </div>

        <div class="clearfix"></div>

        @if (!Auth::guest())
        <!-- menu profile quick info -->
        <div class="profile clearfix">
            
            <div class="profile_info">
                <span>Welcome,</span>
                <h2>John Doe</h2>
            </div>
            <div class="clearfix"></div>
        </div>
        <!-- /menu profile quick info -->
        @endif
        <br/>

        @if (!Auth::guest())
        <!-- sidebar menu -->
         <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>Resources</h3>
                <ul class="nav side-menu">
                    <li><a><i class="fa fa-home"></i> Tasks <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{!! route('tasks.today') !!}">Today tasks</a></li>
                            <li><a href="{!! route('tasks.filter') !!}">Filter tasks</a></li>
                            <li><a href="{!! route('task.index') !!}">Tasks list</a></li>
                            <li><a href="{!! route('context.index') !!}">Context list</a></li>
                            <li><a href="{!! route('block.index') !!}">Blocks list</a></li>
                            <li><a href="{!! route('category.index') !!}">categories</a></li>
                            <li><a href="{!! route('project.index') !!}">Projects</a></li>
                            <li><a href="{!! route('mood.index') !!}">Moods list</a></li>
                            <li><a href="{!! route('task.archived.list') !!}">Archived tasks</a></li>

                        </ul>
                    </li>
                    <li><a><i class="fa fa-home"></i> Goal \ Step <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{!! route('step.index') !!}">All my steps</a></li>
                            <li><a href="{!! route('goal.index') !!}">Goals</a></li>

                        </ul>
                    </li>
                    <li><a><i class="fa fa-edit"></i> Stack \ Item <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{!! route('stack.index') !!}">Stack list</a></li>
                            <li><a href="{!! route('item.index') !!}">Item list</a></li>
                            <li><a href="{!! route('stack.review') !!}">Stacks review</a></li>

                        </ul>
                    </li>
                    <li><a><i class="fa fa-edit"></i> Checklist \ Point <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{!! route('checklist.index') !!}">Checklist list</a></li>
                            <li><a href="{!! route('checklist.blueprint.index') !!}">Checklist Blueprints</a></li>
                            <li><a href="{!! route('checklist.running') !!}">Checklist Running</a></li>
                            <li><a href="{!! route('point.index') !!}">Point list</a></li>

                        </ul>
                    </li>
                    <li><a><i class="fa fa-edit"></i> Routine \ Chore <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{!! route('routine.actual') !!}">Actual routines</a></li>
                            <li><a href="{!! route('routine.block.filter') !!}">Block filter</a></li>
                            <li><a href="{!! route('routine.index') !!}">Routines list</a></li>
                            <li><a href="{!! route('chore.index') !!}">Chores list</a></li>

                        </ul>
                    </li>
                    <li><a><i class="fa fa-edit"></i> Experimental <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{!! route('tasks.gantt') !!}">Gantt</a></li>
                            <li><a href="{!! route('tasks.widgets') !!}">Widgets</a></li>
                        </ul>
                    </li>

                    <li><a><i class="fa fa-edit"></i> Pages <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{!! route('pages.mixed') !!}">Mixed</a></li>
                            <li><a href="{!! route('pages.day') !!}">Day</a></li>
                            <li><a href="{!! route('pages.today.index') !!}">Today playground</a></li>
                            <li><a href="{!! route('pages.week') !!}">Week</a></li>
                            <li><a href="{!! route('pages.month') !!}">Month</a></li>
                            <li><a href="{!! route('pages.calendar.tasks') !!}">Calendar Tasks</a></li>
                            <li><a href="{!! route('pages.calendar.combined') !!}">Calendar Combined</a></li>

                        </ul>
                    </li>


                </ul>
            </div>


        </div>
        <!-- /sidebar menu -->
        @endif

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>