@component('parts.components.panel._panel')
@slot('title')
{{$item->name}}
@endslot
@slot('content')
@component('parts.components.collapse._collapsable')
<p>
    Description: <br>
    {{$item->description}}
    <br>
    Start: <br>
    {{$item->start_date}}
    <br>
    End: <br>
    {{$item->end_date}}
    <br>
    Finished: <br>
    {{$item->finished}}
    <br>
    Goal: <br>
    {{$item->goal->name}}
    <br>
    Status: <br>
    {{$item->status}}
</p>
@endcomponent
@component('parts.components.buttons.dropdown_button')

<li>
    <a href="{!! route('step.edit', [$item->id]) !!}" class="btn btn-warning btn-sm"
       id="editStep{{$item->id}}">
        <span class="glyphicon glyphicon-edit"></span>
        Edit
    </a>
</li>
<li>
    <a href="{!! route('step.delete', [$item->id]) !!}" id="deleteStep{{$item->id}}"
       class="btn-sm btn btn-danger">
        <span class="glyphicon glyphicon-trash"></span>
        Delete
    </a>
</li>
@endcomponent
@endslot
@endcomponent
