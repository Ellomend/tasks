@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Step {{ $step->id }}</div>
                    <div class="panel-body">

                        <a href="{{ url('step/' . $step->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Step"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['step', $step->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Step',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <a href="{!! route('goal.show', [$step->goal->id]) !!}">
                          <button type="button" class="btn btn-primary">
                              <i class="fa fa-search"></i>
                          </button>
                        </a>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $step->id }}</td>
                                    </tr>
                                    <tr><th> Name </th><td> {{ $step->name }} </td></tr><tr><th> Description </th><td> {{ $step->description }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection