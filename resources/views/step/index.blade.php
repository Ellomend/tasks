@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">


                <a href="{{ url('/step/create') }}" id="createStep" class="btn btn-primary btn-xs"
                   title="Add New Step"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></a>
                <br/>
                <br/>
                @forelse($step as $item)
                    @include('step._parts._step_list_item')
                @empty
                @endforelse

            </div>
        </div>
    </div>
@endsection