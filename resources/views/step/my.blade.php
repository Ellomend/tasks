@extends('layouts.master')

@section('title')
    My Steps
@stop

@section('content')
    @foreach($blocks as $block)
        <h3>Block: {{$block->name}}</h3>
        @foreach($block->goals as $goal)
            <p>goal: {{$goal->name}}</p>

            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>name</th>
                    <th>start</th>
                    <th>end</th>
                    <th>status</th>
                    <th>finished</th>
                    <th>actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($goal->steps as $step)
                    <tr>
                        <td>{{$step->name}}</td>
                        <td>{{$step->start}}</td>
                        <td>{{$step->end}}</td>
                        <td>{{$step->status}}</td>
                        <td>{{$step->finished}}</td>
                        <td>
                            @include('common.parts.step_complete_buttons')
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>



        @endforeach
    @endforeach
@stop