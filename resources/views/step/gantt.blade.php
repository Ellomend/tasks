@extends('layouts.master')

@section('title')
    gantt steps for goal
@stop

@section('content')
    {!! $gantt !!}
@stop


@push('header_styles')
<link href="/vendor/swatkins/gantt/css/gantt.css" rel="stylesheet" type="text/css">
@endpush