<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <label for="status">Status</label>
    <select name="status" id="status" class="form-control">
        <option value="active">active</option>
        <option value="done">done</option>
    </select>
</div>

<div class="form-group">
    <label for="goal">Goal</label>
    <select name="goal_id" id="goal" class="form-control">
    	<option value=""> -- Select One -- </option>
        @foreach($goals as $goal)
            <option value="{{$goal->id}}">{{$goal->name}}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
		<label for="start_date">start date</label>
		<input type="date" class="form-control" name="start_date" id="start_date" value="{{\Carbon\Carbon::today()->toDateString()}}">
</div>



<div class="form-group">
    <label for="end_date">end date</label>
    <input type="date" class="form-control" name="end_date" id="start_date" value="{{\Carbon\Carbon::tomorrow()->toDateString()}}">
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>