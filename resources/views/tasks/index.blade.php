@extends('layouts.master')

@section('title')
    Tasks home
@endsection
@section('content')

    <p>
        This view is loaded from module: {!! config('tasks.name') !!}
    </p>
@stop
