@extends('layouts.master')

@section('title')
    Filter tasks
@stop

@section('content')
{{--    {{dd($params)}}--}}
    <form action="{!! route('task.filter.json') !!}" method="get" role="form">
        <div class="form-group">
            <label> Active
                <input type="checkbox" value="true" name="active"
                       @if(isset($params['active']) && $params['active'] == 'true') checked @endif>
            </label><!-- if checked vlaue equals true else parameter is absent -->
        </div>
        <div class="form-group">
            <label> Today or earlier
                <input type="checkbox" value="true" name="before-tomorrow"
                       @if(isset($params['before-tomorrow']) && $params['before-tomorrow'] == 'true') checked @endif>
            </label><!-- if checked vlaue equals true else parameter is absent -->
        </div>


        {{--Contexts select--}}
        <label for="contexts">contexts:</label>
        <select class="form-control" name="contexts[]" id="contexts" multiple="multiple">

            @foreach($contexts as $context)


                @if(isset($params['contexts']) && in_array($context->id, $params['contexts']))
                    <option value="{{$context->id}}" selected>{{$context->name}}</option>
                @else
                    <option value="{{$context->id}}">{{$context->name}}</option>
                @endif
            @endforeach
        </select>
        <!-- /#contexts -->


        {{--Blocks select--}}
        <label for="blocks">blocks:</label>

        <select class="form-control" name="blocks" id="blocks">
            <option value="">none</option>
            @foreach($blocks as $block)


                @if(isset($params['blocks']) && $block->id == $params['blocks']))
                <option value="{{$block->id}}" selected>{{$block->name}}</option>
                @else
                    <option value="{{$block->id}}">{{$block->name}}</option>
                @endif
            @endforeach
        </select>
        <!-- /#blocks -->

        <button type="submit" class="btn btn-primary" style="margin-top: 20px;">Submit</button>

    </form>
    <h4>Tasks count: {{count($tasks)}}</h4>
    <h4>Time-block:
        @if(isset($selectedBlock))
            {{$selectedBlock->name}} ( {{$selectedBlock->start}} - {{$selectedBlock->end}} )
        @endif
    </h4>
    <table class="table table-bordered table-hover">
        <thead>
        <tr>
            <th>name</th>
            <th>start</th>
            <th>owner</th>
            <th>contexts</th>
        </tr>
        </thead>
        <tbody>
        @foreach($tasks as $task)
            <tr>
                <td>
                    {{$task->name}}
                </td>
                <td></td>
                <td>
                    <p>{{$task->owner->email}}</p>

                </td>
                <td>
                    <p>contexts</p>
                    @foreach($task->contexts as $context)
                        <span class="label label-info">{{$context->name}}</span>
                    @endforeach
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


@stop


@push('footer_itits')

<script type="text/javascript">
    $("#contexts").select2();
    $("#blocks").select2();
</script>

@endpush

@push('header_styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
@endpush

@push('footer_scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
@endpush