@component('parts.components.panel._panel')
    @slot('title')
        {{$block->name}}
    @endslot
    @component('parts.components.collapse._collapsable')
        <p>
            Description: <br>
            {{$block->description}}
            <br>
            Start: <br>
            {{\Carbon\Carbon::parse($block->start)->toTimeString()}}
            <br>
            End: <br>
            {{$block->end}}
            <br>
            Owner: <br>
            {{$block->owner->email}}
        </p>

    @endcomponent

    @component('parts.components.buttons.dropdown_button')
        <li>
            <a href="{!! route('block.edit', [$block->id]) !!}" class="btn btn-warning btn-sm"
               id="editBlock{{$block->id}}">
                <span class="glyphicon glyphicon-edit"></span>
                Edit
            </a>
        </li>
        <li>
            <a href="{!! route('block.delete', [$block->id]) !!}" id="deleteBlock{{$block->id}}"
               class="btn-sm btn btn-danger">
                <span class="glyphicon glyphicon-trash"></span>
                Delete
            </a>
        </li>

    @endcomponent

@endcomponent
