@extends('layouts.master')

@section('title')
    Edit block {{$block->name}}
@stop

@section('content')

    <form action="{!! route('block.update', [$block->id]) !!}" method="post">
        {{csrf_field()}}
        <input name="_method" type="hidden" value="PUT">
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" id="name" value="{{$block->name}}">
        </div>


        <div class="form-group">
            <label for="description">Description</label>
            <textarea name="description" class="form-control" id="description" cols="30"
                      rows="5">{{$block->description}}</textarea>
        </div>

        @include('tasks.block.parts.users_select')


        <div class="form-group">
            <label for="start" class="col-sm-2 control-label">Start</label>
            <div class="col-sm-10">
                <input type="time" class="form-control" id="start" value="{{$block->start}}">
            </div>
        </div>
        <div class="form-group">
            <label for="end" class="col-sm-2 control-label">End</label>
            <div class="col-sm-10">
                <input type="time" class="form-control" id="end" value="{{$block->end}}">
            </div>
        </div>
        <button type="submit" class="btn btn-success">Update block</button>


    </form>



@stop