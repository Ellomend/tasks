<div class="form-group">
    <label for="owner">User</label>
    <select name="user_id" id="owner" class="form-control">
        @foreach($users as $user)
            @if($user->id == $block->owner->id)
                <option value="{{$user->id}}" selected>{{$user->email}} - {{$user->name}}</option>
            @else
                <option value="{{$user->id}}">{{$user->email}} - {{$user->name}}</option>
            @endif
        @endforeach
    </select>
</div>