@extends('layouts.master')

@section('title')
    Blocks list
@stop

@section('content')

    <a href="{!! route('block.create') !!}" id="createBlock">
        <button type="button" class="btn btn-success pull-right mb10" style="margin-bottom: 20px;">Create block</button>
    </a>
    <a href="{!! route('block.generate.default') !!}"><button type="button" class="btn btn-warning">DEFAULT BLOCKS</button></a>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    @foreach($blocks as $block)
        @include('tasks.block._parts._list_item')
    @endforeach

@stop