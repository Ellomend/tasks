@extends('layouts.master')

@section('title')
    Time block {{$block->name}}
@stop

@section('content')
    <p>{{$block->description}}</p>
    <p>start: {{$block->start}}</p>
    <p>end: {{$block->end}}</p>
@stop