@extends('layouts.master')

@section('title')
    Create block
@stop

@section('content')

    <form action="{!! route('block.store') !!}" method="post" role="form">
        {{csrf_field()}}

        <div class="form-group">
            <label for="name"></label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Name">
        </div>

        <div class="form-group">
            <label for="description">Description</label>
            <textarea name="description" class="form-control" id="description" cols="30" rows="5"
                      placeholder="Block description"></textarea>
        </div>
        <div class="form-group">
            <label for="start" class="col-sm-2 control-label">Start</label>
            <div class="col-sm-10">
                <input type="time" name="start" class="form-control" id="start" value="{{\Carbon\Carbon::now()->toTimeString()}}">
            </div>
        </div>
        <div class="form-group">
            <label for="end" class="col-sm-2 control-label">End</label>
            <div class="col-sm-10">
                <input type="time" name="end" class="form-control" id="end" value="{{Carbon\Carbon::now()->addHour(1)->toTimeString()}}">
            </div>
        </div>



        <button type="submit" class="btn btn-success">Create</button>
    </form>


@stop