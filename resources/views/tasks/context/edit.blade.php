@extends('layouts.master')

@section('title')
    Edit context: {{$context->name}}
@endsection

@section('content')

    <form action="{!! route('context.update', [$context->id]) !!}" method="post" role="form">
        {{csrf_field()}}
        <input name="_method" type="hidden" value="PUT">
        <legend>Context: {{$context->name}}</legend>

        <div class="form-group">
            <label for="name">Context name</label>
            <input type="text" class="form-control" name="name" id="name" value="{{$context->name}}">
        </div>
        <div class="form-group">
            <label for="color">Context Color</label>
            <input type="text" class="form-control" name="color" id="color" value="{{$context->color}}">
        </div>
        <div class="form-group">
            <label for="description">Context description</label>
            <textarea name="description" id="description" cols="30" rows="10" class="form-control">{{$context->description}}</textarea>
        </div>


        <button type="submit" class="btn btn-primary">Submit</button>
    </form>





@endsection


@push('footer_scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.2/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/js/bootstrap-datetimepicker.min.js"></script>
@endpush

@push('header_styles')
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/css/bootstrap-datetimepicker.min.css">
@endpush

@push('footer_itits')

        <script type="text/javascript">
            $(function () {
                var format = {
                    format: 'YYYY-MM-DD H:mm:ss'
                };
                $('#start').datetimepicker(format);
                $('#end').datetimepicker(format);
                $('#finished').datetimepicker(format);
            });
        </script>


@endpush