@extends('layouts.master')

@section('title')
    My contexts
@endsection


@section('content')

    <a href="{!! route('context.create') !!}">
        <button type="button" class="btn btn-success pull-right" style="margin-bottom: 20px;">
            <span class="glyphicon glyphicon-plus"></span>
            Create Context
        </button>
    </a>

    <table class="table table-bordered table-hover">
        <thead>
        <tr>
            <th>name</th>
            <th>description</th>
            <th>actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($contexts as $context)
            <tr>
                <td style="color: {{$context->color}}"><strong>{{$context->name}}</strong></td>
                <td>{{$context->description}}</td>
                <td>
                    <div class="btn-group">
                        <a href="{!! route('context.show', [$context->id]) !!}"><button type="button" class="btn btn-default">Show</button></a>
                        <a href="{!! route('context.edit', [$context->id]) !!}"><button type="button" class="btn btn-warning">Edit</button></a>
                        <a href="{!! route('context.delete', [$context->id]) !!}"><button type="button" class="btn btn-danger">Delete</button></a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection