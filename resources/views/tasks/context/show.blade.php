@extends('layouts.master')

@section('title')
    {{$context->name}}
@endsection

@section('content')
    <p>{{$context->description}}</p>
@endsection