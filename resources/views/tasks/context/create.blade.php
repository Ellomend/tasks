@extends('layouts.master')

@section('title')
    Create new context
@endsection

@section('content')

    <form action="{!! route('context.store') !!}" method="post" role="form">
        {{csrf_field()}}

        <legend>New context</legend>

        <div class="form-group">
            <label for="name">Contex name</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="context name ...">
        </div>
                <div class="form-group">
            <label for="color">Contex color</label>
            <input type="text" class="form-control" name="color" id="color" value="#ff0000">
        </div>
        <div class="form-group">
            <label for="description">Context description</label>
            <textarea name="description" id="description" cols="30" rows="10" class="form-control" placeholder="context description ..."></textarea>
        </div>


        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@endsection


