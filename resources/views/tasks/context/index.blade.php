@extends('layouts.master')

@section('title')
    List of contexts
@endsection

@section('content')
    <p>List of contexts</p>

    <a href="{!! route('context.create') !!}" class="btn-success btn pull-right">
        <span class="glyphicon glyphicon-plus"></span>
        Create new context
    </a>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    @foreach($contexts as $context)
        @include('tasks.context._parts._list_item')
    @endforeach
@endsection