@component('parts.components.panel._panel')
    @slot('title')
        {{$context->name}}
    @endslot

    @component('parts.components.collapse._collapsable')
        <p>
            Description: <br>
            {{$context->description}}
        </p>
        <p>
            Color: <br>
            <span style="color: {{$context->color}}; font-weight: bolder;">{{$context->color}}</span><br>
            User: <br>
            {{$context->owner->email}}
            <br>Hidden: <br>
            {{$context->hidden}}
        </p>

    @endcomponent

    @component('parts.components.buttons.dropdown_button')
        <li>
            <a href="{!! route('context.edit', [$context->id]) !!}" class="btn btn-warning btn-sm"
               id="editContext{{$context->id}}">
                <span class="glyphicon glyphicon-edit"></span>
                Edit
            </a>
        </li>
        <li>
            <a href="{!! route('context.delete', [$context->id]) !!}" id="deleteContext{{$context->id}}"
               class="btn-sm btn btn-danger">
                <span class="glyphicon glyphicon-trash"></span>
                Delete
            </a>
        </li>

    @endcomponent

@endcomponent
