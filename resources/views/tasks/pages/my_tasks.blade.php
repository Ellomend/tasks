@extends('layouts.master')

@section('title')
    My tasks
@endsection


@section('content')

    <a href="{!! route('task.create') !!}">
        <button type="button" class="btn btn-success pull-right" style="margin-bottom: 20px;">
            <span class="glyphicon glyphicon-plus"></span>
            Create Task
        </button>
    </a>

    <table class="table table-bordered table-hover">
        <thead>
        <tr>
            <th>name</th>
            <th>owner</th>
            <th>tags</th>
            <th>date</th>
            <th>actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($tasks as $task)
            <tr>
                <td>{{$task->name}}</td>
                <td>{{$task->owner->email}}</td>
                <td>
                    @foreach($task->contexts as $context)
                        <span class="label label-info">{{$context->name}}</span>
                    @endforeach
                </td>
                <td>{{$task->date}}</td>
                <td>
                    @include('common.parts.task_complete_buttons')
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection