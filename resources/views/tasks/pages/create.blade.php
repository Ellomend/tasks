@extends('layouts.master')

@section('title')
    Create new task
@endsection

@section('content')

    <form action="{!! route('task.store') !!}" method="post" role="form">
        {{csrf_field()}}

        @include('tasks.pages._parts._create_form_part')

        {{--<div class="form-group datetimepicker3">--}}
            {{--<label for="name">Finished time</label>--}}
            {{--<input type="datetime" class="form-control" name="finished" id="finished">--}}
        {{--</div>--}}


        <button type="submit" class="btn btn-primary">Submit</button>
    </form>





@endsection


@push('footer_scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.2/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/js/bootstrap-datetimepicker.min.js"></script>
@endpush

@push('header_styles')
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/css/bootstrap-datetimepicker.min.css">
@endpush

@push('footer_itits')

        <script type="text/javascript">
            $(function () {
                var format = {
                    format: 'YYYY-MM-DD'
                };
                $('#start').datetimepicker(format);
                $('#end').datetimepicker(format);
                $('#finished').datetimepicker(format);
            });
        </script>


@endpush