@extends('layouts.master')


@section('title')
    Week
@endsection

@section('content')
    <style>
        .week-wrapper {
            width: 100%;
            display: block;
            float: left;
            min-height: 400px;
        }
        .day-wrapper {
            width: 14%;
            display: block;
            float: left;
            border: 1px solid darkslategray;
            min-height: 350px;
        }
        .event-wrapper {
            width: 100%;
            display: block;
            float: left;
            height: 30px;
            border: 1px solid darkred;
        }
    </style>
    <div class="week-wrapper">
        @foreach($week->dates as $day)

            <div class="day-wrapper">
                <h4>{{$day->date->toDateString()}}</h4>
                <h3>{{$day->date->format('l')}}</h3>
                <div class="week-tasks-wrapper">
                    @if(!empty($day->events))
                        @forelse($day->events as $event)
                            <div class="event-wrapper">
                                {{$event->name}}
                            </div>
                        @empty
                            <p>No events.</p>
                        @endforelse
                    @endif
                </div>


            </div>
        @endforeach
    </div>
@stop