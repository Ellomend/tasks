@extends('layouts.master')

@section('title')
    Mixing tasks
@endsection
{{--https://www.kunkalabs.com/mixitup/--}}
{{-- TODO slice in smaller parts --}}
{{-- TODO fix collisions with other data-toggle ellements --}}
@section('content')


    <style>
        button.btn {
            opacity: 0.5;
        }
        button.btn.mixitup-control-active {
            opacity: 1;
        }
    </style>
    <div class="container-fluid">
    	<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" >
    		<h3>Blocks</h3>
            @forelse($filters['blocks'] as $block)
                <button
                        type="button"
                        data-toggle=".block-{{$block->id}}"
                        class="btn btn-primary">
                    {{$block->name}}
                </button>
            @empty
            <p>No blocks exists</p>
            @endforelse
    	</div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            <h3>Categories</h3>
            @forelse($filters['categories'] as $category)
                <button
                        type="button"
                        data-toggle=".category-{{$category->id}}"
                        class="btn btn-warning">
                    {{$category->name}}
                </button>
            @empty
                <p>No categories exists</p>
            @endforelse
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            <h3>Contexts</h3>
            @forelse($filters['contexts'] as $context)
                <button
                        type="button"
                        data-toggle=".context-{{$context->id}}"
                        class="btn btn-info">
                    {{$context->name}}
                </button>
            @empty
                <p>No contexts exists</p>
            @endforelse
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            <h3>Moods</h3>
            @forelse($filters['moods'] as $mood)
                <button
                        type="button"
                        data-toggle=".mood-{{$mood->id}}"
                        class="btn btn-success">
                    {{$mood->name}}
                </button>
            @empty
                <p>No moods exists</p>
            @endforelse
        </div>

    </div>
    <div class="container-fluid">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h3>Projects</h3>
            @forelse($filters['projects'] as $project)
                <button
                        type="button"
                        data-toggle=".project-{{$project->id}}"
                        class="btn btn-danger">
                    {{$project->name}}
                </button>
            @empty
                <p>No projects exists</p>
            @endforelse
        </div>

    </div>

    <button type="button" data-filter="all">All</button>
    <button type="button" data-sort="order:asc">Ascending</button>
    <button type="button" data-sort="order:descending">Descending</button>
    <button type="button" data-sort="random">Random</button>
    <hr>
    <div class="container">
        @foreach($tasks as $task)
            <div class="mix 123
            @forelse($task->contexts as $context)
                    context-{{$context->id}}
            @empty
            @endforelse
            @forelse($task->blocks as $block)
                    block-{{$block->id}}
            @empty
            @endforelse


            category-a
            category-<?php  echo ((!empty($task->category)) ? $task->category->id : 'none')?>
            mood-<?php  echo ((!empty($task->mood)) ? $task->mood->id : 'none')?>
            project-<?php  echo ((!empty($task->project)) ? $task->project->id : 'none')?>



            " data-order="1">


                @component('tasks.pages._parts._widget_item', ['task' => $task])

                    @slot('title')
                        {{$task->name}}
                    @endslot

                    @slot('subtitle')
                        {{$task->description}}
                    @endslot


                Blocks:
                @forelse($task->blocks as $block)
                    <span class="label label-primary">{{$block->name}}</span>

                @empty
                @endforelse
                <br>
                Category: @if(!empty($task->category))
                    <span class="label label-warning">{{$task->category->name}}</span>
                @else
                @endif
                <br>
                Contexts:
                @forelse($task->contexts as $context)
                    <span class="label label-info">{{$context->name}}</span>
                @empty
                @endforelse
                <br>
                Mood: @if(!empty($task->mood))
                    <span class="label label-success">{{$task->mood->name}}</span>
                @else
                @endif
                <br>
                Project: @if(!empty($task->project))
                    <span class="label label-danger">{{$task->project->name}}</span>
                @else
                @endif
                @endcomponent
            </div>
            
        @endforeach
        {{--<div class="mix category-a" data-order="1">111</div>--}}
        {{--<div class="mix category-b test" data-order="2">222</div>--}}
        {{--<div class="mix category-b category-c" data-order="3">333</div>--}}
        {{--<div class="mix category-a category-d" data-order="4">4444</div>--}}
    </div>
@stop


@push('bottom_scripts')
<script src="{{asset('../js/mixitup.min.js')}}"></script>

@endpush
@push('bottom_scripts_init')
<script>
    var mixer = mixitup('.container', {
        controls: {
            toggleLogic: 'and'
        }
    });
</script>
@endpush