@extends('layouts.master')

@section('title')
    Today tasks
@stop

@section('content')
    <div class="lateTasksWrapper">

        <h2>Late tasks</h2>
        @forelse($lateTasks as $task)
            @include('tasks.pages._parts._list_item')
        @empty
            <h6>No late tasks</h6>
        @endforelse
    </div>

    <div class="dueTasksWrapper">
        <h2>Due Tasks</h2>
        @forelse($dueTasks as $task)
            @include('tasks.pages._parts._list_item')
        @empty
            <h6>No due tasks</h6>
        @endforelse
    </div>

@endsection