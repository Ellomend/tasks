@extends('layouts.master')


@section('title')
    Month
@endsection

@section('content')
    <style>
        .month-wrapper {
            /*width: 100%;*/
            display: block;
            float: left;
            /*min-height: 400px;*/
        }

        .day-wrapper {
            min-width: 30px;
            display: block;
            float: left;
            border: 1px solid lightgrey;
            /*min-height: 350px;*/
            height: 100%;
        }

        .event-wrapper {
            width: 100%;
            display: block;
            float: left;
            height: 30px;

        }
        .event-wrapper.complete {
            border: 1px solid darkgreen;
            background: darkgreen;
        }
        .event-wrapper.active {
            border: 1px solid darkmagenta;
            background: darkmagenta;
        }
        .event-wrapper.skipped {
            border: 1px solid darkgoldenrod;
            background: darkgoldenrod;
        }

    </style>
    <div class="month-wrapper">
        <table class="table table-hover">
            <tbody>
            <tr>
                @foreach($month->dates as $day)

                    <td>
                        <div class="day-wrapper" >
                            <h4>{{$day->date->toDateString()}}</h4>
                            {{--<h3>{{$day->date->format('l')}}</h3>--}}
                            @if(!empty($day->events))
                                @forelse($day->events as $event)
                                    @if($event->state  == 'complete')
                                        <div class="event-wrapper complete">
                                    @elseif($event->state == 'active')
                                        <div class="event-wrapper active">
                                    @elseif($event->state == 'skipped')
                                        <div class="event-wrapper skipped">
                                    @else
                                        <div class="event-wrapper">
                                    @endif
                                    <div class="event-wrapper">
                                        {{--{{$event->name}}--}}
                                    </div>
                                @empty
                                    <p>No events.</p>
                                @endforelse
                            @endif

                        </div>
                    </td>
                @endforeach

            </tr>
            </tbody>
        </table>


    </div>
@stop