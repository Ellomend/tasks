@extends('layouts.master')

@section('title')
    List of tasks
@endsection

@section('content')
    <p>List of tasks</p>

    <a href="{!! route('task.create') !!}" id="createTask" class="btn-success btn pull-right">
        <span class="glyphicon glyphicon-plus"></span>
        Create new task
    </a>
    <button type="button" id="task_create_modal_show" class="btn btn-success">create</button>
    <br>
    <br>
    @foreach($tasks as $task)
        @include('tasks.pages._parts._list_item')
    @endforeach
@endsection