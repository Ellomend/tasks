
{{--<div class="form-group">--}}
    {{--<label for="name">Task name</label>--}}
    {{--<input type="text" class="form-control" name="name" id="name" placeholder="task name ...">--}}
{{--</div>--}}
{{--<div class="form-group">--}}
    {{--<label for="description">Task description</label>--}}
    {{--<textarea name="description" id="description" cols="30" rows="10" class="form-control" placeholder="task description ..."></textarea>--}}
{{--</div>--}}
@include('tasks.pages.filter.parts.form.form_parts.contexts')
@include('tasks.pages.filter.parts.form.form_parts.blocks')
@include('tasks.pages.filter.parts.form.form_parts.categories')
@include('tasks.pages.filter.parts.form.form_parts.moods')
@include('tasks.pages.filter.parts.form.form_parts.projects')

<div class="form-group">
    <label for="status">Task status</label>
    <select name="status" id="status" class="form-control">
        <option value=""> -- Select one -- </option>
        <option value="active" @if ('active' == $request->status) selected @endif>Active</option>
        <option value="done" @if ('done' == $request->status) selected @endif>Done</option>
    </select>
</div>
<div class="form-group datetimepicker1">
    <label for="name">Start time</label>
    <input 
            type="date" class="form-control" 
            name="start" 
            id="start"  
            @if($request->has('start'))
                value="{{$request->start}}">
            @else
                value="{{\Carbon\Carbon::today()->subDays(20)->toDateString()}}">
            @endif


</div>

<div class="form-group datetimepicker2">
    <label for="name">End time</label>
    <input
            type="date"
            class="form-control"
            name="end"
            id="end"
            @if($request->has('end'))
                value="{{$request->end}}">
            @else
                value="{{\Carbon\Carbon::today()->addDays(20)->toDateString()}}">
            @endif
</div>