
<div class="form-group">
    <label for="project">Project:</label>
    <select name="projectId" id="project" class="form-control">
        <option value=""> -- select --</option>
        @foreach($result->projects as $project)
            <option value="{{$project->id}}" {{($project->id == $request->get('projectId')) ?  'selected': ''}}>{{$project->name}}</option>
        @endforeach
    </select>
</div>


