
<div class="form-group">
    <label for="category">Belongs to Category:</label>
    <select name="categoryId" id="category" class="form-control">
        <option value=""> -- select One -- </option>
        @foreach($result->categories as $category)
            <option value="{{$category->id}}" {{($category->id == $request->get('categoryId')) ?  'selected': ''}}>{{$category->name}}</option>
        @endforeach
    </select>
</div>


