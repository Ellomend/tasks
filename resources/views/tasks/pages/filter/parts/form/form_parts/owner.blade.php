<div class="form-group">
    <label for="owner">Owner:</label>
    <select name="user_id" id="owner" class="form-control">
        @foreach($users as $user)
            @if(isset($task->owner) && $task->owner->id == $user->id)
                <option value="{{$user->id}}" selected>{{$user->name}}</option>
            @else
                <option value="{{$user->id}}" >{{$user->name}}</option>
            @endif
        @endforeach
    </select>

</div>
