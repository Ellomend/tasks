
<div class="form-group">
    <label for="mood">Mood:</label>
    <select name="moodId" id="mood" class="form-control">
        <option value=""> -- select --</option>
        @foreach($result->moods as $mood)
            <option value="{{$mood->id}}" {{($mood->id == $request->get('moodId')) ?  'selected': ''}}>{{$mood->name}}</option>
        @endforeach
    </select>
</div>


