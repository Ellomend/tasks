

<label for="blocks">Any Task that have any of this <b>BLOCKS</b>:</label>
<select class="form-control" name="blocks[]" id="blocks" multiple="multiple">

    @foreach($result->blocks as $block)
            <option value="{{$block->id}}" {{(collect($request->get('blocks'))->contains($block->id)) ? 'selected':''}}>{{$block->name}}</option>
    @endforeach

</select>
