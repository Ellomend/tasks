@extends('layouts.master')

@section('title')
    Tasks filter
@stop

@section('content')

    <div class="panel panel-success">
        <div class="panel-heading">
            <h4 class="panel-title">Filters</h4>
            <span class="pull-right">
                                        <i class="glyphicon glyphicon-chevron-up showhide clickable panel-collapsed"> TOGGLE </i>
                                        <i class="glyphicon glyphicon-remove removepanel clickable"></i>
                                    </span>
        </div>
        <div class="panel-body"  style="display: block">
                {!! Form::open(['url' => route('tasks.filter'), 'method' => 'post']) !!}
                {{csrf_field()}}
                @include('tasks.pages.filter.parts.form._filter_form_part')
                <button type="submit" class="btn btn-primary">Submit</button>
            {!! Form::close() !!}


        </div>
    </div>
    <?php
            var_dump($result->request->all());
    ?>
    @forelse($tasks as $task)
        @include('tasks.pages._parts._list_item')
    @empty
        <p>no tasks</p>
    @endforelse

@stop