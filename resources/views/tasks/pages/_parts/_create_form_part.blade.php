<legend>New task</legend>

<div class="form-group">
    <label for="name">Task name</label>
    <input type="text" class="form-control" name="name" id="name" placeholder="task name ...">
</div>
<div class="form-group">
    <label for="description">Task description</label>
    <textarea name="description" id="description" cols="30" rows="10" class="form-control" placeholder="task description ..."></textarea>
</div>
@include('resources.entities.task.modal.fields.contexts')
@include('resources.entities.task.modal.fields.blocks')
@include('resources.entities.task.modal.fields.categories')
@include('resources.entities.task.modal.fields.moods')
@include('resources.entities.task.modal.fields.projects')

<div class="form-group">
    <label for="status">Task status</label>
    <select name="status" id="status" class="form-control">
        <option value="active">Active</option>
        <option value="done">Done</option>
    </select>
</div>

<div class="form-group datetimepicker1">
    <label for="name">Start time</label>
    <input type="date" class="form-control" name="start" id="start"  value="{{\Carbon\Carbon::today()->toDateString()}}">
</div>

<div class="form-group datetimepicker2">
    <label for="name">End time</label>
    <input type="date" class="form-control" name="end" id="end" value="{{\Carbon\Carbon::today()->addDay(1)->toDateString()}}">
</div>