@component('parts.components.panel._panel')
    @slot('title')
        {{$task->name}}

    @endslot
    @slot('content')
        @component('parts.components.collapse._collapsable')
            <p>
                Description: <br>
                {{$task->description}}
                <br>
                Status: <br>
                {{$task->status}}
                <br>
                Start: <br>
                {{$task->start}}
                <br>
                End: <br>
                {{$task->end}}
                <br>
                Finished: <br>
                {{$task->finished}}
                <br>
                Owner: <br>
                {{$task->owner->email or 'no owner'}}
                <br>
                Category: <br>
                {{$task->category->name or 'no category'}}
                <br>
                Mood: <br>
                {{$task->mood->name or 'no mood'}}
                <br>
                Project: <br>
                {{$task->project->name or 'no project'}}
                <br>
                Type: <br>
                {{$task->type}}
            </p>

        @endcomponent
        
        @component('parts.components.buttons.dropdown_button')
            <li>
                <a href="{!! route('task.edit', [$task->id]) !!}" class="btn btn-warning btn-sm"
                   id="editTask{{$task->id}}">
                    <span class="glyphicon glyphicon-edit"></span>
                    Edit
                </a>
            </li>
            <li>
                <a href="{!! route('task.show', [$task->id]) !!}" class="btn btn-success btn-sm"
                   id="showTask{{$task->id}}">
                    <span class="glyphicon glyphicon-search"></span>
                    show
                </a>
            </li>
            <li>
                <a href="{!! route('task.delete', [$task->id]) !!}" id="deleteTask{{$task->id}}"
                   class="btn-sm btn btn-danger">
                    <span class="glyphicon glyphicon-trash"></span>
                    Delete
                </a>
            </li>
            <li>
                <a href="{!! route('task.archive', [$task->id]) !!}" id="archiveTask{{$task->id}}"
                   class="btn-sm btn btn-warning">
                    <span class="glyphicon glyphicon-inbox"></span>
                    Archive
                </a>
            </li>
            
        @endcomponent
    @endslot
@endcomponent
