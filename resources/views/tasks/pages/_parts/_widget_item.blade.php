<div class="tile-stats" style="max-width: 300px; float: left; min-width: 300px;">

    <a href="{!! route('task.edit', [$task->id]) !!}"><div class="icon"><i class="fa fa-caret-square-o-right" style="color: darkorange"></i>
        </div>
    <div class="count">{{$title or 'title'}}</div>

    <h3>{{ $subtitle or '&nbsp;' }}</h3>
    <p>{{ $content or '' }}
        {{ $slot }}</p>
</div>

