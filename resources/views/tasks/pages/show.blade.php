@extends('layouts.master')

@section('title')
    {{$task->name}}
@endsection

@section('content')
    <p>{{$task->description}}</p>

    <div class="well">
        <h4>
            Status:
            @if($task->status == 'active')
                <span style="color:red; font-weight: bold;">Open</span>
            @elseif($task->status == 'done')
                <span style="color:Green; font-weight: bold;">Done</span>
            @else
                <span style="color:yellow; font-weight: bold;">Unresolved</span>
            @endif
        </h4>
        <p>status: {{$task->status}}
        </p>
        <p>
            Start time:
            {{$task->start}}
            <br>
            End time:
            {{$task->end}}
            <br>
            Finished:
            {{$task->finished}}
        </p>
        <br>
        <a href="{!! route('task.edit', [$task->id]) !!}">
            <button type="button" class="btn btn-warning">
                Edit
            </button>
        </a>
        <a href="{!! route('task.complete', [$task->id, 'done']) !!}">
            <button type="button" class="btn btn-success">
                Complete
            </button>
        </a>


        <h4>Contexts:</h4>
        @foreach($task->contexts as $context)
            <span class="label label-info">{{$context->name}}</span>
        @endforeach
    </div>
@endsection