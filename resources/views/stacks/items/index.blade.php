@extends('layouts.master')

@section('title')
    Items list
@stop

@section('content')
    <a href="{!! route('item.create') !!}" id="createItem">
        <button type="button" class="btn btn-success pull-right">Create item</button>
    </a>


    @foreach($items as $item)
               @include('stacks.items._parts._item_list_item')

    @endforeach
@stop