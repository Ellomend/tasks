@extends('layouts.master')

@section('title')
    Item edit
@stop

@section('content')
    <div class="panel panel-warning">
    	  <div class="panel-heading">
    			<h3 class="panel-title">Edit item</h3>
    	  </div>
    	  <div class="panel-body">

			  <form action="{!! route('item.update', [$item->id]) !!}"  method="post" role="form">
				  {{csrf_field()}}
				  <input name="_method" type="hidden" value="PUT">

				  <div class="form-group">
					  <label for="name">name</label>
					  <input type="text" class="form-control" name="name" id="name" value="{{$item->name}}">
				  </div>

				  <div class="form-group">
					  <label for="description">description</label>
					  <textarea name="description" id="description" cols="30" rows="10">{{$item->description}}</textarea>
				  </div>

				  <select name="stack_id" id="stack" class="form-control">
					  @foreach($stacks as $stack)
						  <option value="{{$stack->id}}">{{$stack->name}}</option>
					  @endforeach
				  </select>

				  <button type="submit" class="btn btn-primary">Submit</button>
			  </form>
    	  </div>
    </div>
@stop


@push('bottom_scripts')
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
@endpush

@push('bottom_scripts_init')
<script>
    tinymce.init({
        selector: 'textarea',
        height: 300,
        theme: 'modern',
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
        ],
        toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
        image_advtab: true,
        templates: [
            { title: 'Test template 1', content: 'Test 1' },
            { title: 'Test template 2', content: 'Test 2' }
        ],
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'
        ]
    });


</script>
@endpush