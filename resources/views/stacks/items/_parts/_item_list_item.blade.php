@component('parts.components.panel._panel')
    @slot('title')
        {{$item->name}}
    @endslot

    @component('parts.components.collapse._collapsable')
        {!!  $item->description !!}

    @endcomponent

    @component('parts.components.buttons.dropdown_button')
    <li><a href="{!! route('item.show', [$item->id]) !!}" id="showItem{{$item->id}}">
        <button type="button" class="btn btn-success">Show</button>
    </a></li>
<li><a href="{!! route('item.edit', [$item->id]) !!}" id="editItem{{$item->id}}">
        <button type="button" class="btn btn-warning">Edit</button>
    </a></li>
<li><a href="{!! route('item.delete', [$item->id]) !!}" id="deleteItem{{$item->id}}">
        <button type="button" class="btn btn-danger">Del</button>
    </a></li>

    @endcomponent
@endcomponent