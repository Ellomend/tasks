@extends('layouts.master')

@section('title')
    Item show
@stop

@section('content')
    <div class="panel panel-success">
    	  <div class="panel-heading">
    			<h3 class="panel-title">{{$item->name}}</h3>
    	  </div>
    	  <div class="panel-body">
    			<p>{{$item->description}}</p>
    	  </div>
    </div>
@stop