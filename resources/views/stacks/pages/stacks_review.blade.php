@extends('layouts.master')
@section('title')
    stacks review
@endsection

@section('page_class')
    stacks-review
@endsection
@section('content')
    <div class="stacks-review-wrapper">
        @forelse($stacks as $stack)
            <div class="stack">
                <h4>{{$stack->name}}</h4>
                <p>{{$stack->description}}</p>
                <div class="pull-right new-item-form">
                    <a href="{!! route('stack.edit', [$stack->id]) !!}">
                        <button type="button" class="btn btn-warning">
                            <i class="fa fa-pencil"></i>
                        </button>
                    </a>
                    @component('parts.components.collapse._collapsable')
                        @slot('buttonText')
                            add item
                        @endslot
                        @slot('button_class')
                            btn-success
                        @endslot

                    <form action="{!! route('item.store') !!}" method="post" role="form">
                        {{csrf_field()}}

                        <div class="form-group">
                            <input type="text" name="stack_id" value="{{$stack->id}}" class="hidden">
                            <input type="text" class="form-control" name="name" id="" placeholder="Item name">
                            <br>
                            <textarea name="description" id="description"  rows="5" placeholder="Item description"></textarea>
                        </div>



                        <button type="submit" class="btn btn-primary">Add item</button>
                    </form>

                    @endcomponent
                </div>
                <div class="items-wrapper">
                    @forelse($stack->items as $item)
                        <div class="item">
                            <div class="info">
                                <h5>
                                    {{$item->name}}
                                </h5>
                                <h6 style="color: #0a6aa1; text-transform: uppercase;"><b>{{$item->status}}</b></h6>
                                <p>{!! $item->description !!}</p>
                            </div>
                            <div class="actions">


                            </div>
                            <div class="status-form">
                                <a href="{!! route('item.delete', [$item->id]) !!}">
                                    <button type="button" class="btn btn-danger">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </a>
                                <a href="{!! route('item.edit', [$item ->id]) !!}">
                                    <button type="button" class="btn btn-warning">
                                        <i class="fa fa-pencil"></i>
                                    </button>
                                </a>
                                @component('parts.components.collapse._collapsable')
                                    @slot('button_class')
                                        btn-success
                                    @endslot
                                    @slot('buttonText')
                                        <i class="fa fa-book"></i>
                                    @endslot
                                    <form action="{!! route('item.status') !!}" method="post" role="form">
                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <input type="text" name="id" class="hidden" value="{{$item->id}}">
                                            <input type="text" class="form-control" name="status"  placeholder="item status">
                                        </div>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </form>
                                @endcomponent

                            </div>
                        </div>
                    @empty
                        <p>no items</p>
                    @endforelse
                </div>
            </div>
        @empty
            <p>no stacks</p>
        @endforelse
    </div>
@endsection