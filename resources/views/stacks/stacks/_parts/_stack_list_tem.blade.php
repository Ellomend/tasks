@component('parts.components.panel._panel')
    @slot('title')
        {{$stack->name}}
    @endslot


    @component('parts.components.collapse._collapsable')
        {{$stack->description}}

        @forelse($stack->items as $item)
            @include('stacks.items._parts._item_list_item')
        @empty
            <p>no items</p>
        @endforelse
    @endcomponent




    @component('parts.components.buttons.dropdown_button')
    <li>
        <a href="{!! route('stack.show', [$stack->id]) !!}" id="showStack{{$stack->id}}">
            <button type="button"  class="btn btn-success">Show</button>
        </a>
    </li>
<li><a href="{!! route('stack.edit', [$stack->id]) !!}" id="editStack{{$stack->id}}">
        <button type="button" class="btn btn-warning">Edit</button>
    </a></li>
<li><a href="{!! route('stack.delete', [$stack->id]) !!}" id="deleteStack{{$stack->id}}">
        <button type="button" class="btn btn-danger">Del</button>
    </a></li>
<li>
    <a href="{!! route('stack.additem', [$stack->id]) !!}"><button type="button" class="btn btn-primary">Add item to stack</button></a>
</li>
<li>
    <a href="{!! route('stack.review.single', [$stack->id]) !!}"><button type="button" class="btn btn-primary">Review stack</button></a>
</li>

    @endcomponent


@endcomponent