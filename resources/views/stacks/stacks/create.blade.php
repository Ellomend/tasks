@extends('layouts.master')

@section('title')
    Stack create
@stop

@section('content')
    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title">Create stack</h3>
        </div>
        <div class="panel-body">
            <form action="{!! route('stack.store') !!}" method="post" role="form">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="name">name</label>
                    <input type="text" class="form-control" name="name" id="name">
                </div>

                <div class="form-group">
                    <label for="description">description</label>
                    <input type="text" class="form-control" name="description" id="description">
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@stop