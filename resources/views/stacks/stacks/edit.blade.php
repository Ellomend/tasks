@extends('layouts.master')

@section('title')
    Stack edit
@stop

@section('content')
    <div class="panel panel-warning">
    	  <div class="panel-heading">
    			<h3 class="panel-title">Edit stack</h3>
    	  </div>
    	  <div class="panel-body">

			  <form action="{!! route('stack.update', [$stack->id]) !!}"  method="post" role="form">
				  {{csrf_field()}}
				  <input name="_method" type="hidden" value="PUT">

				  <div class="form-group">
					  <label for="name">name</label>
					  <input type="text" class="form-control" name="name" id="name" value="{{$stack->name}}">
				  </div>

				  <div class="form-group">
					  <label for="description">description</label>
					  <input type="text" class="form-control" name="description" id="description" value="{{$stack->description}}">
				  </div>

				  <button type="submit" class="btn btn-primary">Submit</button>
			  </form>
    	  </div>
    </div>
@stop