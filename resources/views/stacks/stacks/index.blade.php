@extends('layouts.master')

@section('title')
    Stacks list
@stop

@section('content')
    <a href="{!! route('stack.create') !!}" id="createStack">
        <button type="button"  class="btn btn-success pull-right">Create stack</button>
    </a>


        @foreach($stacks as $stack)
            @include('stacks.stacks._parts._stack_list_tem')
        @endforeach
@stop