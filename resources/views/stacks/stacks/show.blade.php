@extends('layouts.master')

@section('title')
    Stack show
@stop

@section('content')
    <div class="panel panel-success">
    	  <div class="panel-heading">
    			<h3 class="panel-title">{{$stack->name}}</h3>
    	  </div>
    	  <div class="panel-body">
    			<p>{{$stack->description}}</p>
    	  </div>
    </div>
@stop