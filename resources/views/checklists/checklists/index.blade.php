@extends('layouts.master')

@section('title')
    Checklists list
@stop

@section('content')
    <a href="{!! route('checklist.create') !!}" id="createChecklist">
        <button type="button"  class="btn btn-success pull-right">Create checklist</button>
    </a>

        @forelse($checklists as $checklist)
            @include('checklists.checklists._checklist_list_item')
        @empty
        <p>No checklists</p>
        @endforelse
@stop