@extends('layouts.master')

@section('title')
    Running checklists
@endsection

@section('page_class')
    running_checklists
@stop
@section('content')

    <div class="running-checklists-wrapper">
        @forelse($checklists as $checklist)
            <div class="checklist">
                <div class="header">
                    <h5>
                        {{$checklist->name}}
                    </h5>
                    <p>{{$checklist->description}}</p>
                </div>
                <a href="{!! route('checklist.reorder', [$checklist->id]) !!}">
                    <button type="button" class="btn btn-warning btn-xs pull-right">
                        <i class="fa fa-retweet"></i>
                    </button>
                </a>
                <a href="{!! route('checklist.delete', [$checklist->id]) !!}">
                    <button type="button" class="btn btn-xs btn-danger pull-right">
                        <i class="fa fa-trash"></i>
                    </button>
                </a>
                <div class="collapse-wrapper">
                    @forelse($checklist->ordered_points as $point)
                        <div class="checklist-point @if($point->is_complete) complete @endif">
                            <div class="pane first-pane">
                                <div class="h4">
                                    {{$point->name}}
                                </div>
                                <p>Order: {{$point->order}}</p>
                            </div>
                            <div class=" pane second-pane">
                                {{$point->description}} <br>
                                @if($point->status == 'complete')
                                    <span class="label label-success">Complete</span>
                                @else
                                    <span class="label label-warning">{{$point->status}}</span>
                                @endif

                            </div>
                            <div class=" pane second-pane pull-right">
                                @if($point->status !== 'complete')
                                    <a href="{!! route('point.status', [$point->id, 'complete']) !!}">
                                        <button type="button" class="btn btn-xs btn-success">
                                            <i class="fa fa-check"></i>

                                        </button>
                                    </a>
                                @elseif($point->status == 'complete')
                                    <a href="{!! route('point.status', [$point->id, 'active']) !!}">
                                        <button type="button" class="btn btn-xs btn-danger">
                                            <i class="fa fa-close"></i>

                                        </button>
                                    </a>
                                @endif
                                <a href="{!! route('point.moveup', [$point->id]) !!}">
                                    <button type="button" class="btn btn-xs btn-warning">
                                        <i class="fa fa-arrow-up"></i>
                                    </button>
                                </a>
                                <a href="{!! route('point.movedown', [$point->id]) !!}">
                                    <button type="button" class="btn btn-xs btn-warning">
                                        <i class="fa fa-arrow-down"></i>
                                    </button>
                                </a>

                            </div>
                        </div>
                    @empty
                        <p>No points in this checklist</p>
                    @endforelse

                    </div>


            </div>
        @empty
            <p>No running checklists</p>
        @endforelse
    </div>
@endsection