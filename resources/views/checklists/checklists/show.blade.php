@extends('layouts.master')

@section('title')
    Checklist show
@stop

@section('content')
    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title">{{$checklist->name}}</h3>
        </div>
        <div class="panel-body">
            <p>{{$checklist->description}}</p>
        </div>
        @include('checklists.checklists._checklist_dropdown_action')
    </div>
    @component('parts.components.panel._panel')
        @slot('title')
            Points

        @endslot
        @forelse($checklist->points()->orderBy('order')->get() as $point)
            @include('checklists.points._point_list_item')
        @empty
            <p>No points in checklist</p>
        @endforelse
    @endcomponent
@stop