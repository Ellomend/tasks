@component('parts.components.buttons.dropdown_button')
<li><a href="{!! route('checklist.show', [$checklist->id]) !!}" id="showChecklist{{$checklist->id}}">
        <button type="button"  class="btn btn-success">Show</button>
    </a></li>
<li><a href="{!! route('checklist.edit', [$checklist->id]) !!}" id="editChecklist{{$checklist->id}}">
        <button type="button"  class="btn btn-warning">Edit</button>
    </a></li>
<li><a href="{!! route('checklist.delete', [$checklist->id]) !!}" id="deleteChecklist{{$checklist->id}}">
        <button type="button"  class="btn btn-danger">Del</button>
    </a>
</li>
<li>
    <a href="{!! route('checklist.add.point', [$checklist->id]) !!}" id="addPoint{{$checklist->id}}">
        <button type="button"  class="btn btn-success">Add point</button>
    </a>
</li>
<li>
    <a href="{!! route('checklist.reorder', [$checklist->id]) !!}" id="reorderChecklist{{$checklist->id}}">
        <button type="button"  class="btn btn-warning">Reorder</button>
    </a>
</li>
@if($checklist->status == 'blueprint')
    <li>
        <a href="{!! route('checklist.blueprint.start', [$checklist->id]) !!}" id="startChecklist{{$checklist->id}}">
            <button type="button"  class="btn btn-success">Start</button>
        </a>
    </li>
@endif

@endcomponent