@component('parts.components.panel._panel')
    @slot('title')
        {{$checklist->name}}
    @endslot
    @slot('subtitle')
        {{$checklist->status}}
    @endslot

    @component('parts.components.collapse._collapsable')
        <p>{{$checklist->description}}</p>
    @endcomponent
    @component('parts.components.collapse._collapsable')
        @forelse($checklist->points as $point)
            @include('checklists.points._point_list_item')
        @empty
            <p>No points in checklist</p>
        @endforelse
    @endcomponent

    @include('checklists.checklists._checklist_dropdown_action')
    
@endcomponent