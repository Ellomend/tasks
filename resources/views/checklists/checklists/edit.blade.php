@extends('layouts.master')

@section('title')
    Checklist edit
@stop

@section('content')
    <div class="panel panel-warning">
    	  <div class="panel-heading">
    			<h3 class="panel-title">Edit checklist</h3>
    	  </div>
    	  <div class="panel-body">

			  <form action="{!! route('checklist.update', [$checklist->id]) !!}"  method="post" role="form">
				  {{csrf_field()}}
				  <input name="_method" type="hidden" value="PUT">

				  <div class="form-group">
					  <label for="name">name</label>
					  <input type="text" class="form-control" name="name" id="name" value="{{$checklist->name}}">
				  </div>

				  <div class="form-group">
					  <label for="description">description</label>
					  <input type="text" class="form-control" name="description" id="description" value="{{$checklist->description}}">
				  </div>

				  <button type="submit" class="btn btn-primary">Submit</button>
			  </form>
    	  </div>
    </div>
@stop