@extends('layouts.master')

@section('title')
    Point create
@stop

@section('content')
    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title">Create point</h3>
        </div>
        <div class="panel-body">
            <form action="{!! route('point.store') !!}" method="post" role="form">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="name">name</label>
                    <input type="text" class="form-control" name="name" id="name">
                </div>

                <div class="form-group">
                    <label for="description">description</label>
                    <input type="text" class="form-control" name="description" id="description">
                </div>

                <select name="checklist_id" id="checklist" class="form-control">
                    @foreach($checklists as $checklist)
                        <option value="{{$checklist->id}}">{{$checklist->name}}</option>
                    @endforeach
                </select>

                <select name="status" id="status" class="form-control">
                	<option value="active">active</option>
                	<option value="done">done</option>
                </select>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@stop