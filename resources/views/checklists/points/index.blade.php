@extends('layouts.master')

@section('title')
    Point list
@stop

@section('content')
    <a href="{!! route('point.create') !!}" id="createPoint">
        <button type="button"  class="btn btn-success pull-right">Create point</button>
    </a>
        @foreach($points as $point)
           @include('checklists.points._point_list_item')
        @endforeach
@stop