@extends('layouts.master')

@section('title')
    Point show
@stop

@section('content')
    <div class="panel panel-success">
    	  <div class="panel-heading">
    			<h3 class="panel-title">{{$point->name}}</h3>
    	  </div>
    	  <div class="panel-body">
    			<p>{{$point->description}}</p>
    	  </div>
    </div>
@stop