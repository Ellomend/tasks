@component('parts.components.panel._panel')
    @slot('title')
        {{$point->name}}
    @endslot
    @slot('subtitle')
        order: {{$point->order}}
        status: {{ $point->point_status }}

    @endslot

    @component('parts.components.collapse._collapsable')

        <p>
            {{$point->description}}
        </p>
        <p>Status: {{$point->status}}</p>
        <p>Checklist: {{$point->checklist->name}}</p>

    @endcomponent

    @component('parts.components.buttons.dropdown_button')
        <li><a href="{!! route('point.show', [$point->id]) !!}" id="showPoint{{$point->id}}">
            <button type="button"  class="btn btn-success">Show</button>
        </a></li>
        <li><a href="{!! route('point.edit', [$point->id]) !!}" id="editPoint{{$point->id}}">
            <button type="button"  class="btn btn-warning">Edit</button>
        </a></li>
        <li><a href="{!! route('point.delete', [$point->id]) !!}" id="deletePoint{{$point->id}}">
            <button type="button"  class="btn btn-danger">Del</button>
        </a></li>
        <li><a href="{!! route('point.status', [$point->id, 'complete']) !!}" id="completePoint{{$point->id}}">
            <button type="button"  class="btn btn-success">complete</button>
        </a></li>
        <li><a href="{!! route('point.status', [$point->id, 'active']) !!}" id="activePoint{{$point->id}}">
        <button type="button"  class="btn btn-warning">active</button>
    </a></li>
        <li>
            <a href="{!! route('point.moveup', [$point->id]) !!}">UP</a>
        </li>
        <li>
            <a href="{!! route('point.movedown', [$point->id]) !!}">DOWN</a>
        </li>

    @endcomponent

@endcomponent