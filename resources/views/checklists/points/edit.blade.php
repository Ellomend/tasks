@extends('layouts.master')

@section('title')
    Point edit
@stop

@section('content')
    <div class="panel panel-warning">
    	  <div class="panel-heading">
    			<h3 class="panel-title">Edit point</h3>
    	  </div>
    	  <div class="panel-body">

			  <form action="{!! route('point.update', [$point->id]) !!}"  method="post" role="form">
				  {{csrf_field()}}
				  <input name="_method" type="hidden" value="PUT">

				  <div class="form-group">
					  <label for="name">name</label>
					  <input type="text" class="form-control" name="name" id="name" value="{{$point->name}}">
				  </div>

				  <div class="form-group">
					  <label for="description">description</label>
					  <input type="text" class="form-control" name="description" id="description" value="{{$point->description}}">
				  </div>

				  <select name="checklist_id" id="checklist" class="form-control">
					  @foreach($checklists as $checklist)
						  <option value="{{$checklist->id}}">{{$checklist->name}}</option>
					  @endforeach
				  </select>

				  <button type="submit" class="btn btn-primary">Submit</button>
			  </form>
    	  </div>
    </div>
@stop