@extends('layouts.master')

@section('title')
    Create chore
@stop

@section('content')
    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title">Create chore</h3>
        </div>
        <div class="panel-body">
            <form action="{!! route('chore.store') !!}" method="post" role="form">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="name">name</label>
                    <input type="text" class="form-control" name="name" id="name">
                </div>

                <div class="form-group">
                    <label for="description">description</label>
                    <input type="text" class="form-control" name="description" id="description" >
                </div>

                <div class="form-group">
                    <label for="status">Status</label>
                    <select name="status" id="status" class="form-control">
                        <option value="active">active</option>
                        <option value="done" selected>done</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="routine">routine</label>
                    <select name="routine_id" id="routine" class="form-control">
                        @foreach($routines as $routine)
                    	    <option value="{{$routine->id}}">{{$routine->name}}</option>
                        @endforeach
                    </select>
                </div>




                <div class="form-group">
                    <label for="completed_at" class="col-sm-2 control-label">Completed_at</label>
                    <div class="col-sm-10">
                        <input type="datetime" class="form-control" name="completed_at" id="completed_at" value="{{\Carbon\Carbon::now()->toDateTimeString()}}">
                    </div>
                </div>


                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection