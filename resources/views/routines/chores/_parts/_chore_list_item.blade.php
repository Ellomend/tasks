@component('parts.components.panel._panel')
    @slot('title')
        {{$chore->name}}
        
    @endslot
    @component('parts.components.collapse._collapsable')
        <p>{{$chore->description}}</p>
        <p>{{$chore->status}}</p>
        <p>{{$chore->completed_at}}</p>
        
    @endcomponent

    @component('parts.components.buttons.dropdown_button')

        <li><a href="{!! route('chore.show', [$chore->id]) !!}" id="showChore{{$chore->id}}"><button type="button"  class="btn btn-success">show</button></a></li>
        <li><a href="{!! route('chore.edit', [$chore->id]) !!}" id="editChore{{$chore->id}}"><button type="button"  class="btn btn-warning">edit</button></a></li>
        <li><a href="{!! route('chore.delete', [$chore->id]) !!}" id="deleteChore{{$chore->id}}"><button type="button"  class="btn btn-danger">delete</button></a></li>

    @endcomponent
    
@endcomponent