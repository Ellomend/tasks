@extends('layouts.master')

@section('title')
    Chores list
@stop

@section('content')
    <a href="{!! route('chore.create') !!}" id="createChore">
        <button type="button" class="btn btn-success pull-right">create chore</button>
    </a>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title">chores list</h3>
        </div>
        <div class="panel-body">

                @foreach($chores as $chore)
            		@include('routines.chores._parts._chore_list_item')
                @endforeach
        </div>
    </div>
@endsection