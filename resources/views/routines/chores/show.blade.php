@extends('layouts.master')

@section('title')
    Chore:{{$chore->name}}
@stop

@section('content')
    <div class="panel panel-default">
    	  <div class="panel-heading">
    			<h3 class="panel-title">{{$chore->name}}</h3>
    	  </div>
    	  <div class="panel-body">
    			<p>
                    <b>Description:</b><br>
                    {{$chore->description}}
                </p>
    	  </div>
    </div>
@endsection