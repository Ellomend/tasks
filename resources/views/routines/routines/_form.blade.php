<form action="{!! route('routine.store') !!}" method="post" role="form">
    {{csrf_field()}}

    <div class="form-group">
        <label for="name">name</label>
        <input type="text" class="form-control" name="name" id="name">
    </div>

    <div class="form-group">
        <label for="description">description</label>
        <input type="text" class="form-control" name="description" id="description">
    </div>
    <div class="form-group">
        <label for="interval_type">Interval</label>
        <select name="interval_type" id="interval_type" class="form-control">
            <option value="1">Hourly</option>
            <option value="2">Daily</option>
        </select>
    </div>
    <div class="form-group">
        <label for="interval_value">Interval Value</label>
        <input name="interval_value" type="text" class="form-control">
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
</form>