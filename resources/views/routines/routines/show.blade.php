@extends('layouts.master')

@section('title')
    Routine show
@stop

@section('content')
    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title">{{$routine->name}}</h3>
        </div>
        <div class="panel-body">
            <a href="{!! route('routine.history.week', [$routine->id]) !!}">
                <button type="button" class="btn btn-default">
                    <i class="fa fa-history"></i>
                    Week
                </button>
            </a>
            <a href="{!! route('routine.history.month', [$routine->id]) !!}">
                <button type="button" class="btn btn-default">
                    <i class="fa fa-history"></i>
                    Month
                </button>
            </a>
            <p>{{$routine->description}}</p>

            <table class="table table-hover">
                <thead>
                <tr>
                    <th>id</th>
                    <th>name</th>
                    <th>status</th>
                    <th>completed at</th>

                </tr>
                </thead>
                <tbody>

                    @forelse($routine->chores as $chore)
                        <tr>
                            <td>{{$chore->id}}</td>
                            <td>{{$chore->name}}</td>
                            <td>{{$chore->status}}</td>
                            <td>{{$chore->completed_at}}</td>
                        </tr>
                    @empty
                    @endforelse

                </tbody>
            </table>
        </div>
    </div>
@stop