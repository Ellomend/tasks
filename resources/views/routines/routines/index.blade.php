@extends('layouts.master')

@section('title')
    Routines list
@stop

@section('content')
    <a href="{!! route('routine.create') !!}" id="createRoutine">
        <button type="button"  class="btn btn-success pull-right">Create routine</button>
    </a>


        @foreach($routines as $routine)

                    @include('routines._parts._routine_list_item')
        @endforeach
@stop