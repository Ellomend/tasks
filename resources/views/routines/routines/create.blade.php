@extends('layouts.master')

@section('title')
    Routine create
@stop

@section('content')
    <div class="panel panel-success">
    	  <div class="panel-heading">
    			<h3 class="panel-title">Create routine</h3>
    	  </div>
    	  <div class="panel-body">
    			@include('routines.routines._form')
    	  </div>
    </div>
@stop