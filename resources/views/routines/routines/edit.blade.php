@extends('layouts.master')

@section('title')
    Routine edit
@stop

@section('content')
    <div class="panel panel-warning">
        <div class="panel-heading">
            <h3 class="panel-title">Edit routine</h3>
        </div>
        <div class="panel-body">

            <form action="{!! route('routine.update', [$routine->id]) !!}" method="post" role="form">
                {{csrf_field()}}
                <input name="_method" type="hidden" value="PUT">

                <div class="form-group">
                    <label for="name">name</label>
                    <input type="text" class="form-control" name="name" id="name" value="{{$routine->name}}">
                </div>

                <div class="form-group">
                    <label for="description">description</label>
                    <input type="text" class="form-control" name="description" id="description"
                           value="{{$routine->description}}">
                </div>

                <div class="form-group">
                    <label for="interval_type">Interval</label>
                    <select name="interval_type" id="interval_type" class="form-control">
                        <option value="1" {{ ($routine->interval_type == 1) ? 'selected': ''}}>Hourly</option>
                        <option value="2" {{ ($routine->interval_type == 2) ? 'selected': ''}}>Daily</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="interval_value">Interval Value</label>
                    <input name="interval_value" type="number" value="{{$routine->interval_value}}" class="form-control">
                </div>


                <div class='input-group date' id='datetimepicker1'>
                    <label for="next">next</label>
                    <input type='text' name="next" class="form-control" value="{{$routine->next}}" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>

                <label for="blocks">blocks:</label>
                <select class="form-control" name="blocks[]" id="blocks" multiple="multiple">

                    @foreach($blocks as $block)


                        @if(isset($routine) && $routine->blocks->contains('id', $block->id))
                            <option value="{{$block->id}}" selected>{{$block->name}}</option>
                        @else
                            <option value="{{$block->id}}">{{$block->name}}</option>
                        @endif
                    @endforeach
                </select>
                <!-- /#blocks -->


                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@stop

@push('header_styles')
<link href="{{asset('../vendors/datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
@endpush




@push('bottom_scripts_init')

<script type="text/javascript">
    $(function () {
        $('#datetimepicker1').datetimepicker(
            {
                format: 'YYYY-MM-DD hh:mm:ss'
            }
        );
    });
</script>
@endpush