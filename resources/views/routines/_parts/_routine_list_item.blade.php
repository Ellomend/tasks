@component('parts.components.panel._panel')
    @slot('title')
        {{$routine->name}}

    @endslot
    @component('parts.components.collapse._collapsable')
        Description: <br>
        {{$routine->description}}
        <br>
        Next: {{$routine->next}}
<br>
        Is : {{\Carbon\Carbon::parse($routine->next)->diffForHumans()}}

<br>
        @forelse($routine->chores as $chore)
            @include('routines.chores._parts._chore_list_item')
        @empty
            <p>No completed chores</p>
        @endforelse
    @endcomponent

    @component('parts.components.buttons.dropdown_button')
        <li>
            <a href="{!! route('routine.complete', [$routine->id]) !!}" id="completeRoutine{{$routine->id}}"
               class="btn-sm btn btn-success">
                <span class="glyphicon glyphicon-check"></span>
                Complete
            </a>
        </li>
        <li>
            <a href="{!! route('routine.edit', [$routine->id]) !!}" class="btn btn-warning btn-sm"
               id="editRoutine{{$routine->id}}">
                <span class="glyphicon glyphicon-edit"></span>
                Edit
            </a>
        </li>
        <li>
            <a href="{!! route('routine.show', [$routine->id]) !!}" class="btn btn-primary btn-sm"
               id="editRoutine{{$routine->id}}">
                <span class="glyphicon glyphicon-search"></span>
                Show
            </a>
        </li>
        <li>
            <a href="{!! route('routine.delete', [$routine->id]) !!}" id="deleteRoutine{{$routine->id}}"
               class="btn-sm btn btn-danger">
                <span class="glyphicon glyphicon-trash"></span>
                Delete
            </a>
        </li>

    @endcomponent
@endcomponent
