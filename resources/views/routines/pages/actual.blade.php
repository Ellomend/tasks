@extends('layouts.master')

@section('title')
    Actual routines
@stop

@section('content')

    @forelse($routines as $routine)
        @include('routines._parts._routine_list_item')
    @empty
        <p>no actual routines</p>
    @endforelse
@stop