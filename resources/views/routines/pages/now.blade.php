@extends('layouts.master')
@section('content')
    @component('parts.components.panel._panel')
    @slot('title')
    Routines for now

    @endslot

    @forelse($routines as $routine)
        @include('routines._parts._routine_list_item')
    @empty
        <p>Nothing for now</p>
    @endforelse
    @endcomponent
@endsection
