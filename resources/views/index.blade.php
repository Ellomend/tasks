@extends('layouts.master')

@section('title')
    Amitai Roy basic Vue Tutorial
@endsection

@section('content')
    <div id="vue-app">
        <ul class="nav nav-pills">
            <router-link :to="{ name: 'home' }">Home</router-link>
            <router-link :to="{ name: 'profile' }">Profile</router-link>
        </ul>
        <app></app>
    </div>
@endsection


@push('footer_scripts')
<script src="{{ elixir('js/vue.js') }}"></script>
@endpush

@push('header_inits')
<script>
    window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
    ]); ?>;
</script>
@endpush