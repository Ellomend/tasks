<!DOCTYPE html>
<html lang="en">
@include('parts.page_sections._head_tag')
<body class="nav-md @yield('page_class')">
<div class="container body">
    <div class="main_container">
        <!-- Content -->
        @include('parts.page_sections._page_content')
        <!-- footer content -->
        @include('parts.page_sections._page_footer')
        <!-- /footer content -->
    </div>
</div>
@include('parts.page_sections._bottom_scripts')

</body>
</html>
