<div class="panel panel-warning">
    <div class="panel-heading">
        <h3 class="panel-title">Actions</h3>
    </div>
    <div class="panel-body">
        <a href="{!! route('task.create') !!}">
            <button type="button" class="btn  btn-success">
                    Create taks
            </button>
        </a>
        <a href="{!! route('block.create') !!}"><button type="button" class="btn btn-success">Create block</button></a>
        <a href="{!! route('context.create') !!}"><button type="button" class="btn btn-success">Create context</button></a>
        <a href="{!! route('category.create') !!}"><button type="button" class="btn btn-success">Create category</button></a>
        <a href="{!! route('goal.create') !!}"><button type="button" class="btn btn-success">Create goal</button></a>
    </div>
</div>