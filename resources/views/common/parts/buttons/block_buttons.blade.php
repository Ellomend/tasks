<div class="button-group">
    <a href="{!! route('block.edit', [$block->id]) !!}" id="editBlock{{$block->id}}"><button type="button" class="btn btn-xs btn-warning">
            <span class="glyphicon glyphicon-pencil"></span>
        </button></a>
    <a href="{!! route('block.delete', [$block->id]) !!}" id="deleteBlock{{$block->id}}"><button type="button" class="btn btn-xs btn-danger">
            <span class="glyphicon glyphicon-trash"></span>
        </button></a>
</div>