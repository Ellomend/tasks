<a href="{!! route('routine.show', [$routine->id]) !!}" target="_blank">
    <button type="button" class="btn btn-xs btn-default">
        <i class="fa fa-search"></i>
    </button>
</a>
<a href="{!! route('routine.complete', [$routine->id]) !!}" >
    <button type="button" class="btn btn-xs btn-default">
        <i class="fa fa-check"></i>
    </button>
</a>

<br>
