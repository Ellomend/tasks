({{$step->carbon_start->toDateString() }} - {{$step->carbon_end->toDateString()}})
({{$step->carbon_start->diffForHumans() }} - {{$step->carbon_end->diffForHumans()}})
{{$step->status}}
<a href="{!! route('step.show', [$step->id]) !!}" target="_blank">
    <button type="button" class="btn btn-xs btn-default">
        <i class="fa fa-search"></i>
    </button>
</a>

<a href="{!! route('step.complete', [$step->id, \App\Models\Status\StatusEnum::STATUS_COMPLETE]) !!}"
    title="complete">
    <button  type="button" class="btn btn-xs btn-default">
        <i  class="fa fa-check"></i>
    </button>
</a>
<a href="{!! route('steps.skip', [$step->id]) !!}">
    <button type="button" class="btn btn-xs btn-default">
        <i class="fa fa-arrow-left"></i>
    </button>
</a>
<a href="{!! route('step.complete', [$step->id, \App\Models\Status\StatusEnum::STATUS_FAILED]) !!}"
    title="failed">
    <button type="button" class="btn btn-xs btn-default">
        <i class="fa fa-close"></i>
    </button>
</a>
<a href="{!! route('step.complete', [$step->id, \App\Models\Status\StatusEnum::STATUS_CANCELLED]) !!}"
    title="cancelled">
    <button type="button" class="btn btn-xs btn-default">
        <i class="fa fa-ban"></i>
    </button>
</a>

<a href="{!! route('step.complete', [$step->id, \App\Models\Status\StatusEnum::STATUS_ACTIVE]) !!}"
    title="active">
    <button type="button" class="btn btn-xs btn-default">
        <i class="fa fa-level-up"></i>
    </button>
</a>
<br>