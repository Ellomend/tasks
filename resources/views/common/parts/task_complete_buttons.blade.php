<br>
<div class="btn-group">
        <a id="updateStatusDone{{$task->id}}" href="{!! route('task.complete', [$task->id, 'done']) !!}"><button type="button" class="btn btn-xs btn-info">D</button></a>
        <a id="updateStatusNailed{{$task->id}}" href="{!! route('task.complete', [$task->id, 'nailed']) !!}"><button type="button" class="btn btn-xs btn-success">N</button></a>
        <a id="updateStatusFailed{{$task->id}}" href="{!! route('task.complete', [$task->id, 'failed']) !!}"><button type="button" class="btn btn-xs btn-danger">F</button></a>
        <a id="updateStatusCanceled{{$task->id}}" href="{!! route('task.complete', [$task->id, 'canceled']) !!}"><button type="button" class="btn btn-xs btn-warning">C</button></a>
</div>