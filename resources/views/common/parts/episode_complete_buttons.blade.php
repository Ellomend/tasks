<br>
<div class="btn-group">
        <a href="{!! route('step.complete', [$step->id, 'done']) !!}"><button type="button" class="btn btn-xs btn-info">D</button></a>
        <a href="{!! route('step.complete', [$step->id, 'nailed']) !!}"><button type="button" class="btn btn-xs btn-success">N</button></a>
        <a href="{!! route('step.complete', [$step->id, 'failed']) !!}"><button type="button" class="btn btn-xs btn-danger">F</button></a>
        <a href="{!! route('step.complete', [$step->id, 'skipped']) !!}"><button type="button" class="btn btn-xs btn-warning">S</button></a>
        <a href="{!! route('step.edit', [$step->id]) !!}"><button type="button" class="btn btn-xs btn-warning">
                <span class="glyphicon glyphicon-pencil"></span>
            </button></a>
        <a href="{!! route('step.delete', [$step->id]) !!}"><button type="button" class="btn btn-xs btn-danger">
                <span class="glyphicon glyphicon-trash"></span>
            </button></a>

</div>