<div class="panel panel-success">
    <div class="panel-heading">
        <h3 class="panel-title">{{$block->name}}&nbsp;<span class="small">{{$block->description}}</span></h3>
    </div>
    <div class="panel-body">


        <div class="">


            <ul class="">


                @foreach($block->goals as $goal)
                    @foreach($goal->steps as $step)
                        @if(\Carbon\Carbon::parse($step->start_date) < \Carbon\Carbon::now() && \Carbon\Carbon::parse($step->end_date) > \Carbon\Carbon::now())

                            <li>

                                <span >{{$step->name}}</span>&nbsp;/&nbsp;
                                <span class="small">({{\Carbon\Carbon::parse($step->start_date)->toDateString()}}) &nbsp;</span>
                                <span class="small">({{\Carbon\Carbon::parse($step->end_date)->toDateString()}}) &nbsp;</span>/
                                <span class="small">{{$step->status}}</span>

                            </li>
                        @endif
                    @endforeach
                @endforeach

            </ul>
        </div>

    </div>
</div>