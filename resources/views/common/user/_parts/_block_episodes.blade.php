<div class="panel panel-success">
    <div class="panel-heading">
        <h3 class="panel-title">{{$block->name}}&nbsp;<span class="small">{{$block->description}}</span></h3>
    </div>
    <div class="panel-body">


        <div class="">


            <table class="table table-hover table-bordered">
                <thead>
                <tr>
                    <th>name</th>
                    <th>start</th>
                    <th>end</th>
                    <th>status</th>
                    <th>actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($block->goals as $goal)
                    @foreach($goal->steps as $step)
                        @if(\Carbon\Carbon::parse($step->start_date) < \Carbon\Carbon::now() && \Carbon\Carbon::parse($step->end_date) > \Carbon\Carbon::now())

                            <tr>

                                <td>{{$step->name}}</td>
                                <td>{{\Carbon\Carbon::parse($step->start_date)->toDateString()}}</td>
                                <td>{{\Carbon\Carbon::parse($step->end_date)->toDateString()}}</td>
                                <td>{{$step->status}}</td>
                                <td>
                                    @include('common.parts.step_complete_buttons')
                                </td>
                            </tr>
                        @endif
                    @endforeach
                @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>