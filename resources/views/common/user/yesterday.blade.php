@extends('layouts.master')

@section('title')
    Yesterday
@endsection

@section('content')
    @include('common.parts.actions')


    {{-- COMPLETED TASKS --}}

    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title">Task completed yesterday</h3>
        </div>
        <div class="panel-body">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>name</th>
                    <th>start</th>
                    <th>end</th>
                    <th>status</th>
                    <th>actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($completed as $task)
                <tr>
                    <td>{{$task->name}}</td>
                    <td>{{$task->start}}</td>
                    <td>{{$task->end}}</td>
                    <td>{{$task->finished}}</td>
                    <td>
                        @include('common.parts.task_complete_buttons')

                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>


    {{-- INCOMPLETED TASKS--}}


    <div class="panel panel-warning">
        <div class="panel-heading">
            <h3 class="panel-title">Task NOT completed yesterday</h3>
        </div>
        <div class="panel-body">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>name</th>
                    <th>start</th>
                    <th>end</th>
                    <th>status</th>
                    <th>actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($incompleted as $task)
                <tr>
                    <td>{{$task->name}}</td>
                    <td>{{$task->start}}</td>
                    <td>{{$task->end}}</td>
                    <td>{{$task->finished}}</td>
                    <td>
                        @include('common.parts.task_complete_buttons')

                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>

    {{--COMPLETED CHORES--}}

    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title">Steps completed yesterday</h3>
        </div>
        <div class="panel-body">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>name</th>
                    <th>status</th>
                    <th>actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($steps_completed as $step)
                <tr>
                    <td>{{$step->name}}</td>
                    <td>{{$step->status}}</td>
                    <td>
                        @include('common.parts.step_complete_buttons')

                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>


    {{-- SKIPPED CHORES --}}


        <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title">Steps skipped yesterday</h3>
        </div>
        <div class="panel-body">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>name</th>
                    <th>status</th>
                    <th>actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($steps_incompleted as $step)
                <tr>
                    <td>{{$step->name}}</td>
                    <td>{{$step->status}}</td>
                    <td>
                        @include('common.parts.step_complete_buttons')

                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>

@stop