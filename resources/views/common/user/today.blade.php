@extends('layouts.master')

@section('title')
    Today
@stop

@section('content')

    @include('common.parts.actions')
    <div class="container-fluid">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Tasks</h3>
                </div>
                <div class="panel-body">
                    <ul>
                            @foreach($tasks as $task)
                                <li>{{$task->name}}</li>
                            @endforeach
                    </ul>
                </div>
            </div>
            @foreach($blocks as $block)
                @include('common.user._parts._block_steps_lis')
            @endforeach

        </div>
    </div>


@stop

