@extends('layouts.master')

@section('title')
    Now
@stop

@section('content')

    @include('common.parts.actions')
    <div class="container-fluid">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            @foreach($blocks as $block)
                <div class="panel panel-success block-{{$block->name}}">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{$block->name}}</h3>
                    </div>
                    <div class="panel-body">
                    <p>


                        {{$block->description}}

                        </p>
                            <p class="small text-muted">start {{\Carbon\Carbon::parse($block->start)->toTimeString()}} / end {{\Carbon\Carbon::parse($block->end)->toTimeString()}}</p>

                        <div class="">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>status</th>
                                    <th>start</th>
                                    <th>end</th>
                                    <th>actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($block->tasks as $task)
                                    @if(\Carbon\Carbon::parse($task->start) <= \Carbon\Carbon::today() && \Carbon\Carbon::parse($task->end) >= \Carbon\Carbon::today() && $task->owner->id == \Illuminate\Support\Facades\Auth::user()->id)

                                        <tr>
                                            <td>{{$task->name}}</td>
                                            <td>{{$task->status}}</td>
                                            <td>{{$task->start}}</td>
                                            <td>{{$task->end}}</td>
                                            <td>
                                                @include('common.parts.task_complete_buttons')
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                        <div class="">


                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th>name</th>
                                    <th>start</th>
                                    <th>end</th>
                                    <th>status</th>
                                    <th>actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($block->goals as $goal)
                                    @foreach($goal->steps as $step)
                                        @if(\Carbon\Carbon::parse($step->start_date) < \Carbon\Carbon::now() && \Carbon\Carbon::parse($step->end_date) > \Carbon\Carbon::now())

                                            <tr>

                                                <td>{{$step->name}}</td>
                                                <td>{{\Carbon\Carbon::parse($step->start_date)->toDateString()}}</td>
                                                <td>{{\Carbon\Carbon::parse($step->end_date)->toDateString()}}</td>
                                                <td>{{$step->status}}</td>
                                                <td>
                                                    @include('common.parts.step_complete_buttons')
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            @endforeach
        </div>
    </div>


@stop

