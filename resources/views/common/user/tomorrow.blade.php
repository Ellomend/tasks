@extends('layouts.master')

@section('title')
    Tomorrow
@stop

@section('content')

    @include('common.parts.actions')
    <div class="container-fluid">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <p>Time blocks</p>
            @foreach($blocks as $block)
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{$block->name}}</h3>
                    </div>
                    <div class="panel-body">
                        <p class="bg-success"><span class="text-success"><strong>{{$block->description}}</strong></span>
                        </p>
                        <div class="">
                            <p class="bg-success">Tasks</p>
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>status</th>
                                    <th>start</th>
                                    <th>end</th>
                                    <th>actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($block->tasks as $task)

                                    @if(\Carbon\Carbon::parse($task->start) <= $day && \Carbon\Carbon::parse($task->end) >= $day && $task->owner->id == \Illuminate\Support\Facades\Auth::user()->id)

                                        <tr>
                                            <td>
                                                {{$task->name}}
                                                <br>
                                                <span class="small text-muted">{{$task->owner->name}}
                                                <br>
                                                blocks:
                                                @foreach($task->blocks as $blockt)
                                                    {{$blockt->name}}
                                                @endforeach

                                                    </span>
                                            </td>
                                            <td>{{$task->status}}</td>
                                            <td>{{$task->start}}</td>
                                            <td>{{$task->end}}</td>
                                            <td>
                                                @include('common.parts.task_complete_buttons')
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                        <div class="">
                            <p class="bg-success">
                                Steps
                            </p>

                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th>name</th>
                                    <th>start</th>
                                    <th>end</th>
                                    <th>status</th>
                                    <th>actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($block->goals as $goal)



                                    @foreach($goal->steps as $step)
                                        @if(\Carbon\Carbon::parse($step->start_date) <= $day && \Carbon\Carbon::parse($step->end_date) >= $day)

                                            <tr>

                                                <td>{{$step->name}}</td>
                                                <td>{{\Carbon\Carbon::parse($step->start_date)->toDateString()}} </td>
                                                <td>{{\Carbon\Carbon::parse($step->end_date)->toDateString()}} </td>
                                                <td>{{$step->status}}</td>
                                                <td>
                                                    @include('common.parts.step_complete_buttons')
                                                </td>
                                            </tr>
                                        @endif

                                    @endforeach



                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>


            @endforeach

        </div>
    </div>


@stop

