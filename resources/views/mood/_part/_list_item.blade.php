@component('parts.components.panel._panel')
    @slot('title')
        {{$item->name}}

    @endslot
    @component('parts.components.collapse._collapsable')


    Desription: <br>
    {{$item->description}}
    <br>
    Value: <br>
    {{$item->value}}
    <br>
    Owner: <br>
    {{$item->owner->email or 'No owner is set'}}

    @endcomponent
    @component('parts.components.buttons.dropdown_button')
        <li>
            <a href="{!! route('mood.edit', [$item->id]) !!}" class="btn btn-warning btn-sm"
               id="editMood{{$item->id}}">
                <span class="glyphicon glyphicon-edit"></span>
                Edit
            </a>
        </li>
        <li>
            <a href="{!! route('mood.delete', [$item->id]) !!}" id="deleteMood{{$item->id}}"
               class="btn-sm btn btn-danger">
                <span class="glyphicon glyphicon-trash"></span>
                Delete
            </a>
        </li>

    @endcomponent
@endcomponent
