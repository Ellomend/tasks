@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <a href="{{ url('/mood/create') }}" id="createMood" class="btn btn-primary" title="Add New Mood"><span
                            class="glyphicon glyphicon-plus" aria-hidden="true"/>
                Create Mood
                </a>
                <br/>
                <br/>
                @if(count($mood) > 0)
                    @foreach($mood as $item)
                        @include('mood._part._list_item')
                    @endforeach
                @else
                    <h4>No moods for you.</h4>
                @endif

            </div>
        </div>
    </div>
@endsection