@extends('layouts.master')

@section('page_class')
    calendar_tasks
@endsection

@section('title')
    Calendar tasks
@endsection

@section('content')
    Calendar tasks
    <div id="calendar_tasks">

    </div>


    <div id="dialog" style="display: none">
        <form action="{!! route('pages.calendar.tasks.add') !!}" method="post" role="form">
            {{csrf_field()}}
            <legend>Form Title</legend>

            @include('tasks.pages._parts._create_form_calendar')

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
    <div id="showTask" style="display: none">
        <a href="#" id="showTaskLink" target="_blank">Show task</a>
        <a href="#" id="editTaskLink" target="_blank">edit task</a>
    </div>
@endsection

@push('bottom_scripts')
<script src="http://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.2.0/fullcalendar.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.2.0/locale/ru.js"></script>


@endpush

@push('header_styles')
    <link rel="stylesheet" href="{{asset('css/pages.css')}}">

@endpush

@push('bottom_scripts_init')
<script>
//    import * as $ from "../../../../bower_components/jquery/dist/jquery";

    $(document).ready(function() {
        $('#calendar_tasks').fullCalendar({
            dayClick: function (date, event, view) {
                $('#dialog').dialog('open');
                var clickDate = date.format();
                $('#start_date').val(clickDate);
                $('#end_date').val(clickDate);
            },
            eventClick: function(calEvent, jsEvent, view) {
              console.log(calEvent);
              $('#showTask').dialog('open');
              $('#showTaskLink').attr('href','/tasks/task/'+calEvent.id);
              $('#editTaskLink').attr('href','/tasks/task/'+calEvent.id+'/edit');
            },
            eventSources: [
                {
                    //complete tasks
                    editable: true,
                    url: '{!! route('pages.calendar.tasks.completed') !!}',
                    backgroundColor: '#0e435f',
                    borderColor: '#900',
//                    textColor: '#900'
                },
                {
                    //incomplete tasks
                    editable: true,
                    url: '{!! route('pages.calendar.tasks.incomplete') !!}',
                    backgroundColor: '#11689e',
                    borderColor: '#007200',
//                    textColor: '#007200'
                }
            ],
            eventResize: function (event2, jsEvent, ui, view) {
                if (!confirm("Are you sure about this change?")) {

                    revertFunc();
                }
                console.log('wtf' , event2, jsEvent);
                $.ajaxSetup({
                    headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' }
                });
                $.ajax({
                    url: "{!! route('pages.calendar.tasks.move') !!}",
                    type: 'POST',
                    data: {

                        data: JSON.stringify(event2)
                    },
                    success: (function(){
                        console.log('success');
                    }),
                    error: (function () {
                        console.log('error');
                    })
                });
            },

            eventDrop: function(event, delta, revertFunc) {
                if (!confirm("Are you sure about this change?")) {

                    revertFunc();
                }
                $.ajaxSetup({
                    headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' }
                });
                $.ajax({
                    url: "{!! route('pages.calendar.tasks.move') !!}",
                    type: 'POST',
                    data: {

                        data: JSON.stringify(event)
                    },
                    success: (function(){
                        console.log('success');
                    }),
                    error: (function () {
                        console.log('error');
                    })
                });
            }
        });

    });
    $('#dialog').dialog({
        autoOpen: false,
        height: 300,
    });
    $('#showTask').dialog({
        autoOpen: false,
        height: 300,
    });

</script>

@endpush