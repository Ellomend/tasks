@extends('layouts.master')

@section('page_class')
    calendar_combined
@endsection

@section('title')
    Calendar combined
@endsection

@section('content')
    Calendar combined
    <div id="calendar_combined">

    </div>
    <div id="showEntity" style="display: none">
        <a href="#" id="editEntityLink" target="_blank">edit task</a>
    </div>



@endsection

@push('bottom_scripts')
<script src="http://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.2.0/fullcalendar.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.2.0/locale/ru.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

@endpush

@push('header_styles')
    <link rel="stylesheet" href="{{asset('css/pages.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css">
@endpush

@push('bottom_scripts_init')
<script>
//    import * as $ from "../../../../bower_components/jquery/dist/jquery";

    $(document).ready(function() {
        $('#calendar_combined').fullCalendar({


            eventSources: [
                {
                    //complete tasks
                    editable: true,
                    url: '{!! route('pages.calendar.tasks.completed') !!}',
                    backgroundColor: '#0e435f',
                    borderColor: '#900',
//                    textColor: '#900'
                },
                {
                    //incomplete tasks
                    editable: true,
                    url: '{!! route('pages.calendar.tasks.incomplete') !!}',
                    backgroundColor: '#11689e',
                    borderColor: '#007200',
//                    textColor: '#007200'
                },

                {
                    //incomplete steps
                    allDay: true,
                    editable: true,
                    url: '{!! route('pages.calendar.steps.completed') !!}',
                    backgroundColor: '#32602c',
//                    textColor: '#900',
                    borderColor: '#900',

                },
                {
                    //completed steps
                    allDay: true,

                    editable: true,
                    url: '{!! route('pages.calendar.steps.incomplete') !!}',
                    backgroundColor: '#68b454',
                    borderColor: '#900',
//                    textColor: '#007200'
                }
            ],

            eventDataTransform: function( json ) {
                var date = new Date(json.end);
                date.setDate(date.getDate() + 1);
                json.end = date.format('Y-m-d');


                return json;
            },
            eventClick: function(calEvent, jsEvent, view) {
              console.log(calEvent);
              var entityLink = null;
              if (calEvent.goal_id !== undefined) {
                $('#editEntityLink').attr('href','/step/'+calEvent.id+'/edit');
              } else {
                $('#editEntityLink').attr('href','/tasks/task/'+calEvent.id+'/edit');
              }
              $('#showEntity').dialog('open');
            },
            eventResize: function (event2, jsEvent, ui, view) {
                if (!confirm("Are you sure about this change?")) {

                    revertFunc();
                }
                $.ajaxSetup({
                    headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' }
                });
                $.ajax({
                    url: "{!! route('pages.calendar.tasks.move') !!}",
                    type: 'POST',
                    data: {

                        data: JSON.stringify(event2)
                    },
                    success: (function(){
                        console.log('success');
                    }),
                    error: (function () {
                        console.log('error');
                    })
                });
            },

            eventDrop: function(event, delta, revertFunc) {
                if (!confirm("Are you sure about this change?")) {

                    revertFunc();
                }
                $.ajaxSetup({
                    headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' }
                });
                console.log(event);
                $.ajax({
                    url: "{!! route('pages.calendar.tasks.move') !!}",
                    type: 'POST',
                    data: {

                        data: JSON.stringify(event)
                    },
                    success: (function(){
                        console.log('success');
                    }),
                    error: (function () {
                        console.log('error');
                    })
                });
            }
        });

    });
    $('#dialog').dialog({
        autoOpen: false,
        height: 300,
    });
    $('#showEntity').dialog({
        autoOpen: false,
        height: 300,
    });

</script>

@endpush