@extends('layouts.master')

@section('title')
    {{\Carbon\Carbon::now()->toFormattedDateString()}} - {{\Carbon\Carbon::now()->format('l')}}
@stop

@section('page_class') today_playground @endsection

@section('content')

    <div class="today">
        <div class="blockless-tasks">
            <h4>Today tasks</h4>
            @component('parts.components.collapse._collapsable')
                @slot('buttonText')
                    Today tasks
                @endslot

                @forelse($tasks as $task)

                    <a href="{!! route('task.edit', [$task->id]) !!}">
                        <h3>{{$task->name}}</h3>
                        <p>{{$task->description}}</p>
                        <p>State: {{$task->state()}}</p>
                        <p>Start: {{$task->start}} - End: {{$task->end}}</p>
                    </a>
                <hr color="gray">
                @empty
                    <p>No tasks for today</p>
                @endforelse

            @endcomponent
        </div>
        <div class="today-blocks">
            @forelse($blocks as $block)
                <div class="block-wrapper @if($block->is_current) current @endif">
                    <div class="block-head">
                        <div class="block-title">{{$block->name}}</div>
                        @if($block->is_current)
                            <div class="if-current">
                                &nbsp;&nbsp;<span class="label label-success">Actual block</span>
                            </div>
                        @endif
                    </div>
                    <div class="block-body">
                        <div class="block-routines-wrapper">
                            <h4>Routines</h4>
                            @forelse($block->routines as $routine)
                                @include('pages.day._parts._today_block_routine_widget')
                            @empty
                                <p>no routines for block</p>
                            @endforelse
                        </div>
                        <div class="block-goals-wrapper">
                            <h4>
                                Goals
                            </h4>

                            @forelse($block->goals as $goal)
                                @if($goal->is_active)
                                    @include('pages.day._parts._today_block_goal_widget')
                                @endif
                            @empty
                                <p>No goals</p>
                            @endforelse
                        </div>
                        <div class="block-tasks-wrapper">
                            <h4>
                                Tasks
                            </h4>
                            @forelse($block->tasks as $task)
                                @if($task->state() != 'future')
                                    @if($task->status == 'done' && \Carbon\Carbon::parse($task->end)->lt(\Carbon\Carbon::today()))

                                    @else
                                        @include('pages.day._parts._today_block_task_widget')
                                    @endif
                                @endif
                            @empty
                                <p>No tasks</p>
                            @endforelse
                        </div>
                    </div>
                </div>
            @empty
                <p>no blocks</p>
            @endforelse
        </div>
    </div>
@stop