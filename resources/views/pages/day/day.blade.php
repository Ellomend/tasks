@extends('layouts.master')

@section('title')
    Day layout
@stop

@section('page_class')
    today_page
@endsection

@section('content')

    {!! $master->getTodayOverview()->getTasks()->render() !!}
    {!! $master->getTodayOverview()->dayGoals()->render() !!}
    <br>
    {!! $master->getTodayOverview()->getCurrentBlocks()->render() !!}
    <br>
    {!! $master->getTodayOverview()->getAllBlocks()->render() !!}
    <br>
    {!! $master->getTodayOverview()->getRoutines()->render() !!}
    <br>


@stop