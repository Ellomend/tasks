@component('parts.components.widgets.widget_panel')
@slot('title')
{{$task->name}}
@endslot
@slot('subtitle')
    {{$task->description}}
@endslot
@slot('class')
    @if($task->state() != 'active' && $task->state() !== 'late')
        done
    @endif

@endslot

@slot('fa_icon')
    fa-retweet
@endslot

<span class="label label-info">{{$task->state()}}</span>
<br>
@if($task->state() == 'active' || $task->state() == 'late')
    <a href="{!! route('task.complete', [$task->id, 'done']) !!}">
        <button type="button" class="btn  btn-xs btn-success">
            <i class="fa fa-check"></i>
        </button>
    </a>
@endif

@component('parts.components.collapse._collapsable')
    @slot('buttonText')
        <i class="fa fa-book"></i>
    @endslot
    @slot('button_class')
        btn-xs pull-right
    @endslot

Start date : {{$task->start}}
<br>
End date : {{$task->end}}
<br>
Status: {{$task->status}}
<br>
State: {{$task->state()}}

@endcomponent
@endcomponent