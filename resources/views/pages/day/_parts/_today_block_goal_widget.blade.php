@component('parts.components.widgets.widget_panel')
@slot('fa_icon')
fa-retweet
@endslot
@slot('title')
{{$goal->name}}
@endslot
@slot('class')
@if($goal->is_active)
    active
@else
    done
@endif

@endslot

@slot('subtitle')
    {{$goal->description}}
@endslot

@if($goal->is_active)
    <a href="{!! route('goal.complete.current', [$goal->id]) !!}">
        <button type="button" class="btn btn-xs btn-success">
            <i class="fa fa-check"></i>
        </button>
    </a>
@endif

<br>

@component('parts.components.collapse._collapsable')
@slot('button_class')
btn-xs pull-right
@endslot
@slot('buttonText')
<i class="fa fa-book"></i>

@endslot



@forelse($goal->steps as $step)
    <span class="label label-success">{{$step->name}} {{$step->id}} - {{$step->state}}</span> <br>
@empty
    <p>no steps</p>
@endforelse

@endcomponent


@endcomponent