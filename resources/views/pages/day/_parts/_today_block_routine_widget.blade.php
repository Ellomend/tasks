@component('parts.components.widgets.widget_panel')

@slot('class')
{{$routine->state}}
hide-icon
@endslot


@slot('fa_icon')
fa-retweet
@endslot


@slot('title')
{{$routine->name}}
@endslot


@slot('subtitle')
<p>{{$routine->description}}</p>
@endslot


@component('parts.components.collapse._collapsable')
@slot('button_class')
    btn-xs pull-right
@endslot
@slot('buttonText')
<i class="fa fa-book"></i>

@endslot

Last one: {{$routine->last->diffForHumans()}}
<br>
Next: {{$routine->next_one->diffForHumans()}}
<br>
State: {{$routine->state}}

<br>
Recurrence:
Every {{$routine->interval_value}} {{$routine->interval_kind}}

Completed in past: <br>
@forelse($routine->chores as $chore)
    <span class="label label-success">{{$chore->name}} - {{\Carbon\Carbon::parse($chore->completed_at)->toFormattedDateString()}}</span>
    <br>
@empty
    <p>No completed chores</p>
@endforelse

@endcomponent

@if($routine->state == 'active')
    <a href="{!! route('routine.complete', [$routine->id]) !!}">
        <button type="button" class="btn btn-xs btn-success">
            <i class="fa fa-check"></i>
        </button>
    </a>
@endif
<a href="{!! route('routine.edit', [$routine->id]) !!}" style="float:right;">
    <button type="button" class="btn btn-xs btn-warning">
        <i class="fa fa-pencil"></i>
    </button>
</a>
@endcomponent