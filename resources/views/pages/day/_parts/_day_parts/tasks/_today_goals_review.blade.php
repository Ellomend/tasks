<h3>Steps</h3>

    @component('parts.components.collapse._collapsable')
    @slot('buttonText')
    Open overview of goals
    @endslot
    <?php

    $thruSteps = $resource->thruToday();
    $todayOnlySteps = $resource->todayOnly();
    $startTodayGoals = $resource->startToday();
    $endTodayGoals = $resource->endToday();
    $yesterdayGoals = $resource->getYesterdaySteps();
    ?>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="pane">
                <p><b>thru steps</b></p>
                @forelse($thruSteps as $step)
                    - {{$step->name}} @include('common.parts.buttons.button_show_step')
                @empty
                    <p>no steps</p>
                @endforelse
            </div>
            <div class="pane">
                <p><b>today only steps</b></p>
                @forelse($todayOnlySteps as $step)
                    - {{$step->name}} @include('common.parts.buttons.button_show_step')
                @empty
                    <p>no steps</p>
                @endforelse
            </div>
            <div class="pane">
                <p><b>start today</b></p>
                @forelse($startTodayGoals as $step)
                    - {{$step->name}} @include('common.parts.buttons.button_show_step')
                @empty
                    <p>no steps</p>
                @endforelse
            </div>
            <div class="pane">
                <p><b>end today</b></p>
                @forelse($endTodayGoals as $step)
                    - {{$step->name}} @include('common.parts.buttons.button_show_step')
                @empty
                    <p>no steps</p>
                @endforelse
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="pane">
                <p><b>yesterday</b></p>
                @forelse($yesterdayGoals as $step)
                    - {{$step->name}}
                    @include('common.parts.buttons.button_show_step')
                @empty
                    <p>no steps</p>
                @endforelse
            </div>
            <div class="pane">
                <p><b>completed today</b></p>
                @forelse($resource->stepsCompletedToday() as $step)
                    - {{$step->name}}
                    @include('common.parts.buttons.button_show_step')
                @empty
                    <p>no steps</p>
                @endforelse
            </div>
        </div>
    </div>

    @endcomponent