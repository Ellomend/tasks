<h4>{{$blockItem->block->name}}</h4>
@component('parts.components.collapse._collapsable')
@slot('buttonText')
Open block
@endslot
<div class="pane">
    <p>
        <strong>Active tasks</strong>
    </p>
    <ul>
        @forelse($blockItem->getActiveTasks() as $task)
            <li>
                {{$task->name}}
                @include('common.parts.buttons.button_show_task')
            </li>
        @empty
            <li>No active tasks</li>
        @endforelse
    </ul>
</div>
<div class="pane">
    <p>
        <strong>Late tasks</strong>
    </p>
    <ul>
        @forelse($blockItem->getLateTasks() as $task)
            <li>
                {{$task->name}}
                @include('common.parts.buttons.button_show_task')
            </li>
        @empty
            <li>No late tasks</li>
        @endforelse
    </ul>
</div>
<div class="pane">
    <p>
        <strong>Active steps</strong>
    </p>
    <ul>
        @forelse($blockItem->getActiveSteps() as $step)
            <li>
                {{$step->name}}
                @include('common.parts.buttons.button_show_step')
            </li>
        @empty
            <li>No steps</li>
        @endforelse
    </ul>
</div>
{{--<div class="pane">--}}
    {{--<p>--}}
        {{--<strong>Late steps</strong>--}}
    {{--</p>--}}
    {{--<ul>--}}
        {{--@forelse($blockItem->getLateSteps() as $step)--}}
            {{--<li>--}}
                {{--{{$step->name}}--}}
                {{--@include('common.parts.buttons.button_show_step')--}}
            {{--</li>--}}
        {{--@empty--}}
            {{--<li>No steps</li>--}}
        {{--@endforelse--}}
    {{--</ul>--}}
{{--</div>--}}
<div class="pane">
    <p>
        <strong>Active routines</strong>
    </p>
    <ul>
        @forelse($blockItem->getActiveRoutines() as $routine)
            <li>{{$routine->name}} @include('common.parts.buttons.button_show_routine')</li>
        @empty
            <p>no routines</p>
        @endforelse
    </ul>
</div>
<div class="pane">
    <p>
        <strong>Completed today routines</strong>
    </p>
    <ul>
        @forelse($blockItem->getRoutinesCompletedToday() as $routine)
            <li>{{$routine->name}} @include('common.parts.buttons.button_show_routine')</li>
        @empty
            <p>no routines</p>
        @endforelse
    </ul>
</div>
@endcomponent