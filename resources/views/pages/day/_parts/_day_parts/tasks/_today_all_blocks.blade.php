<h3>
    All blocks
</h3>
@component('parts.components.collapse._collapsable')
@slot('buttonText')
    Open all blocks
@endslot


<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="pane">
            @forelse($resource->getAllBlocks() as $blockItem)
                @include('pages.day._parts._day_parts.tasks.block_part.render_block')
            @empty
                <p>No current blocks</p>
            @endforelse

        </div>
    </div>
</div>
@endcomponent