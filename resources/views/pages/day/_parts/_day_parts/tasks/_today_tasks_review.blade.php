<h3>Tasks</h3>
@component('parts.components.collapse._collapsable')

@slot('buttonText')
Open overview of tasks
@endslot
<div class="container-fluid">
    {{--Tasks--}}
    <div class="row">
        {{-- Thru today--}}
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="pane thru-tasks-pane">
                <p>thru today</p>

                @forelse($resource->getThruTasks() as $task)

                    @include('common.parts.buttons.button_show_task')
                @empty
                    <strong>No thru tasks</strong>
                @endforelse
            </div>

            {{-- End Thru today--}}
            {{-- TOday only--}}
            <div class="pane">
                <p>today only</p>

                @forelse($resource->getTodayOnlyTasks() as $task)

                    @include('common.parts.buttons.button_show_task')
                @empty
                    <strong>No thru tasks</strong>
                @endforelse
            </div>

            {{--Start today--}}
            <div class="pane">
                <p>start today ( {{count($resource->startTodayTasks())}} )</p>

                @forelse($resource->startTodayTasks() as $task)

                    @include('common.parts.buttons.button_show_task')
                @empty
                    <strong>No thru tasks</strong>
                @endforelse
            </div>
            {{--end today--}}
            <div class="pane">
                <p>end today ( {{count($resource->endTodayTasks())}} )</p>

                @forelse($resource->endTodayTasks() as $task)

                    @include('common.parts.buttons.button_show_task')
                @empty
                    <strong>No thru tasks</strong>
                @endforelse
            </div>

        </div>
    </div>
    {{--End Tasks--}}
    {{--Late--}}
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="pane">
                <p>late ( {{count($resource->lateTasks())}} )</p>

                @forelse($resource->lateTasks() as $task)

                    @include('common.parts.buttons.button_show_task')
                @empty
                    <strong>No thru tasks</strong>
                @endforelse
            </div>
            {{--completed today--}}
            <div class="pane">
                <p>completed today ( {{count($resource->getCompletedToday())}} )</p>

                @forelse($resource->getCompletedToday() as $task)

                    @include('common.parts.buttons.button_show_task')
                @empty
                    <strong>No  tasks</strong>
                @endforelse
            </div>
        </div>
    </div>
</div>
@endcomponent