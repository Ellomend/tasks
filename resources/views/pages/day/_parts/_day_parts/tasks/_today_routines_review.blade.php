<h3>Routines</h3>
@component('parts.components.collapse._collapsable')

@slot('buttonText')
Open overview of tasks
@endslot
<div class="container-fluid">
    {{--Tasks--}}
    <div class="row">
        {{-- Thru today--}}
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">


            {{--end today--}}
            <div class="pane">
                <p>active routines ( {{count($resource->getActiveRoutines())}} )</p>

                @forelse($resource->getActiveRoutines() as $routine)
                    - {{$routine->name}}
                    @include('common.parts.buttons.button_show_routine')
                @empty
                    <strong>No routines</strong>
                @endforelse
            </div>
            <div class="pane">
                <p>completed today routines ( {{count($resource->getRoutinesCompletedToday())}} )</p>

                @forelse($resource->getRoutinesCompletedToday() as $routine)
                    - {{$routine->name}}
                    @include('common.parts.buttons.button_show_routine')
                @empty
                    <strong>No routines</strong>
                @endforelse
            </div>
        </div>
    </div>
    {{--End Tasks--}}
    {{--Late--}}
</div>
@endcomponent