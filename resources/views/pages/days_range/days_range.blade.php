@extends('layouts.master')

@section('title')+
@endsection

@section('page_class')
    days_range
@endsection

@section('content')
    Days range review
    <div class="range-wrapper">
        @foreach($range as $day)
            <div class="day-wrapper">
                {{--date--}}
                <div class="date">
                    date
                </div>
                <div class="goals-wrapper">
                    <div class="skipped-steps">
                        <div class="skipped-step">

                        </div>
                    </div>
                    <div class="completed-steps">
                        <div class="completed-step">

                        </div>
                    </div>
                </div>
                <div class="routines-wrapper">
                    <div class="skipped-chores">
                        <div class="skipped-chore">

                        </div>
                    </div>
                    <div class="completed-chores">
                        <div class="completed-chore">

                        </div>
                    </div>
                </div>



            </div>
        @endforeach
    </div>
@endsection