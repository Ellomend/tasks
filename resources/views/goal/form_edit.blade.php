<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group col-md-7 col-md-offset-2">
    <label for="type">Type:</label>

    {!! Form::select('type', ['daily' => 'daily', 'weekly' => 'weekly', 'monthly' => 'monthly'], null, ['class' => 'form-control']) !!}
</div>

<!-- /.form-group -->
<div class="form-group col-md-7 col-md-offset-2">
    <label for="count">Count</label>
    {!! Form::number('count', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-md-7 col-md-offset-4">
    <label for="duration">Duration</label>
    {!! Form::number('duration', null, ['class' => 'form-control']) !!}

</div>

<div class="form-group col-md-7 col-md-offset-4">
    <label for="start">Start</label>
    {!! Form::date('start', null, ['class' => 'form-control']) !!}
</div>


<div class="form-group col-md-7 col-md-offset-2">
    <label for="blocks">block:</label>
    <select class="form-control" name="blocks[]" id="blocks" multiple>
        @foreach($blocks as $block)
            @if(isset($goal) && $goal->blocks->contains('id',$block->id))
                <option value="{{$block->id}}" selected>{{$block->name}}</option>
            @else
                <option value="{{$block->id}}" >{{$block->name}}</option>
            @endif
        @endforeach


    </select>
</div>
<div class="form-group col-md-7 col-md-offset-2">
    <label for="weekdays">weekdays:</label>

    {!! Form::select('weekdays', [ 'monday' => 'monday', 'tuesday' => 'tuesday', 'wednesday' => 'wednesday', 'thursday' => 'thursday', 'friday' => 'friday', 'saturday' => 'saturday', 'sunday' => 'sunday'], null, ['class' => 'form-control', 'multiple' => true, 'name'=>'weekdays[]']) !!}
</div>

<div class="form-group col-md-7 col-md-offset-2">
    <label for="category_id">category:</label>
    <select class="form-control" name="category_id" id="category_id">
        @foreach($categories as $category)
            @if(isset($goal->category) && $goal->category->id == $category->id)
                <option value="{{$category->id}}" selected>{{$category->name}}</option>
            @else
                <option value="{{$category->id}}" >{{$category->name}}</option>
            @endif
        @endforeach


    </select>
</div>


{{--<div class="form-group col-md-7 col-md-offset-2">--}}
    {{--<label for="block">block:</label>--}}
    {{--<select class="form-control" name="blocks[]" id="blocks" multiple>--}}
        {{--@foreach($blocks as $block)--}}
            {{--@if(isset($goal) && $goal->blocks->contains('id',$block->id))--}}
                {{--<option value="{{$block->id}}" selected>{{$block->name}}</option>--}}
            {{--@else--}}
                {{--<option value="{{$block->id}}" >{{$block->name}}</option>--}}
            {{--@endif--}}
        {{--@endforeach--}}


    {{--</select>--}}
{{--</div>--}}

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Update', ['class' => 'btn btn-primary']) !!}
    </div>
</div>