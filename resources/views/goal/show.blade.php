@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Goal {{ $goal->id }}</div>
                    <div class="panel-body">

                        <a href="{{ url('goal/' . $goal->id . '/edit') }}" class="btn btn-primary btn-xs"
                           title="Edit Goal"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['goal', $goal->id],
                            'style' => 'display:inline'
                        ]) !!}
                        {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                                'type' => 'submit',
                                'class' => 'btn btn-danger btn-xs',
                                'title' => 'Delete Goal',
                                'onclick'=>'return confirm("Confirm delete?")'
                        ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <br>
                        <br>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $goal->id }}</td>
                                </tr>
                                <tr>
                                    <th> Name</th>
                                    <td> {{ $goal->name }} </td>
                                </tr>
                                <tr>
                                    <th> Description</th>
                                    <td> {{ $goal->description }} </td>
                                </tr>
                                <tr>
                                    <th> Weekdays</th>
                                    <td>
                                        @foreach($goal->weekdays as $weekday)
                                            {{$weekday}}
                                        @endforeach
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <h3>Steps:</h3>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Start date</th>
                                <th>End date</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <a href="{!! route('goal.history.week', [$goal->id]) !!}">
                                <button type="button" class="btn btn-default">
                                    <i class="fa fa-history"></i>
                                    Week
                                </button>
                            </a>
                            <a href="{!! route('goal.history.month', [$goal->id]) !!}">
                                <button type="button" class="btn btn-default">
                                    <i class="fa fa-history"></i>
                                    Month
                                </button>
                            </a>
                            @foreach($steps as $step)
                                <tr>
                                    <td>{{$step->name}}</td>

                                    <td>{{$step->start_date}}</td>

                                    <td>{{$step->end_date}}</td>

                                    <td>{{$step->status}}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection