@component('parts.components.panel._panel')
@slot('title')
{{$goal->name}}
@endslot
@slot('content')
@component('parts.components.collapse._collapsable')
Description: <br>
{{$goal->description}}
<br>
Category: <br>
{{$goal->category->name or 'no category'}}
<br>
Priority: <br>
{{$goal->priority}}
<br>
Points: <br>
{{$goal->points}}
<br>
Type: <br>
{{$goal->type}}
<br>
Duration: <br>
{{$goal->duration}}
<br>
Start: <br>
{{$goal->start}}
Count: <br>
{{$goal->count}}
<br>
Weekdays: <br>
<ul>
    @forelse($goal->weekdays as $weekday)
        <li>{{$weekday}}</li>
    @empty
        No weekdays !
    @endforelse
</ul>
<br>
Owner: <br>
{{$goal->owner->name}}
<br>
<br>

Blocks <br>
@forelse($goal->blocks as $block)
    {{$block->name}} <br>
@empty
    <p>No blocks</p>
@endforelse
@endcomponent

@component('parts.components.collapse._collapsable')
@slot('buttonText')
Steps {{count($goal->steps)}}
@endslot
@forelse($goal->steps as $item)
    @include('step._parts._step_list_item')
@empty
    <p>No steps !</p>
@endforelse
@endcomponent


@endslot
@component('parts.components.buttons.dropdown_button')

<li>
    <a href="{!! route('goal.edit', [$goal->id]) !!}" class="btn btn-warning btn-sm"
       id="editGoal{{$goal->id}}">
        <span class="glyphicon glyphicon-edit"></span>
        Edit
    </a>
</li>
<li>
    <a href="{!! route('goal.delete', [$goal->id]) !!}" id="deleteGoal{{$goal->id}}"
       class="btn-sm btn btn-danger">
        <span class="glyphicon glyphicon-trash"></span>
        Delete
    </a>
</li>
<li>
    <a href="{!! route('goal.show', [$goal->id]) !!}" id="deleteGoal{{$goal->id}}"
       class="btn-sm btn btn-success">
        <span class="glyphicon glyphicon-trash"></span>
        Show
    </a>
</li>
<li>
    <a href="{!! route('step.gantt', [$goal->id]) !!}">Gantt</a>
</li>
@endcomponent

@endcomponent

