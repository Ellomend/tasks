@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit Goal {{ $goal->id }}</div>
                    <div class="panel-body">
                        <a href="{!! route('steps.regenerate', [$goal->id]) !!}"><button type="button" class="btn btn-primary">Regenerate steps</button></a>
                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($goal, [
                            'method' => 'PATCH',
                            'url' => ['/goal', $goal->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('goal.form_edit')

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection