@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">


                <a href="{{ url('/goal/create') }}" id="createGoal" class="btn btn-primary btn-xs"
                   title="Add New Goal"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></a>
                <a href="{!! route('steps.regenerate.all') !!}">
                    <button type="button" class="btn btn-xs btn-warning">
                        Regenerate All Goals
                    </button>
                </a>
                <br/>
                <br/>

                @forelse($goals as $goal)
                    @include('goal._parts._goal_list_item')
                @empty
                    <p>No goals</p>
                @endforelse
            </div>

        </div>
    </div>

@endsection