<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group col-md-7 col-md-offset-2">
    <label for="type">Type:</label>
    <select class="form-control" name="type" id="type">
        <option value="daily">daily</option>
        <option value="weekly">weekly</option>
        <option value="monthly">monthly</option>
    </select>
</div>

<!-- /.form-group -->
<div class="form-group col-md-7 col-md-offset-2">
    <label for="count">Count</label>
    <input type="number" class="form-control" id="count" name="count" value="10">
</div>

<div class="form-group col-md-7 col-md-offset-4">
    <label for="duration">Duration</label>
    <input type="number" class="form-control" id="duration" name="duration" value="1">
</div>

<div class="form-group col-md-7 col-md-offset-4">
    <label for="start">Start</label>
    <input type="date" class="form-control" id="start" name="start" value="{{\Carbon\Carbon::now()->toDateString()}}">
</div>


<div class="form-group col-md-7 col-md-offset-2">
    <label for="weekdays">weekdays:</label>
    <select class="form-control" name="weekdays[]" id="weekdays" multiple>
        <option value="monday">monday</option>
        <option value="tuesday">tuesday</option>
        <option value="wednesday">wednesday</option>
        <option value="thursday">thursday</option>
        <option value="friday">friday</option>
        <option value="saturday">saturday</option>
        <option value="sunday">sunday</option>

    </select>
</div>

<div class="form-group col-md-7 col-md-offset-2">
    <label for="category_id">category:</label>
    <select class="form-control" name="category_id" id="category_id">
        @foreach($categories as $category)
            @if(isset($goal->category) && $goal->category->id == $category->id)
                <option value="{{$category->id}}" selected>{{$category->name}}</option>
            @else
                <option value="{{$category->id}}" >{{$category->name}}</option>
            @endif
        @endforeach


    </select>
</div>


<div class="form-group col-md-7 col-md-offset-2">
    <label for="blocks">block:</label>
    <select class="form-control" name="blocks[]" id="blocks" multiple>
        @foreach($blocks as $block)
            @if(isset($goal) && $goal->blocks->contains('id',$block->id))
                <option value="{{$block->id}}" selected>{{$block->name}}</option>
            @else
                <option value="{{$block->id}}" >{{$block->name}}</option>
            @endif
        @endforeach


    </select>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>