/**
 * Created by Alex on 08.11.20111.
 *
 *
 *
 *
 */



import {getTodos} from './todoActions'
import {addTodo} from './todoActions'
import {deleteTodo} from './todoActions'
import {completeTodo} from './todoActions'

const state = {
    todos: []
};
const mutations = {
    getTodos(state, todos){
        state.todos = todos;
    },
    addTodo(state, todo){
        state.todos.push(todo);
    },
    deleteTodo(state, todo){
        state.todos.splice(state.todos.indexOf(todo), 1);
    },
    completeTodo(state, todoPayload){
        todoPayload[0].status = todoPayload[1].status;
    }

};

const actions = {
    getTodos,
    addTodo,
    deleteTodo,
    completeTodo
};



export default {
    state, mutations, actions
};