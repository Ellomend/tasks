/**
 * Created by Alex on 08.11.2016.
 *
 *
 *
 *
 */

import Vue from 'vue';
import Vuex from 'vuex';


Vue.use(Vuex);


export const getTodos = function (context) {

    Vue.http.get('/api/v1/tasks').then((response) => {

        context.commit('getTodos', response.data);
    })
};

export const addTodo = function (context, postData) {

    Vue.http.post('/api/v1/tasks', postData).then(function (response) {
        if (response.status == 200) {
            context.commit('addTodo', response.data)
        }
    });
};

export const deleteTodo = function (context, todo) {

    Vue.http.post('/api/v1/tasks/delete', todo).then(function (response) {
        if (response.status == 200) {
            context.commit('deleteTodo', todo)
        }
        else {
        }
    });
};

export const completeTodo = function (context, todo) {
    var postData = {id: todo.id};
    Vue.http.post('/api/v1/tasks/toggle', postData).then(function (response) {
        if (response.status == 200) {
            context.commit('completeTodo', [todo, response.data])
        }
    }).catch(function (response) {
        console.log('error', response);
    })

};