/**
 * Created by Alex on 07.11.2016.
 *
 *
 *
 *
 */

import Vue from 'vue';
import VueResource from 'vue-resource';
import VeeValidate from 'vee-validate';
import VueRouter from 'vue-router';



import store from './vuex/store'

import Dashboard from './pages/Dashboard.vue';
import Profile from './pages/Profile.vue';
import App from './components/app.vue';

Vue.use(VueResource);
Vue.use(VeeValidate);
Vue.use(VueRouter);

Vue.http.interceptors.push((request, next) => {
    request.headers.set('X-CSRF-TOKEN', Laravel.csrfToken);

    next();
});


Vue.component('Dashboard', Dashboard);
Vue.component('Profile', Profile);
Vue.component('app', App);
Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue')
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue')
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue')
);


const routes = [
    {
        path: '/',
        name: 'home',
        component: Dashboard
    },
    {
        path: '/profile',
        name: 'profile',
        component: Profile
    },

];
var router = new VueRouter({
    routes
});


const app = new Vue({
    store,
    router
}).$mount('#vue-app');