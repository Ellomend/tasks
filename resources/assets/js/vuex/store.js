/**
 * Created by Alex on 08.11.2016.
 *
 *
 *
 *
 */


import Vue from 'vue';
import Vuex from 'vuex';


import todoStore from './../components/Todo/todoStore'

Vue.use(Vuex);
Vue.config.debug = true;
const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
    state: {
        count: 1
    },
    mutations: {
        increment(state){
            state.count++;
        }
    },
    actions: {
        increment(context){
            context.commit('increment');
        }
    },
    modules: {
        todoStore: todoStore
    },
    strict: false
})