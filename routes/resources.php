<?php

Route::resource('category', 'CategoryController');
Route::resource('mood', 'MoodController');
Route::resource('project', 'ProjectController');
Route::resource('goal', 'GoalController');
Route::resource('step', 'StepController');


/* Route get delete step  */
Route::get('step/{id}/delete', array( 'as' => 'step.delete', 'uses' => 'StepController@delete' ));


/* Route get get delete category  */
Route::get('/category/{id}/delete', array( 'as' => 'category.delete', 'uses' => 'CategoryController@getDelete' ));


/* Route get delete project  */
Route::get('/project/{id}/delete', array( 'as' => 'project.delete', 'uses' => 'ProjectController@getDelete' ));


/* Route get delete goal  */
Route::get('goal/{id}/delete', array( 'as' => 'goal.delete', 'uses' => 'GoalController@getDelete' ));



/* Route get get goal history  */
Route::get( 'history/goal/{id}/week', array( 'as' => 'goal.history.week', 'uses' => 'GoalController@weekHistory' ) );

/* Route get get goal history  */
Route::get( 'history/goal/{id}/month', array( 'as' => 'goal.history.month', 'uses' => 'GoalController@monthHistory' ) );



/* Route get delete mood  */
Route::get('mood/{id}/delete', array( 'as' => 'mood.delete', 'uses' => 'MoodController@getDelete' ));



/* Route get step gantt  */
Route::get('step/gantt/{id}/show', array( 'as' => 'step.gantt', 'uses' => 'StepController@gantt' ));



/* Route group steps related*/
Route::group([], function () {

    /* Route get complete steps  */
    Route::get('steps/complete/{id}/status/{status}/done', array( 'as' => 'step.complete', 'uses' => 'StepController@complete' ));
    /* Route get regenerate steps from  */
    Route::get('steps/regenerate/all', array( 'as' => 'steps.regenerate.all', 'uses' => 'StepController@regenerateall' ));

    Route::get('steps/{id}/regenerate', array( 'as' => 'steps.regenerate', 'uses' => 'StepController@regenerate' ));


    /* Route get skip  */
    Route::get( 'steps/{id}/skip', array( 'as' => 'steps.skip', 'uses' => 'StepController@skip' ) );


    /* Route get complete current step  */
    Route::get('goal/{id}/complete', array('as' => 'goal.complete.current', 'uses' => 'GoalController@completeCurrentStep'));
});
