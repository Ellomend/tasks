<?php

/* Route group stacks*/
Route::group([ 'prefix' => 'stacks' ], function () {
    /*
    Route resource stack
    stack management
    */
    Route::resource('stack', 'StackController') ;


    /* Route get delete stack  */
    Route::get('/stack/{id}/delete', array( 'as' => 'stack.delete', 'uses' => 'StackController@destroy' ));



    /* Route get bump stack  */
    Route::get('/stack/{id}/bump', array( 'as' => 'stack.additem', 'uses' => 'StackController@bump' ));

    
    /* Route get stacks review  */
    Route::get('stacks/review', array( 'as' => 'stack.review', 'uses' => 'StackController@review' ));

    /* Route get single stack review  */
    Route::get( '/stack/single/{id}/review', array( 'as' => 'stack.review.single', 'uses' => 'StackController@singleReview' ) );


    /*
    Route resource item
    item management
    */
    Route::resource('item', 'ItemController') ;
    
    
    /* Route get delete item  */
    Route::get('/item/{id}/delete', array( 'as' => 'item.delete', 'uses' => 'ItemController@destroy' ));



    /* Route get set item status  */
    Route::any('/item/set/status/', array( 'as' => 'item.status', 'uses' => 'ItemController@status'));

});
