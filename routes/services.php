<?php

Route::get('/', function () {
	return view('auth.home.homepage');
});

Auth::routes();

/* Route get logout  */
Route::get( 'logout', array( 'as' => 'logout', 'uses' => 'Auth\LoginController@logout' ) );



/* Route get home  */
Route::get( '/home', array( 'as' => 'home', 'uses' => 'HomeController@index' ) );


/* Route get 403 page  */
Route::get( '/error/403', array( 'as' => 'unauthorized', 'uses' => 'HomeController@unauthorized' ) );

