<?php
/* Route group pages*/
Route::group(['prefix' => 'pages', 'namespace' => 'Pages'], function () {

    /* Route get mixed tasks  */
    Route::get('mixed/tasks', array('as' => 'pages.mixed', 'uses' => 'PagesController@mixed'));



    /* Route get day  */
    Route::get('day', array( 'as' => 'pages.day', 'uses' => 'PagesController@day' ));

    /* Route get week view  */
    Route::get('week', array('as' => 'pages.week', 'uses' => 'PagesController@week'));

    /* Route get month  */
    Route::get('month', array( 'as' => 'pages.month', 'uses' => 'PagesController@month' ));



    /* Route get tasks calendar page */
    Route::get('calendar/tasks', array( 'as' => 'pages.calendar.tasks', 'uses' => 'CalendarController@tasks' ));

    /* Route get tasks json  */
    Route::get('calendar/tasks/json', array( 'as' => 'pages.calendar.tasks.json', 'uses' => 'CalendarController@getTasks'));
    /* Route get store task from calendar  */
    Route::post('calendar/tasks/add', array( 'as' => 'pages.calendar.tasks.add', 'uses' => 'CalendarController@storeTask'));


    /* Route post update tasks from calendar  */
    Route::post( 'calendar/tasks/move', array( 'as' => 'pages.calendar.tasks.move', 'uses' => 'CalendarController@updateTask' ) );



    /* Route get canedar tasks and steps  */
    Route::get( 'calendar/combined', array( 'as' => 'pages.calendar.combined', 'uses' => 'CalendarController@combined' ) );

    /* Route get tasks json  */
    Route::get('calendar/steps/json', array( 'as' => 'pages.calendar.steps.json', 'uses' => 'CalendarController@getSteps'));


    /* Route get get completed tasks  */
    Route::get( 'calendar/tasks/complete', array( 'as' => 'pages.calendar.tasks.completed', 'uses' => 'CalendarController@getCompletedTasks' ) );
    Route::get( 'calendar/tasks/incomplete', array( 'as' => 'pages.calendar.tasks.incomplete', 'uses' => 'CalendarController@getIncompleteTasks' ) );

    /* Route get get completed steps  */

    /* Route get all steps  */
    Route::get( 'calendar/steps/json', array( 'as' => 'pages.calendar.steps.json', 'uses' => 'CalendarController@getSteps' ) );

    Route::get( 'calendar/steps/complete', array( 'as' => 'pages.calendar.steps.completed', 'uses' => 'CalendarController@getCompletedSteps' ) );
    Route::get( 'calendar/steps/incomplete', array( 'as' => 'pages.calendar.steps.incomplete', 'uses' => 'CalendarController@getIncompleteSteps' ) );



    /* Route get past range  */
    Route::match(['get','post'], 'past/range', array( 'as' => 'pages.past.range', 'uses' => 'PastRangeController@pastRange' ) );


});
