<?php

Route::group(['middleware' => 'web', 'prefix' => 'tasks'], function () {
    Route::get('/', 'Tasks\TaskWebController@index');


    /*Tasks*/

    /*
    Route resource Task
    Task management
    */
    Route::resource('task', 'Tasks\TaskWebController');


    /* Route get delete task  */
    Route::get('task/{id}/delete', array('as' => 'task.delete', 'uses' => 'Tasks\TaskWebController@getDelete'));


    /*Contexts*/
    /*
    Route resource context
    context management
    */
    Route::resource('context', 'ContextWebController');


    /* Route get delete context  */
    Route::get('context/{id}/delete', array('as' => 'context.delete', 'uses' => 'ContextWebController@getDelete'));


    /* Route get complete task  */
    Route::get('tasks/complete/{id}/status/{status}/done', array('as' => 'task.complete', 'uses' => 'Tasks\TaskWebController@complete'));

    /* Route get complete task  */
    Route::get('tasks/{id}/archive', array('as' => 'task.archive', 'uses' => 'Tasks\TaskWebController@archive'));

    /*
    Route resource Blocks
    Blocks management
    */
    Route::resource('block', 'BlockWebController');


    /* Route get delete block  */
    Route::get('block/{id}/delete', array('as' => 'block.delete', 'uses' => 'BlockWebController@getdelete'));


    /* Route get generate default blocks  */
    Route::get('blocks/generate/default', array('as' => 'block.generate.default', 'uses' => 'BlockWebController@regenerate'));


    /* Route get today  */
    Route::get('today', array('as' => 'tasks.today', 'uses' => 'Tasks\TaskTodayController@today'));

    /* Route get archived list  */
    Route::get('archived', array('as' => 'task.archived.list', 'uses' => 'Tasks\TaskWebController@archived'));


    /* Route get filter  */
    Route::any('filter', array('as' => 'tasks.filter', 'uses' => 'Tasks\TaskFilterController@filter'));
});


/* Route group experimental*/
Route::group(['middleware' => ['web'], 'prefix' => 'experimental'], function () {

    /* Route get gantt chart  */
    Route::get('gantt', array('as' => 'tasks.gantt', 'uses' => 'Tasks\TaskWebController@gantt'));

    /* Route get widgets demo  */
    Route::get('widgets', array( 'as' => 'tasks.widgets', 'uses' => 'Tasks\TaskWebController@widgets' ));
});
