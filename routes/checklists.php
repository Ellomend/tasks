<?php

/* Route group checklist*/
Route::group(['prefix' => 'checklists'], function () {
    /*
    Route resource checklist
    checklist management
    */
    Route::resource('checklist', 'ChecklistController') ;


    /* Route get delete checklist  */
    Route::get('/checklist/{id}/delete', array( 'as' => 'checklist.delete', 'uses' => 'ChecklistController@destroy' ));
    
    
    /* Route get add point to checklist  */
    Route::get('/checklist/{id}/add', array( 'as' => 'checklist.add.point', 'uses' => 'ChecklistController@addPoint' ));


    /* Route get blueprints checklists  */
    Route::get('blueprints', array('as' => 'checklist.blueprint.index', 'uses' => 'ChecklistController@getBlueprints'));


    /* Route get blueprint start  */
    Route::get('blueprint/{id}/start', array('as' => 'checklist.blueprint.start', 'uses' => 'ChecklistController@startBlueprint'));



    /* Route get reorder checklist  */
    Route::get('reorder/{id}/', array( 'as' => 'checklist.reorder', 'uses' => 'ChecklistController@reorder' ));


    /* Route get running checklists  */
    Route::get( '/running/checklists', array( 'as' => 'checklist.running', 'uses' => 'ChecklistController@running' ) );
    



    /*
    Route resource point
    point management
    */
    Route::resource('point', 'PointController') ;


    /* Route get delete point  */
    Route::get('/point/{id}/delete', array( 'as' => 'point.delete', 'uses' => 'PointController@destroy' ));


    /* Route get move up point  */
    Route::get('point/{id}/moveup', array('as' => 'point.moveup', 'uses' => 'PointController@moveUp'));


    /* Route get move down point  */
    Route::get('point/{id}/movedown', array('as' => 'point.movedown', 'uses' => 'PointController@moveDown'));

    /* Route get complete point  */
    Route::get('status/{id}/{status}', array( 'as' => 'point.status', 'uses' => 'PointController@setStatus' ));

});
