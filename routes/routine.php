<?php

/* Route group Routine*/
Route::group(['prefix' => 'routine'], function () {
    /*
    Route resource routine
    routine management
    */
    Route::resource('routine', 'RoutineController') ;


    /* Route get actual routines  */
    Route::get('/actual', array( 'as' => 'routine.actual', 'uses' => 'RoutineController@actual'));


    /* Route get routine by block  */
    Route::get('/blockfilter/{id?}', array('as' => 'routine.block.filter', 'uses' => 'RoutineController@blockFilter'));



    /* Route get complete routine  */
    Route::get('/complete/{id}/', array( 'as' => 'routine.complete', 'uses' => 'RoutineController@complete' ));


    
    /* Route get delete routine  */
    Route::get('/routine/{id}/delete', array( 'as' => 'routine.delete', 'uses' => 'RoutineController@destroy' ));
    
    /*
    Route resource Chore
    Chore management
    */
    Route::resource('chore', 'ChoreController') ;
    
    
    /* Route get delete chore  */
    Route::get('/chore/{id}/delete', array( 'as' => 'chore.delete', 'uses' => 'ChoreController@destroy' ));



    /* Route get routine week history  */
    Route::get( '/history/routine/{id}/week', array( 'as' => 'routine.history.week', 'uses' => 'RoutineController@routineWeekHistory' ) );


    /* Route get routine month history  */
    Route::get( 'history/routine/{id}/month', array( 'as' => 'routine.history.month', 'uses' => 'RoutineController@routineMonthHistory' ) );



});
