const elixir = require('laravel-elixir');
var browserify = require('laravel-elixir-browserify-official');

require('laravel-elixir-vue-2');
require('laravel-elixir-webpack-react');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

elixir((mix) => {
    mix
        // .sass('app.scss')
        .copy('node_modules/bootstrap-sass/assets/fonts/bootstrap/', 'public/fonts/bootstrap')
        // .webpack('app.js')
        // .scripts('vueground.js')
        // .webpack('vue.js')
        .webpack('react.js')
        .browserSync({
            proxy: 'blog.app',
            open: true
        });
});

